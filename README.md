<h1>Notes</h1>
This project is for personal professional material.

<h2>Useful links</h2>
<ol>
    <strong><li>Arduino:</li></strong>
    <dl>
        <dt>Arduino Class by Instructables</dt>
        <dd>http://www.instructables.com/class/Arduino-Class/</dd>
    </dl>
    <strong><li>Electronics:</li></strong>
    <dl>
        <dt>Electronics Class by Instructables</dt>
        <dd>http://www.instructables.com/class/Electronics-Class/</dd>
    </dl>
    <dl>
        <dt>Charlieplexing LEDs (many LEDs / few PINs)</dt>
        <dd>http://www.instructables.com/id/Charlieplexing-LEDs--The-theory/</dd>
    </dl>
    <dl>
        <dt>Charlieplexing the Arduino</dt>
        <dd>http://www.instructables.com/id/Charlieplexing-the-Arduino/</dd>
    </dl>
    <dl>
        <dt>Many LEDs few PINs (Shift Register)</dt>
        <dd>https://www.arduino.cc/en/Tutorial/ShiftOut<br>
            http://forum.arduino.cc/index.php?topic=15933.0</dd>
    </dl>
</ol>