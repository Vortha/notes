<h2>GIT</h2>
<h3>Make the current git branch a master branch</h3>
This is what you want to do:
<p>
<code>git checkout better_branch
git merge --strategy=ours master    # keep the content of this branch, but record a merge
git checkout master
git merge better_branch             # fast-forward master up to the merge</code>
</p>

If you want your history to be a little clearer, I'd recommend adding some information to the merge commit message to make it clear what you've done. Change the second line to:
<p>
<code>git merge --strategy=ours --no-commit master
git commit          # add information to the template merge message</code>
</p>
