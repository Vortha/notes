<p>Dependency Injection - Design pattern</p>

<p>HelloWorld Spring/Maven: http://www.programcreek.com/2014/02/spring-mvc-helloworld-using-maven-in-eclipse/</p>

<p>Spring Tutorial: https://www.tutorialspoint.com/spring/spring_bean_scopes.htm</p>

<p>
    <h2>Associazione: Aggregazione vs Composizione</h2>
    <p>
        <p>
            Data un'Associazione fra due classi, risolvo l'Associazione inserendo come attributo di una classe un oggetto dell'altra e lo istanzio:
        </p>
    </p>
</p>
```java
public class Quadro {
    private String titolo;
    private int altezza;
    private int larghezza;
    private Autore autore = new Autore();
}
```
<p>
    <p>
        </p>
            In questo modo indico l'Associazione fra Quadro e Autore.
        </p>
        <p>
            Per identificare una relazione di Composizione, devo istanziare l'oggetto attributo con l'oggetto passato come parametro del costruttore della classe dell'Associazione:
        </p>
    </p>
</p>
```java
public class Quadro {
    private String titolo;
    private int altezza;
    private int larghezza;
    private Autore autore = new Autore();

    public Quadro(String titolo, int altezza, int larghezza, Autore autore) {
		super();
		this.titolo = titolo;
		this.altezza = altezza;
		this.larghezza = larghezza;
		this.autore = autore; // Composizione
	}
}
```
<p>
    <p>
        <p>
            Per identificare l'Aggregazione, semplicemente predispongo lo spazio per l'oggetto attributo all'interno del costruttore:
        </p>
    </p>
</p>
```java
public class Museo {
	private String nome;
	private String citta;
	private String tipo;
	private Quadro[] quadri;

	// Predispongo lo spazio per i quadri senza doverli assegnare sul momento
	// per rispettare la relazione di Aggregazione
	public Museo(String nome, String citta, String tipo, int dimMuseo) {
		super();
		this.nome = nome;
		this.citta = citta;
		this.tipo = tipo;
		this.quadri = new Quadro[dimMuseo]; // Aggregazione
	}
}
```

<hr>
<p>
    <b>c3p0 per connessioni cached (Hibernate)</b>
</p>

<hr>
<p>
    Ogni DAO rilancia l'eccezione al service specifico, sottoforma di eccezione trasformata per il service:
</p>
```java
public class DAO {

    public void method() {
        try {
            // codice che lancia una DAOException
        } catch (DAOException e) {
            throw new ServiceException("Messaggio di errore");
        }
    }
}
```

<h2>Java Web</h2>

<p>Resource: oggetto la cui classe ha il metodo <code>close()</code></p>

<p>
    Quando realizzo il codice Java, specchio le relazioni del DB: relazioni 1-N si traducono mettendo l'identificativo dell'entità dell'1 nella classe dell'N:<br>
    <b>Database</b><br>
<code>    ______                  _____________
    |Utente|________        |ContoCorrente|______________________
    |id|nome|cognome|-n---1-|numero|saldo|data_apertura|id_utente|</code><br>
    <b>Java</b><br>
    <code>Utente:
    int id;
    String nome;
    String cognome;</code><br><br>
    
    <code>ContoCorrente:
    int numero;
    double saldo;
    Date dataApertura;
    int idUtente;</code><br>
</p>

<h2>Java EE</h2>
<p>Java con un'ulteriore libreria per le applicazioni distribuite.</p>

<p>
    Java EE offre due container separati:
    <ul>
        <li>Web container (parte web)</li>
        <li>EJB container (parte business)</li>
    </ul>
</p>

<p>
    Web container: libreria di java che ci aiuta a realizzare una web application server side.
    EEJB container: libreria di java che ci aiuta a realizzare un'applicazione di business back-end.
</p>

<p>
    Container: speciale tipo di framework. Cioè un framework + gestione del ciclo di vita degli oggetti.
    Framework: è una libreria che utilizza il pattern InversionOfControl.
    Pattern: soluzione architetturale ad una problematica ricorrente.
</p>

<p>Quando un framework gestisce la creazione e la distruzione degli oggetti, si chiama container (esempio: main parte del framework).</p>

<p>
    (Alcune) regole HTTP:
    <ul>
        <li>Il Client inizia (sempre) la comunicazione richiedendo qualcosa e il Server risponde;</li>
        <li>L'indirizzo (URL) del servizio deve essere formato come: http:\\IP:PORT\WEBAPP\RESOURCE (RESOURCE: path ad una risorsa sul server).</li>
    </ul>
</p>

<p>Il Server è un programma (Tomcat) ma anche la macchina che ospita il programma.</p>

<p>
    JSP:
</p>
```jsp
<% // codice Java %>
<%= // codice Java da stampare %>
```
```jsp
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	Date date = new Date();
%>

<h2>La data corrente è: <%= date %></h2>

</body>
</html>
```

<h2>Spring</h2>
<p>
    <ol>
        <li>Creo un nuovo Web Dynamic Project;</li>
        <li>Click destro sul progetto -> Configure -> Convert to a Maven Project;</li>
        <li>Aggiungo le librerie di Spring per Maven*</li>
    </ol>
    
    *
</p>
```xml
<dependencies>
	<!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-context</artifactId>
		<version>4.3.9.RELEASE</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/org.springframework/spring-aop -->
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-aop</artifactId>
		<version>4.3.9.RELEASE</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-webmvc</artifactId>
		<version>4.3.9.RELEASE</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/org.springframework/spring-web -->
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-web</artifactId>
		<version>4.3.9.RELEASE</version>
	</dependency>
</dependencies>
```
<p>
    <p>
        Creo una classe controller per ogni funzionalità della web app.<br>
        <b>Packaging</b><br>
        <ul>
            <li>java.src.controller</li>
            <li>java.src.model</li>
            <li>WEB-INF/jsp</li>
        </ul>
        
        Tramite l'annotation <code>@Controller</code> si determina una classe Controller. Quando Spring legge l'annotazione e fa il resto.<br>
        Definisco una classe Controller la quale ha i metodi che implementano i vari servizi (contestualizzati). I metodi del Controller richiamano le classi Service per soddisfare la richiesta. Sui metodi della classe Controller che identificano un servizio, inserisco l'annotation <code>@RequestMapping("/NomeServizio")</code>. Il metodo della classe Controller restituisce (come minimo) il nome della JSP da visualizzare. Se ci sono dati da passare, restituisco un oggetto di tipo ModelAndView:
    </p>
</p>
```java
@Controller
public class MathController {

    // Se non ci fossero dati da passare
	public String somma(double n1, double n2) {
		MathService ms = new MathServiceImpl();
		double ris = ms.somma(n1, n2);

		return "risSomma";
	}

	// Se ci fossero dati da passare
	@RequestMapping("/Somma")
	public ModelAndView somma(double n1, double n2) {
		MathService ms = new MathServiceImpl();
		double ris = ms.somma(n1, n2);

		return new ModelAndView("risSomma", "somma", ris);
	}
}
```

```java
public interface MathService {

	public double somma(double a, double b);
}
```

```java
public class MathServiceImpl implements MathService {

	@Override
	public double somma(double a, double b) {
		return a + b;
	}

}
```

<p>
    Per più package controller:
</p>

```xml
<context:component-scan base-package="controller, x, y" />
```

<p>
    <p>
        Sfruttare la RAM del client solo se non c'è perdita dei dati: il server deve sapere tutto quello che fa il client, posso utilizzare la RAM del client solo se i dati che registro poi non vengono persi.
    </p>
    
    <h3>Dependency Injection</h3>
    <p>
        <p>
        <blockquote>In software engineering, dependency injection is a technique whereby one object supplies the dependencies of another object. A dependency is an object that can be used (a service). An injection is the passing of a dependency to a dependent object (a client) that would use it.</blockquote>
        </p>
        <p>
        Con l'annotation <em><code>@Autowired</code></em> voglio dire a spring di risolvere da solo il setting di una determinata proprietà: se non ci riesce restituisce un errore. Se c'è una sola classe ad implementare l'interfaccia UserService, istanzia un oggetto di quella classe, altrimenti va in eccezione. Quest'annotation funziona solo per classi annotate. Tre stereotipi: Controller, Service e Repository. Se una classe non appartiene a nessuno di questi tre, allora appartiene a Component.
        </p>
    </p>
</p>
```java
public interface UserService {
    // methods
}
```

```java
@Service
public class UserServiceImpl implements UserService {
    @Override
    // methods
}
```

```java
@Controller
public class UserController {
    @Autowired
    private UserService us;
    
    public void method() {
        us.method();
    }
}
```

<p>
    <h2>OracleDB</h2>
    <p>Operazioni preliminari: avviare il DB dai servizi di Windows, connessione al DB come amministratore e avviarne l'istanza.</p>
    
    <table>
        <tr>
            <th colspan="2"><b>Comandi</b></th>
        </tr>
        <tr>
            <td>SQLPLUS</td>
            <td>Avvio oracleDB</td>
        </tr>
        <tr>
            <td>sys/{password} as sysdba</td>
            <td>Usato per fare l'accesso al db come amministratore</td>
        </tr>
        <tr>
            <td>startup</td>
            <td>Avvio dell'istanza</td>
        </tr>
        <tr>
            <td>connect {user}/{password}</td>
            <td>Connessione al db con utente {user} e password {password}</td>
        </tr>
        <tr>
            <td>select table_name from user_tables</td>
            <td>Mostra le tabelle visibili all'utente</td>
        </tr>
        <tr>
            <td>commit</td>
            <td>Rende effettive le operazioni DML</td>
        </tr>
        <tr>
            <td>set oracle_sid = {dbname}</td>
            <td>setta {dbname} come db di default</td>
        </tr>
        <tr>
            <td>alter user {username} identified by {password} account unlock</td>
            <td>sblocca lo user {username} e gli setta la password {password}</td>
        </tr>
        
    </table>
    <p>Per disabilitare la connessione come sys (as sysdba) per tutti gli utenti, andare sulla configurazione dei gruppi e cancellare tutti gli utenti dal gruppo ora_dba: strumenti di amministrazione -> gestione computer -> utenti e gruppi locali -> gruppi.</p>
    
    <p>I file vengono salvati sul disco rigido e nel DB viene salvato un puntatore al file.</p>
    
    <p>user_tables, all_tables, dba_tables: le prime due possono essere da chiunque ma si adattano all'utente che le visualizza: la prima mostra solo ciò che appartiene all'utente, la seconda mostra ciò che appartiene all'utente e ciò su cui l'utente ha i privilegi. La dba_tables mostra tutte le tabelle (amministratore).<br>
    Le V${...} tables sono consultabili solo dall'amministratore e hanno informazioni sul database.</p>

    <p>Identificativi e nomi tabelle vengono convertite in uppercase. Singoli apici per memorizzare dati così come li si scrive, altrimenti vengono inseriti in uppercase.</p>

    <p>dual - tabella formata da una riga e una colonna</p>
    
    <p>Transazione: insieme di comandi accettati o rifiutati insieme. I comandi DDL fanno una transazione a sé.<br>
    Se devo dare un comando DDL, blocco il lavoro che sto facendo, apro un'altra finestra e do i comandi DDL.</p>
    
    <p>user_tables (vista che) fa parte dello schema esterno (slide 43 - feb02_BD01.ppt).<br>
    Ogni utente vede solo lo schema esterno (viste visibili dall'utente), le tabelle costituiscono lo schema logico. Solo SYS vede lo schema fisico del DB intero.</p>
    
    <p>PL/SQL estensione procedurale di SQL. Non può essere compilato ed eseguito esternamente, è solo interno al database Oracle.</p>
    
    <p>Istanza: insieme delle righe di una tabella / dbms in ram.</p>
    
    <p><code>Grant</code> e <code>Revoke</code> fanno parte del DDL.</p>
    
    <p>Modello logico -> Relazione (tabelle che non hanno due righe uguali)
    Modello concettuale -> Entità-Relazione</p>
    
    <p><h4>Filastrocca</h4>
        <blockquote>
        Chi scrive non blocca chi legge, chi legge non blocca chi scrive, chi scrive blocca chi scrive sulle stesse righe, chi legge blocca chi scrive solo nel caso di select for update.
        </blockquote>
    </p>
    
    <p>Relazione uno a molti: una identificazione esterna è possibile solo attraverso una relationship a cui l’entità da identificare partecipa con cardinalità (1,1).</p>
</p>

<h2>GIT</h2>
<p>
VCS distribuito: file all'interno della working directory. Questi file vengono indicizzati all'interno della staging area, una sorta di buffer tra il progetto e il repository locale, al quale vengono aggiunti file locali attraverso l'operazione <code>add</code>. Con l'operazione <code>commit</code> convalido le modifiche in locale. Con l'operazione <code>push</code> passo il codice sul repository in remoto. Il comando <code>pull</code> equivale a fare <code>fetch</code> + <code>merge</code>.<br>
git diff per vedere la differenza dei tra working directory e repository locale, git diff --staged per vedere la differenza tra directory e repository locale quando i file sono stati aggiunti alla staged area.<br>
branch: puntatore di una commit.<br>
<code>git branch slave</code> crea un branch lasciando l'head sul master<br>
<code>git checkout -b slave</code> crea un branch e switcha l'head<br>
Merge: faccio un checkout al master, dopo di che dico, dal master, git merge slave;<br>
Merge attraverso fast-forward: antenato comune tra il branch master e il branch slave che è sequenziale.<br>
Merge attraverso three-way: ho creato due branch (master e slave), lavoro sullo slave, faccio un checkout sul master e lavoro sul master. Il merge avviene trovando l'antenato comune e creando una nuova merge comune ad entrambi.<br>
<code>git branch -d <nomebranch> --delete</code><br>
Se ci sono conflitti, si può usare <code>-D</code>.<br>
<code>git push origin --delete <nomebranch></code> eliminare un branch remoto.<br>
Rebase: prende un branch e lo riposiziona sulla base commit di un altro.<br>
Creo un master, mi sposto sullo slave, lavoro sullo slave e faccio un git rebase master, stacca il branch slave e lo sposta sul master. Da qui posso fare un git checkout master, git merge slave e fa una fast-forward e riposiziona l'head. Quando si effettua una rebase, chiede se eliminare le commit.<br>
<code>squash</code> crea una nuova commit che è l'unione di più commit, questo serve per pulire i commit.<br>
<code>git commit --amend ""</code> modifica il messaggio della commit precedente.<br>
<code>clone</code> permette di copiare un repository remoto su una macchina locale: <code>git clone "..."</code>.<br>
<code>git tag nometag -a -m '...'</code> crea un tag con un'etichetta, utile ad esempio per tenere traccia delle release stabili.<br>
<code>git show <nometag></code> mostra i dettagli di un tag.<br>
Il tag è un puntatore ad una specifica commit, la differenza con il branch è che i branch non sono immutabili, mentre un tag non è possibile muoverlo avanti o indietro.<br>
<code>git push origin <nometag></code><br><br>


SVN è centralizzato: un server sul quale vari client fanno operazioni di commit e update.
</p>