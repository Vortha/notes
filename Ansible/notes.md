<p>
<b>Note:</b> these notes are based on [Ansible 2.5 documentation](https://docs.ansible.com/ansible/2.5/)
</p>

# 1. Installation

## 1.1 Version

<p>
If you are wishing to run the latest released version of Ansible and you are running Red Hat Enterprise
Linux (TM), CentOS, Fedora, Debian, or Ubuntu, we recommend using the OS package manager.<br>
For other installation options, we recommend installing via “pip”, which is the Python package manager,
though other options are also available.
</p>

## 1.2 Control Machine Requirements

<p>
Currently Ansible can be run from any machine with Python 2 (versions 2.6 or 2.7) or Python 3
(versions 3.5 and higher) installed (Windows isn’t supported for the control machine).<br>
This includes Red Hat, Debian, CentOS, OS X, any of the BSDs, and so on.
</p>

## 1.3 Installing the Control Machine

<p>
For the installation command to execute, see the [official guide section](https://docs.ansible.com/ansible/2.5/installation_guide/intro_installation.html#installing-the-control-machine).
</p>

# 2. Quick Start

<p>
WHAT IS ANSIBLE?
</p>

<p>
Ansible is powerful IT automation that you can learn quickly. It's simple enough for everyone
in your IT team to use, yet powerful enough to automate even the most complex deployments.
Ansible handles repetitive tasks, giving your team more time to focus on innovation.
</p>

<p><b>
Simple. Powerful. Agentless.
</b></p>

<p>
With Ansible you can start to do real work in just minutes due to its simple, human-readable
language. Altogether its powerful capabilities allow orchestration of your entire application
lifecycle regardless of where it’s deployed. And Ansible’s agentless architecture means it is
one less thing to keep secure. 
</p>

<p><b>
Control. Knowledge. Delegation.
</b></p>

<p>
Red Hat Ansible Tower expands IT automation to your enterprise adding control, knowledge,
and delegation on top of Ansible’s automation engine.
</p>

<p><b>
Ready to Accelerate?
</b></p>

<p>
The answer is clear. We need to stretch beyond the boundaries of manual effort. When you
automate, you accelerate. And when you build a culture of automation across your company,
anything is possible. Ansible helps smart people do smarter work by having automation do
the rest.
</p>

## 2.1 Playbooks

<p>
Plain-text YAML files that describe the desired state of something. They're human and machine readable
and can be used to build entire application environments.<br>
Playbooks don't require any special coding skills to use.
</p>

## 2.2 Variables

<p>
Variables enable you to alter how Playbooks run. They can be used nearly everywhere in Playbooks and
can be inherited from an inventory explicitly set at runtime --discovered at the start of a Playbook
run-- read from files or even set via Ansible Tower or also set as the result of a task.
</p>

## 2.3 Inventories

<p>
In order to run a Playbook or an Ansible command, you need a list of targets on which to automate.
This is what an inventory does. Inventory lists can be built and stored in several different ways,
including static files, i.e. Etsy Ansible hosts. It can be also dynamically generated via an inventory
script that will pull a list of hosts from an external source. You can also specify variables as part
of an inventory list. For example, set a particular host key that's needed to log into that system
remotely. Inventories are ultimately lists of things you want to automate across.
</p>

<p>
Example of a static inventory list:
</p>

```YAML
[web]
web-1.example.com
web-2.example.com
[db]
db-a.example.com
db-b.example.com
```

## 2.4 Overview

<p>
<b>Playbooks</b> contain <b>plays</b>.<br>
<b>Plays</b> contain <b>tasks</b>.<br>
<b>Tasks</b> call <b>modules</b>.
</p>

<p>
<b>Tasks</b> run <b>sequentially</b> so the work like you'd expect them to if you were to manually
step through your list of processes on the command line.
</p>

<p>
Here's an example of a Playbook which has one play, three tasks and a handler:
</p>

```YAML
---
- name: install and start apache
  hosts: web
  remote_user: justin
  become_method: sudo
  become_user: root
  vars:
    http_port: 80
    max_clients: 200

  tasks:
  - name: install httpd
    yum: name=httpd state=latest
  - name: write apache config file
    template: src=srv/httpd.j2 dest=/etc/httpd.conf
	notify:
	- restart apache
  - name: start httpd
    service: name=httpd state=running

  handlers:
  - name: restart apache
    service: name=httpd state=restarted
```

<p>
The three dashes at the top of the file indicate that this is a YAML file. Next, we set some directives
and then move along to the tasks themselves.<br>
The <b>hosts</b> keyword specifies which inventory group Ansible should apply this Playbook to.<br>
With <b>remote_user</b> we tell Ansible which user to use for remote connections.<br>
Next, we use several more directives to tell Ansible how to escalate privileges on the target systems.
In this case, we're just using sudo but can support nearly any mechanism necessary. Also note that you
can use Ansible <b>without</b> root access. You'll just be limited to doing only the things that you
would do if logged directly into the target system as your normal non-privileged user.<br>
We also added a few variables into the Playbook itself.
</p>

<p>
Next, we have tasks. Playbooks are self-documenting, so the name directives you see listed here serve
to classify output at runtime but also help with the readability of the Playbook as a whole.
Each task is given a name, followed by a module, which is what tells Ansible how to configure the
target system.<br>
We start with the package installation using <b>yum</b>. Then we deploy a configuration file
using the <b>template</b> module. Any variables inside that template will be replaced as appropriate
with the variables that we specified at the top of the Playbook.<br>
Finally, we ensure that <b>service</b> is started.<br>
We also added a handler here. If the configuration file is altered as part of this Playbook run,
then Ansible will restart the service once all the plays have successfully completed.
</p>

### 2.4.1 Modules

<p>
Modules generally follow the same structure when you call them. In the docs, you'll find a list of what
RED HAT ship along with possible directives and values. They also provide numerous example tasks for each
module, all of which help you get started even faster.<br>
There are over 450 Ansible-provided modules that automate nearly every part of your environment.
</p>

<p>
Standard structure:
</p>

```YAML
module: directive1=value directive2=value
```

<p>
http://docs.ansible.com/ansible/modules_by_category.html
</p>

### 2.4.2 Advanced Playbook capabilities

<p>
The example Playbook we're going to demonstrate here is really pretty simple. But there are many more complex
and sophisticated things you can do with Ansible Playbooks. Beyond altering Playbooks with variables,
there are a number of ways to change how individual tasks are executed on your target hosts. We can loop and
apply conditionals to any task.
</p>

```YAML
with_items, failed_when, changed_when, until, ignore_errors
```

<p>
Ansible also has the concept of a role, which is a special kind of Playbook that is fully self-contained and
portable. A role can include things like tasks, variable files, configuration templates, and other supporting
files that are needed to complete a complex orchestration. The docs have more information on roles. Ansible
Galaxy has been created as a repository for user-generated Ansible role content.
</p>

## 2.5 Using Ansible

<p>
Now that we've covered the basics, it's time to actually automate with Ansible.
</p>

### 2.5.1 How to run

<p>

There are three ways to run Ansible:
 - by directly calling the module from command line (Ad-Hoc);<br>
```YAML
> ansible <inventory> -m
```
 - calling the Playbook from the command line;
```bash
> ansible-playbook
```
 - by using Ansible Tower (Automation Framework).

</p>

#### 2.5.1.1 Ad-Hoc commands

<p>
First, we'll look at how run ad-hoc commands.<br>
We can run raw commands on target systems or call modules directly, no Playbook required.
</p>

```bash
> ansible <inventory> <options>
> ansible web -a /bin/date
> ansible web -m ping
> ansible web -m yum -a "name=openssl state=latest"
```

<p>
To run the above commands, you have to edit /etc/ansible/hosts file adding [web] group of hosts.<br>
E.g.:
</p>

```YAML
[web]
green.example.com
blue.example.com
192.168.100.1
192.168.100.10
```

#### 2.5.1.2 Running Playbooks

<p>
Running one-off tasks across a number of systems is cool and all. But in order to do more complex things,
we need to use a Playbook. Let's look at what it takes to run these.
</p>

```bash
> ansible-playbook <options>
```

<p>
Calling a playbook, we get statuses of each of the items that Ansible automated.
</p>

```bash
> ansible-playbook my-playbook.yml

PLAY [Set up the demo users and files] **********************

TASK [setup] ************************************************
ok: [172.31.12.202]
ok: [172.31.12.200]
...

```

<p>
If we run the same Playbook again, Ansible report that everything is OK. This means that no changes were made,
and the target systems were already in the desired state.
</p>

#### 2.5.1.3 Check Mode

<p>
Ansible also has the concept of a check mode or <b>dry-run</b>. It's a way of validating the existing running state
of your system and systems. Not all the modules support check mode, but a good number of them do. This is a powerful
way to validate changes you want to make before you actually have to change the end state of target systems.
</p>

```bash
> ansible web -C -m yum -a "name=httpd state=latest"
> ansible-playbook -C my-playbook.yml"
```

### 2.5.2 Ansible Tower

<img src="img/ansibleTower.png">

<p>
Tower layers in control, knowledge and delegation, on top of Ansible's simple powerful and agentless automation engine.
Towers UI and API that centralizes Ansible runs, which also makes it easier to integrate Ansible into other systems or
workflows required for things like CICD or DevOps processes. Also, all Ansible playbook run output is captured and
stored inside Tower. So you can easily audit the results, including who ran what job, what systems were updated, the
output of each system, and so on. It even lets you map those actions back to the LDAP or Active Directory user that
initiated the event. With delegation, Tower enables you to define precise roles and responsibilities, so the users
can only run pre-approved automations in environments they're supposed to have access to. You can even remove their
ability to alter variables, effectively, offering them a push-button console from with which to launch automation jobs
from. No Ansible knowledge or skills are required at all. Finally, Ansible Tower can ingest and tightly control access
to the credentials needed to automate various Cloud APIs or even the credentials needed to run jobs and target systems
themselves, which means you can safely automate without concern that your credentials can be exposed to users who should
not have them.<br>
Once you're up and running with Ansible, you should definitely take a look at Tower. There are a lot of information on
the site about it. And frankly, it'll help you take your automation effort to the next level.
</p>

### 2.5.3 Galaxy

<p>
Finally, we have Galaxy.
Ansible Galaxy completes our batteries included story by givint you access to thousands upon thousands of user-contributed
roles, playbooks and modules. With the Ansible Galaxy command line, you can use roles directely from this repo. But you
can easily download and customize the roles locally before running them in your environment. The great thing about Galaxy
is that no matter what you're trying to automate. There's a good chance that someone else has already done it. So why not
just learn from them? It helps you do real work with Ansible that much faster.
</p>