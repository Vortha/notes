<h1>Index</h1>

<ol>
    <li>
        <details>
            <summary><a href="#1-dipendency-injection-di">Dipendency Injection (DI)</a></summary>
            
            <ol>
                <li><a href="#11-constructor-injection">Constructor Injection</a></li>
                <li><a href="#12-configuration">Configuration</a></li>
            </ol>
        </details>
    </li>
    
    <li>
        <a href="#2-aspect-oriented-programming-aop">Aspect-Oriented Programming (AOP)</a>
    </li>
    
    <li>
        <details>
            <summary><a href="#3-wiring-beans">Wiring beans</a></summary>
            
            <ol>
                <li><a href="#31-creating-discoverable-beans">Creating discoverable beans</a></li>
                <li>
                    <details>
                        <summary><a href="#32-wiring-beans-with-java">Wiring beans with Java</a></summary>
                        
                        <ol>
                            <li><a href="#321-creating-a-configuration-class">Creating a configuration class</a></li>
                            <li><a href="#322-injecting-with-javaconfig">Injecting with JavaConfig</a></li>
                        </ol>
                    </details>
                </li>
                <li>
                    <details>
                        <summary><a href="#33-wiring-beans-with-xml">Wiring beans with XML</a></summary>
                        
                        <ol>
                            <li><a href="#331-creating-an-xml-configuration-specification">Creating an XML configuration specification</a></li>
                            <li><a href="#332-declaring-a-simple-bean">Declaring a simple bean</a></li>
                            <li><a href="#333-initializing-a-bean-with-constructor-injection">Initializing a bean with constructor injection</a></li>
                        </ol>
                    </details>
                </li>
                <li>
                    <details>
                        <summary><a href="#34-importing-and-mixing-configurations">Importing and mixing configurations</a></summary>
                        
                        <ol>
                            <li><a href="#341-referencing-xml-configuration-in-javaconfig">Referencing XML configuration in JavaConfig</a></li>
                            <li><a href="#342-referencing-javaconfig-in-xml-configuration">Referencing JavaConfig in XML configuration</a></li>
                        </ol>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    
    <li>
        <details>
            <summary><a href="#4-advanced-wiring">Advanced wiring</a></summary>
            
            <ol>
                <li>
                    <details>
                        <summary><a href="#41-environments-and-profiles">Environments and profiles</a></summary>
                        
                        <ol>
                            <li><a href="#411-configuring-profile-beans">Configuring profile beans</a></li>
                            <li><a href="#412-activating-profiles">Activating profiles</a></li>
                        </ol>
                    </details>
                </li>
                <li><a href="#42-conditional-beans">Conditional beans</a></li>
                <li>
                    <details>
                        <summary><a href="#43-addressing-ambiguity-in-autowiring">Addressing ambiguity in autowiring</a></summary>
                        
                        <ol>
                            <li><a href="#431-designating-a-primary-bean">Designating a primary bean</a></li>
                            <li><a href="#432-qualifying-autowired-beans">Qualifying autowired beans</a></li>
                        </ol>
                    </details>
                </li>
                <li>
                    <details>
                        <summary><a href="#44-scoping-beans">Scoping beans</a></summary>
                        
                        <ol>
                            <li><a href="#441-working-with-request-and-session-scope">Working with request and session scope</a></li>
                            <li><a href="#442-declaring-scoped-proxies-in-xml">Declaring scoped proxies in XML</a></li>
                        </ol>
                    </details>
                </li>
                <li>
                    <details>
                        <summary><a href="#45-runtime-value-injection">Runtime value injection</a></summary>
                        
                        <ol>
                            <li><a href="#451-injecting-external-values">Injecting external values</a></li>
                            <li><a href="#452-wiring-with-the-spring-expression-language">Wiring with the Spring Expression Language</a></li>
                        </ol>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    
    <li>
        <details>
            <summary><a href="#5-aspect-oriented-spring">Aspect-oriented Spring</a></summary>
            
            <ol>
                <li>
                    <details>
                        <summary><a href="#51-what-is-aspect-oriented-programming">What is aspect-oriented programming?</a></summary>
                        
                        <ol>
                            <li><a href="#511-defining-aop-terminology">Defining AOP terminology</a></li>
                            <li><a href="#512-springs-aop-support">Spring’s AOP support</a></li>
                        </ol>
                    </details>
                </li>
                
                <li>
                    <details>
                        <summary><a href="#52-selecting-join-points-with-pointcuts">Selecting join points with pointcuts</a></summary>
                        
                        <ol>
                            <li><a href="#521-writing-pointcuts">Writing pointcuts</a></li>
                            <li><a href="#522-selecting-beans-in-pointcuts">Selecting beans in pointcuts</a></li>
                        </ol>
                    </details>
                </li>
                
                <li>
                    <details>
                        <summary><a href="#53-creating-annotated-aspects">Creating annotated aspects</a></summary>
                        
                        <ol>
                            <li><a href="#531-defining-an-aspect">Defining an aspect</a></li>
                            <li><a href="#532-creating-around-advice">Creating around advice</a></li>
                            <li><a href="#533-handling-parameters-in-advice">Handling parameters in advice</a></li>
                            <li><a href="#534-annotating-introductions">Annotating introductions</a></li>
                        </ol>
                    </details>
                </li>
                
                <li>
                    <details>
                        <summary><a href="#54-declaring-aspects-in-xml">Declaring aspects in XML</a></summary>
                        
                        <ol>
                            <li><a href="#541-declaring-before-and-after-advice">Declaring before and after advice</a></li>
                            <li><a href="#542-declaring-around-advice">Declaring around advice</a></li>
                            <li><a href="#543-passing-parameters-to-advice">Passing parameters to advice</a></li>
                            <li><a href="#544-introducing-new-functionality-with-aspects">Introducing new functionality with aspects</a></li>
                        </ol>
                    </details>
                </li>
                
                <li>
                    <a href="#55-injecting-aspectj-aspects">Injecting AspectJ aspects</a>
                </li>
            </ol>
        </details>
    </li>
    
    <li>
        <details>
            <summary><a href="#6-building-spring-web-applications">Building Spring web applications</a></summary>
            
            <ol>
                <li>
                    <details>
                        <summary><a href="#61-getting-started-with-spring-mvc">Getting started with Spring MVC</a></summary>
                        
                        <ol>
                            <li><a href="#611-following-the-life-of-a-request">Following the life of a request</a></li>
                        </ol>
                    </details>
                </li>
            </ol>
        </details>
    </li>
</ol>
<ol type="A">
    <li>
        <a href="#a-notes">Notes</a>
    </li>
</ol>

<h1>1. Dipendency Injection (DI)</h1>
<p>
<b>With DI, objects are given their dependencies at creation time by some third party that coordinates each object in the system. Objects aren’t expected to create or obtain their dependencies.</b>
That’s the key benefit of DI—loose coupling. If an object only knows about its dependencies by their interface (not by their implementation or how they’re instantiated), then the dependency can be swapped out with a different implementation without the depending object knowing the difference.

<h2>1.1 Constructor Injection</h2>
<p>
<em>Constructor Injection: Page 6.</em>
</p>
<p>
BraveKnight doesn’t create his own quest. Instead, he’s given a quest at construction time as a constructor argument. This is a type of DI known as constructor injection.
</p>

```java
public class BraveKnight implements Knight {
    private Quest quest;
    
    public BraveKnight(Quest quest) {
        this.quest = quest;
    }
    
    public void embarkOnQuest() {
        quest.embark();
    }
}
```

<h2>1.2 Configuration</h2>
<p>

<p>
Configuration can be done both XML way and Java way.
</p>
<p>
XML way: define a configuration file for the bean.
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="knight" class="com.springinaction.knights.BraveKnight">
    <constructor-arg ref="quest" />
    </bean>
    
    <bean id="quest" class="com.springinaction.knights.SlayDragonQuest">
    <constructor-arg value="#{T(System).out}" />
    </bean>

</beans>
```

<p>
Java way: define a configuration class for the bean.
</p>

```java
@Configuration
public class KnightConfig {

	@Bean
	public Knight knight() {
		return new BraveKnight(quest());
	}

	@Bean
	public Quest quest() {
		return new SlayDragonQuest(System.out);
	}

}
```

<p>
Get the bean loading the context.
</p>

```java
public class KnightMain {
    public static void main(String[] args) throws Exception {
    	ApplicationContext context = new AnnotationConfigApplicationContext(KnightConfig.class);
    	Knight knight = context.getBean(Knight.class);
    	knight.embarkOnQuest();
    }
}
```

</p>

<h1>2. Aspect-Oriented Programming (AOP)</h1>
<p>
Although DI makes it possible to tie software components together loosely, <b>aspect-oriented programming (AOP) enables you to capture functionality that’s used throughout your application in reusable components.</b><br>
AOP is often defined as a technique that promotes separation of concerns in a software system. Systems are composed of several components, each responsible for a specific piece of functionality. But often these components also carry additional responsibilities beyond their core functionality. System services such as logging, transaction management, and security often find their way into components whose core responsibilities is something else. These system services are commonly referred to as cross-cutting concerns because they tend to cut across multiple components in a system.
</p>

<h1>3. Wiring beans</h1>
<p>
Spring is incredibly flexible, offering three primary wiring mechanisms:
<ul>
<li>Explicit configuration in XML</li>
<li>Explicit configuration in Java</li>
<li>Implicit bean discovery and automatic wiring</li>
</ul>

<h2>3.1 Creating discoverable beans</h2>
<p>
<p>
The following listing shows CompactDisc, an interface that defines a CD.
</p>

```java
public interface CompactDisc {
	void play();
}
```

<p>
The specifics of the <em>CompactDisc</em> interface aren’t important. What is important is that you’ve defined it as an interface. As an interface, it defines the contract through which a CD player can operate on the CD. And it keeps the coupling between any CD player implementation and the CD itself to a minimum.<br>
You still need an implementation of <em>CompactDisc</em>, though. In fact, you could have several <em>CompactDisc</em> implementations. In this case, you’ll start with one: the <em>SgtPeppers</em> class, as shown in the next listing.
</p>

```java
@Component
public class SgtPeppers implements CompactDisc {

	private String title = "Sgt. Pepper's Lonely Hearts Club Band";
	private String artist = "The Beatles";

	@Override
	public void play() {
		System.out.println("Playing " + title + " by " + artist);
	}

}
```

<p>
As with the <em>CompactDisc</em> interface, the specifics of <em>SgtPeppers</em> aren’t important to this discussion. What you should take note of is that <em>SgtPeppers</em> is annotated with <em><code>@Component</code></em>. This simple annotation identifies this class as a component class and serves as a clue to Spring that a bean should be created for the class. There’s no need to explicitly configure a <em>SgtPeppers</em> bean; Spring will do it for you because this class is annotated with <em><code>@Component</code></em>.<br>
Component scanning isn’t turned on by default, however. You’ll still need to write an explicit configuration to tell Spring to seek out classes annotated with <em><code>@Component</code></em> and to create beans from them. The configuration class in the following listing shows the minimal configuration to make this possible.
</p>

```java
@Configuration
@ComponentScan(basePackages = "com.springinaction.soundsystem")
public class CDPlayerConfig {
}
```

<p>
To test that component scanning works, let’s write a simple JUnit test that creates a Spring application context and asserts that the <em>CompactDisc</em> bean is, in fact, created.
</p>

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CDPlayerConfig.class)
public class CDPlayerTest {

	@Autowired
	private CompactDisc cd;

	@Test
	public void cdShouldNotBeNull() {
		assertNotNull(cd);
	}

}
```

<p>
If there are no matching beans, Spring will throw an exception as the application context is being created. To avoid that exception, you can set the <em>required</em> attribute on <em><code>@Autowired</code></em> to <em>false</em>:
</p>

```java
@Autowired(required = false)
public CDPlayer(CompactDisc cd) {
    this.cd = cd;
}
```

<p>
When <em>required</em> is <em>false</em>, Spring will attempt to perform autowiring; but if there are no matching beans, it will leave the bean unwired. You should be careful setting <em>required</em> to <em>false</em>, however. Leaving the property unwired could lead to <em>NullPointer-Exception</em>s if you don’t check for <em>null</em> in your code.
</p>
<p>
You can use <em><code>@Inject</code></em> annotation for autowiring. Although there are some subtle differences between <em><code>@Inject</code></em> and <em><code>@Autowired</code></em>, they’re interchangeable in many cases.
</p>

</p>

<h2>3.2 Wiring beans with Java</h2>
<p>

<p>
Although automatic Spring configuration with component scanning and automatic wiring is preferable in many cases, there are times when automatic configuration isn’t an option and you must configure Spring explicitly. For instance, let’s say that you want to wire components from some third-party library into your application. Because you don’t have the source code for that library, there’s no opportunity to annotate its classes with <em><code>@Component</code></em> and <em><code>@Autowired</code></em>. Therefore, automatic configuration isn’t an option.<br>
Two choices for explicit configuration: Java and XML.
</p>

<h3>3.2.1 Creating a configuration class</h3>
<p>

<p>
The key to creating a JavaConfig class is to annotate it with <em><code>@Configuration</code></em>. The <em><code>@Configuration</code></em> annotation identifies this as a configuration class, and it’s expected to contain details on beans that are to be created in the Spring application context.
</p>

```java
@Configuration
public class CDPlayerConfig {
}
```

<p>
To declare a bean in JavaConfig, you write a method that creates an instance of the desired type and annotate it with <em><code>@Bean</code></em>. For example, the following method declares the <em>CompactDisc</em> bean:
</p>

```java
@Bean
public CompactDisc sgtPeppers() {
    return new SgtPeppers();
}
```

<p>
The <em><code>@Bean</code></em> annotation tells Spring that this method will return an object that should be registered as a bean in the Spring application context. The body of the method contains logic that ultimately results in the creation of the bean instance.<br>
By default, the bean will be given an ID that is the same as the <em><code>@Bean</code></em>-annotated method’s name. In this case, the bean will be named <em>compactDisc</em>. If you’d rather it have a different name, you can either rename the method or prescribe a different name with the <em>name</em> attribute:
</p>

```java
@Bean(name="lonelyHeartsClubBand")
public CompactDisc sgtPeppers() {
    return new SgtPeppers();
}
```

<h3>3.2.2 Injecting with JavaConfig</h3>
<p>
The simplest way to wire up beans in JavaConfig is to refer to the referenced bean's method.<br>
</p>

```java
@Bean
public CDPlayer cdPlayer() {
    return new CDPlayer(sgtPeppers());
}

@Bean
public CDPlayer anotherCDPlayer() {
    return new CDPlayer(sgtPeppers());
}
```

<p>
By default, all beans in Spring are singletons, and there's no reason you need to create a duplicate instance. So Spring intercepts all calls to <em>sgtPeppers()</em> and makes sure that what is returned is the Spring bean that was created when Spring itself called <em>sgtPeppers()</em> to create the <em>CompactDisc</em> bean.
</p>
<p>
Referring to a bean by calling its method can be confusing. There’s another way that might be easier to digest:
</p>

```java
@Bean
public CDPlayer cdPlayer(CompactDisc compactDisc) {
	return new CDPlayer(compactDisc);
}
```

<p>
Here, the <em>cdPlayer()</em> method asks for a <em>CompactDisc</em> as a parameter. When Spring calls <em>cdPlayer()</em> to create the <em>CDPlayer</em> bean, it autowires a <em>CompactDisc</em> into the configuration method. Then the body of the method can use it however it sees fit. With this technique, the <em>cdPlayer()</em> method can still inject the <em>CompactDisc</em> into the <em>CDPlayer</em>’s constructor without explicitly referring to the <em>CompactDisc</em>’s <em><code>@Bean</code></em> method.
</p>

</p>

</p>

<h2>3.3 Wiring beans with XML</h2>
<p>

<p>
There’s another option for bean wiring that, although less desirable, has a long history with Spring.<br>
Since the beginning of Spring, XML has been the primary way of expressing configuration.
</p>

<h3>3.3.1 Creating an XML configuration specification</h3>
<p>

<p>
The simplest possible Spring XML configuration looks like this:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd
    http://www.springframework.org/schema/context">
    <!-- configuration details go here -->
</beans>
```

<p>
An easy way to create and manage Spring XML configuration files is to use Spring Tool Suite (https://spring.io/tools/sts). Select File > New > Spring Bean Configuration File from Spring Tool Suite’s menu to create a Spring XML configuration file, and select from one of the available configuration namespaces.
</p>

</p>

<h3>3.3.2 Declaring a simple bean</h3>
<p>

<p>
Declare a bean in Spring's XML-based configuration:
</p>

```xml
<bean id="compactDisc" class="soundsystem.SgtPeppers" />
```

<p>
You’ll use this explicit name in a moment when you wire this bean into the <em>CDPlayer</em> bean.
</p>

</p>

<h3>3.3.3 Initializing a bean with constructor injection</h3>
<p>

<p>
There’s only one way to declare a bean in Spring XML configuration: use the <em>bean</em> element, and specify a class attribute.
</p>
<p>
Continue to book's page 49.
</p>

</p>

</p>

<h2>3.4 Importing and mixing configurations</h2>
<p>

<p>
In a typical Spring application, you’re likely to need to use both automatic and explicit configuration. And even if you favor JavaConfig for explicit configuration, there may be times when XML configuration is the best choice.<br>
Fortunately, none of the configuration options available in Spring are mutually exclusive. You’re free to mix component scanning and autowiring with JavaConfig and/or XML configuration.<br>
The first thing to know about mixing configuration styles is that when it comes to autowiring, it doesn’t matter where the bean to be wired comes from. Autowiring considers all beans in the Spring container, regardless of whether they were declared in JavaConfig or XML or picked up by component scanning.
</p>

<h3>3.4.1 Referencing XML configuration in JavaConfig</h3>
<p>

<p>
Use <em><code>@Import</code></em> and <em><code>@ImportResource</code></em> annotations to load Java-based configuration and XML-based configuration:
</p>

```java
@Configuration
@Import(CDPlayerConfig.class)
@ImportResource("classpath:cd-config.xml")
    public class SoundSystemConfig {
}
```

</p>

<h3>3.4.2 Referencing JavaConfig in XML configuration</h3>
<p>

<p>
You can use the <em>import</em> element to split up the XML configuration.<br>
To import a JavaConfig class into an XML configuration, you declare it as a bean like this:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:c="http://www.springframework.org/schema/c"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd">
    
    <bean class="soundsystem.CDConfig" />
    
    <bean id="cdPlayer"
        class="soundsystem.CDPlayer"
        c:cd-ref="compactDisc" />
</beans>
```

<p>
You might consider creating a higherlevel configuration file that doesn’t declare any beans but that brings two or more configurations together:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:c="http://www.springframework.org/schema/c"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd">
    
    <bean class="soundsystem.CDConfig" />
    
    <import resource="cdplayer-config.xml" />
</beans>
```

</p>
</p>
</p>

<h1>4. Advanced wiring</h1>
<p>

<p>
Spring has several other tricks up its sleeve for more advanced bean wiring. You won’t get as much day-to-day use out of the techniques in this chapter, but that doesn’t mean they’re any less valuable.
</p>

<h2>4.1 Environments and profiles</h2>
<p>

<p>
One of the most challenging things about developing software is transitioning an application from one environment to another. Spring has a solution that doesn’t require a rebuild.
</p>

<h3>4.1.1 Configuring profile beans</h3>
<p>

<p>
Spring’s solution for environment-specific beans isn’t much different from build-time solutions. Certainly, an environment-specific decision is made as to which beans will and won’t be created. But rather than make that decision at build time, Spring waits to make the decision at runtime. Consequently, the same deployment unit (perhaps a WAR file) will work in all environments without being rebuilt.<br>
In Java configuration, you can use the @Profile annotation to specify which profile a bean belongs to.
</p>

```java
@Configuration
@Profile("dev")
public class DevelopmentProfileConfig {

    @Bean(destroyMethod="shutdown")
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .addScript("classpath:schema.sql")
            .addScript("classpath:test-data.sql")
            .build();
    }
    
}
```

<p>
The <em><code>@Profile</code></em> annotation tells Spring hat the beans in this configuration class should be created only if the dev profile is active. If the dev profile isn’t active, then the @Bean methods will be ignored.<br>
In Spring 3.1, you could only use the <em><code>@Profile</code></em> annotation at the class level. Starting with Spring 3.2, however, you can use <em><code>@Profile</code></em> at the method level, alongside the <em><code>@Bean</code></em> annotation.
</p>

```java
@Configuration
public class DataSourceConfig {

    @Bean(destroyMethod="shutdown")
    @Profile("dev")                             // Wired for "dev" profile
    public DataSource embeddedDataSource() {
        return new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .addScript("classpath:schema.sql")
            .addScript("classpath:test-data.sql")
            .build();
    }
    
    @Bean
    @Profile("prod")                             // Wired for "prod" profile
    public DataSource jndiDataSource() {
        JndiObjectFactoryBean jndiObjectFactoryBean =
            new JndiObjectFactoryBean();
        jndiObjectFactoryBean.setJndiName("jdbc/myDS");
        jndiObjectFactoryBean.setResourceRef(true);
        jndiObjectFactoryBean.setProxyInterface(javax.sql.DataSource.class);
        return (DataSource) jndiObjectFactoryBean.getObject();
    }
}
```

<p>
You can also configure profiled beans in XML by setting the profile attribute of the <em><code>beans</code></em> element.
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:jdbc="http://www.springframework.org/schema/jdbc"
    xmlns:jee="http://www.springframework.org/schema/jee"
    xmlns:p="http://www.springframework.org/schema/p"
    xsi:schemaLocation="http://www.springframework.org/schema/jee
        http://www.springframework.org/schema/jee/spring-jee.xsd
        http://www.springframework.org/schema/jdbc
        http://www.springframework.org/schema/jdbc/spring-jdbc.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">
    
    <!-- "dev" profile bean -->
    <beans profile="dev">
        <jdbc:embedded-database id="dataSource">
            <jdbc:script location="classpath:schema.sql" />
            <jdbc:script location="classpath:test-data.sql" />
        </jdbc:embedded-database>
    </beans>
    
    <!-- "prod" profile bean -->
    <beans profile="prod">
        <jee:jndi-lookup id="dataSource"
            jndi-name="jdbc/myDatabase"
            resource-ref="true"
            proxy-interface="javax.sql.DataSource" />
    </beans>
    
</beans>
```

</p>

<h3>4.1.2 Activating profiles</h3>
<p>

<p>
Spring honors two separate properties when determining which profiles are active: <em>spring.profiles.active</em> and <em>spring.profiles.default</em>. If <em>spring.profiles.active</em> is set, then its value determines which profiles are active. But if <em>spring.profiles.active</em> isn’t set, then Spring looks to <em>spring.profiles.default</em>. If neither <em>spring.profiles.active</em> nor <em>spring.profiles.default</em> is set, then there are no active profiles, and only those beans that aren’t defined as being in a profile are created.<br>
There are several ways to set these properties:
</p>
<ul>
    <li>As initialization parameters on DispatcherServlet</li>
    <li>As context parameters of a web application</li>
    <li>As JNDI entries</li>
    <li>As environment variables</li>
    <li>As JVM system properties</li>
    <li>Using the @ActiveProfiles annotation on an integration test class</li>
</ul>
<p>
For example, a web application’s web.xml file might set <em>spring.profiles.default</em>:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="2.5"
    xmlns="http://java.sun.com/xml/ns/javaee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
        http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">
    
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>/WEB-INF/spring/root-context.xml</param-value>
    </context-param>
    
    <context-param>
        <param-name>spring.profiles.default</param-name>
        <param-value>dev</param-value>
    </context-param>
    
    <listener>
        <listener-class>
            org.springframework.web.context.ContextLoaderListener
        </listener-class>
    </listener>
    
    <servlet>
        <servlet-name>appServlet</servlet-name>
        <servlet-class>
            org.springframework.web.servlet.DispatcherServlet
        </servlet-class>
        <init-param>
            <param-name>spring.profiles.default</param-name>
            <param-value>dev</param-value>
        </init-param>
        <load-on-startup>1</load-on-startup>
    </servlet>
    
    <servlet-mapping>
        <servlet-name>appServlet</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    
</web-app>
```

<p>
With <em>spring.profiles.default</em> set this way, any developer can retrieve the application code from source control and run it using development settings (such as an embedded database) without any additional configuration.<br>
Then, when the application is deployed in a QA, production, or other environment, the person responsible for deploying it can set <em>spring.profiles.active</em> using system properties, environment variables, or JNDI as appropriate. When <em>spring.profiles.active</em> is set, it doesn’t matter what <em>spring.profiles.default</em> is set to; the profiles set in <em>spring.profiles.active</em> take precedence.<br>
Spring offers the <en><code>@ActiveProfiles</code></em> annotation to let you specify which profile(s) should be active when a test is run. Often it’s the development profile that you’ll want to activate during an integration test. For example, here’s a snippet of a test class that uses <em><code>@ActiveProfiles</code></em> to activate the dev profile:
</p>

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={PersistenceTestConfig.class})
@ActiveProfiles("dev")
public class PersistenceTest {
    // code
}
```

<p>
Spring 4 offers a more general-purpose mechanism for conditional bean definitions.
</p>

</p>
</p>

<h2>4.2 Conditional beans</h2>
<p>

<p>
You want a bean to be created if and only if a specific environment variable is set. Spring 4 introduced a new <em><code>@Conditional</code></em> annotation that can be applied to <em><code>@Bean</code></em> methods. If the prescribed condition evaluates to <em>true</em>, then the bean is created. Otherwise the bean is ignored.<br>
Configuration class:
</p>

```java
@Configuration
public class MagicConfig {

    @Bean("magicBean")
	@Conditional(MagicExistsCondition.class)
	public MagicBean magicBean() {
		System.out.println("magicBean()");
		return new MagicBean();
	}

}
```

<p>
<em><code>@Conditional</code></em> is given a Class that specifies the condition—in this case, MagicExistsCondition. <em><code>@Conditional</code></em> comes paired with a Condition interface:
</p>

```java
public interface Condition {
    boolean matches(ConditionContext ctxt,
        AnnotatedTypeMetadata metadata);
}
```

<p>
If the <em>matches()</em> method returns <em>true</em>, then the <em><code>@Conditional</code></em>-annotated beans are created. If <em>matches()</em> returns <em>false</em>, then those beans aren’t created.<br>
You need to create an implementation of <em>Condition</em> that hinges its decision on the presence of a <em>magic</em> property in the environment.
</p>

```java
public class MagicExistsCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		Environment env = context.getEnvironment();
		return env.containsProperty("magic");   // Check for "magic" property
	}

}
```

<p>
For this example, the value of the property is irrelevant; it only needs to exist. If the property doesn’t exist, the condition will fail, <em>false</em> will be returned from <em>matches()</em>, and none of those beans will be created.<br>
The MagicConfig class with two bean condition definition would be like this:
</p>

```java
@Configuration
public class MagicConfig {

	@Bean("magicBean")
	@Conditional(MagicExistsCondition.class)
	public MagicBean magicBean() {
		return new MagicBean();
	}

	@Bean("magicBean")
	@Conditional(MagicExistsTestCondition.class)
	public MagicBean magicBeanTest() {
		return new MagicBean();
	}

}
```

<p>
<em>Note:</em> Spring handles <em>Bean</em>s as singleton(s), so if both conditions (<em>MagicExistsCondition</em> and <em>MagicExistsTestCondition</em>) are matched, it instantiates the <em>magicBean</em> calling <em>magicBean()</em> method and the other one (<em>magicBeanTest()</em>) is ignored.
</p>

</p>

<h2>4.3 Addressing ambiguity in autowiring</h2>
<p>

<p>
Autowiring only works when exactly one bean matches the desired result. When there’s more than one matching bean, the ambiguity prevents Spring from autowiring the property, constructor argument, or method parameter.
Suppose you’ve annotated the following <em>setDessert()</em> method with <em><code>@Autowired</code></em>:
</p>

```java
@Autowired
public void setDessert(Dessert dessert) {
    this.dessert = dessert;
}
```

<p>
In this example, <em>Dessert</em> is an interface and is implemented by three classes: <em>Cake</em>, <em>Cookies</em>, and <em>IceCream</em>:
</p>

```java
@Component
public class Cake implements Dessert { ... }

@Component
public class Cookies implements Dessert { ... }

@Component
public class IceCream implements Dessert { ... }
```

<p>
Because all three implementations are annotated by <em><code>@Component</code></em>, they’re all picked up during component-scanning and created as beans in the Spring application context. Then, when Spring tries to autowire the <em>Dessert</em> parameter in <em>setDessert()</em>, it doesn’t have a single, unambiguous choice. Spring has no option but to fail and throw an exception.<br>
Spring offers a couple of options. You can declare one of the candidate beans as the primary choice, or you can use qualifiers to help Spring narrow its choices to a single candidate.
</p>

<h3>4.3.1 Designating a primary bean</h3>
<p>

<p>
When declaring beans, you can avoid autowiring ambiguity by designating one of the candidate beans as a primary bean.<br>
You can express your favorite choice in Spring using <em><code>@Primary</code></em> annotation. <em><code>@Primary</code></em> can be used either alongside <em><code>@Component</code></em> for beans that are component-scanned or alongside <em><code>@Bean</code></em> for beans declared in Java configuration.
</p>

```java
@Component
@Primary
public class IceCream implements Dessert { ... }
```

<p>
When more than one bean is designated as primary, there are no primary candidates.
</p>
</p>

<h3>4.3.2 Qualifying autowired beans</h3>
<p>

<p>
The <em><code>@Qualifier</em></code> annotation is the main way to work with qualifiers. It can be applied alongside <em><code>@Autowired</code></em> or <em><code>@Inject</code></em> at the point of injection to specify which bean you want to be injected. For example, let’s say you want to ensure that the <em>IceCream</em> bean is injected into <em>setDessert()</em>:
</p>

```java
@Autowired
@Qualifier("iceCream")
public void setDessert(Dessert dessert) {
    this.dessert = dessert;
}
```

<p>
The parameter given to <em><code>@Qualifier</code></em> is the ID of the bean that you want to inject. All <em><code>@Component</code></em>-annotated classes will be created as beans whose ID is the uncapitalized class name. Therefore, <em><code>@Qualifier("iceCream")</code></em> refers to the bean created when component-scanning created an instance of the <em>IceCream</em> class. To be more precise, <em><code>@Qualifier("iceCream")</code></em> refers to the bean that has the <em>String</em> “iceCream” as a qualifier.<br>
Basing qualification on the default bean ID qualifier is simple but can pose some problems. What do you suppose would happen if you refactored the <em>IceCream</em> class, renaming it <em>Gelato</em>? In that case, the bean’s ID and default qualifier would be <em>gelato</em>, which doesn’t match the qualifier on <em>setDessert()</em>. Autowiring would fail.<br>
The problem is that you specified a qualifier on <em>setDessert()</em> that is tightly coupled to the class name of the bean being injected. Any change to that class name will render the qualifier ineffective.
</p>

<h5>CREATING CUSTOM QUALIFIERS</h5>
<p>
Instead of relying on the bean ID as the qualifier, you can assign your own qualifier to a bean:
</p>

```java
@Component
@Qualifier("cold")
public class IceCream implements Dessert { ... }
```

```java
@Autowired
@Qualifier("cold")
public void setDessert(Dessert dessert) {
    this.dessert = dessert;
}
```

<p>
It’s worth noting that <em><code>@Qualifier</code></em> can also be used alongside the <em><code>@Bean</code></em> annotation when explicitly defining beans with Java configuration:
</p>

```java
@Bean
@Qualifier("cold")
public Dessert iceCream() {
    return new IceCream();
}
```

<h5>DEFINING CUSTOM QUALIFIER ANNOTATIONS</h5>
<p>
Trait-oriented qualifiers still run into trouble when you have multiple beans that share common traits:
</p>

```java
@Component
@Qualifier("cold")
public class Popsicle implements Dessert { ... }
```

<p>
Once again you’re faced with ambiguity in autowiring dessert beans. You need more qualifiers to narrow the selection to a single bean.<br>
Perhaps the solution is to tack on another <em><code>@Qualifier</code></em> at both the injection point and at the bean definition. Maybe the <em>IceCream</em> class could look like this:
</p>

```java
@Component
@Qualifier("cold")
@Qualifier("creamy")
public class IceCream implements Dessert { ... }
```

<p>
There’s only one small problem: Java doesn’t allow multiple annotations of the same type to be repeated on the same item.<br>
What you can do, however, is create custom qualifier annotations to represent the traits you want your beans to be qualified with. All you have to do is create an annotation that is itself annotated with <em><code>@Qualifier</code></em>. Rather than use <em><code>@Qualifier("cold")</code></em>, you can use a custom <em><code>@Cold</code></em> annotation that’s defined like this:
</p>

```java
@Target({ElementType.CONSTRUCTOR, ElementType.FIELD,
ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Cold { }
```

```java
@Target({ElementType.CONSTRUCTOR, ElementType.FIELD,
ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface Creamy { }
```

<p>
Now you can revisit <em>IceCream</em> and annotate it with <em><code>@Cold</code></em> and <em><code>@Creamy</code></em>, like this:
</p>

```java
@Component
@Cold
@Creamy
public class IceCream implements Dessert { ... }
```
<p>
Finally, at the injection point, you can use any combination of qualifier annotations necessary to narrow the selection to the one bean that meets your specifications. To arrive at the <em>IceCream</em> bean, the <em>setDessert()</em> method can be annotated like this:
</p>

```java
@Autowired
@Cold
@Creamy
public void setDessert(Dessert dessert) {
    this.dessert = dessert;
}
```

<p>
By defining custom qualifier annotations, you’re able to use multiple qualifiers together with no limitations or complaints from the Java compiler. Also, your custom annotations are more type-safe than using the raw <em><code>@Qualifier</code></em> annotation and specifying the qualifier as a <em>String</em>.
</p>

</p>
</p>

<h2>4.4 Scoping beans</h2>
<p>

<p>
All beans created in the Spring application context are created as singletons. But sometimes you may find yourself working with a mutable class that does maintain some state and therefore isn’t safe for reuse. In that case, declaring the class as a singleton bean probably isn’t a good idea because that object can be tainted and create unexpected problems when reused later.<br>
Spring defines several scopes under which a bean can be created, including the following:
</p>
<ul>
    <li><em>Singleton</em>—One instance of the bean is created for the entire application.</li>
    <li><em>Prototype</em>—One instance of the bean is created every time the bean is injected into or retrieved from the Spring application context.</li>
    <li><em>Session</em>—In a web application, one instance of the bean is created for each session.</li>
    <li><em>Request</em>—In a web application, one instance of the bean is created for each request.</li>
</ul>
<p>
Singleton scope is the default scope, but as we’ve discussed, it isn’t ideal for mutable types. To select an alternative type, you can use the <em><code>@Scope</code></em> annotation, either in conjunction with the <em><code>@Component</code></em> annotation or with the <em><code>@Bean</code></em> annotation.
</p>

```java
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Notepad { ... }
```

<p>
Regardless of how you specify prototype scope, an instance of the bean will be created each and every time it’s injected into or retrieved from the Spring application context. Consequently, everyone gets their own instance of <em>Notepad</em>.
</p>

<h3>4.4.1 Working with request and session scope</h3>
<p>

<p>
In a web application, it may be useful to instantiate a bean that’s shared within the scope of a given request or session. For instance, in a typical e-commerce application, you may have a bean that represents the user’s shopping cart. If the shopping cart bean is a singleton, then all users will be adding products to the same cart. On the other hand, if the shopping cart is prototype-scoped, then products added to the cart in one area of the application may not be available in another part of the application where a different prototype-scoped shopping cart was injected.<br>
In the case of a shopping cart bean, session scope makes the most sense, because it’s most directly attached to a given user.
</p>

```java
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION, proxyMode=ScopedProxyMode.INTERFACES)
public ShoppingCart cart() { ... }
```

<p>
This tells Spring to create an instance of the <em>ShoppingCart</em> bean for each session in a web application.<br>
Notice that <em><code>@Scope</code></em> also has a <em>proxyMode</em> attribute set to <em>ScopedProxyMode.INTERFACES</em>. This attribute addresses a problem encountered when injecting a session- or request-scoped bean into a singleton-scoped bean. Let’s look at a scenario that presents the problem that <em>proxyMode</em> addresses.<br>
Suppose you want to inject the <em>ShoppingCart</em> bean into the following setter method on a singleton <em>StoreService</em> bean:
</p>

```java
@Component
public class StoreService {
    
    @Autowired
    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
    
    ...
}
```

<p>
Because <em>StoreService</em> is a singleton bean, it will be created as the Spring application context is loaded. As it’s created, Spring will attempt to inject <em>ShoppingCart</em> into the <em>setShoppingCart()</em> method. But the <em>ShoppingCart</em> bean, being session scoped, doesn’t exist yet. There won’t be an instance of <em>ShoppingCart</em> until a user comes along and a session is created.<br>
You want <em>StoreService</em> to work with the <em>ShoppingCart</em> instance for whichever session happens to be in play when <em>StoreService</em> needs to work with the shopping cart.<br>
Instead of injecting the actual <em>ShoppingCart</em> bean into <em>StoreService</em>, Spring should inject a proxy to the <em>ShoppingCart</em> bean. This proxy will expose the same methods as <em>ShoppingCart</em> so that for all <em>StoreService</em> knows, it is the shopping cart. But when <em>StoreService</em> calls methods on <em>ShoppingCart</em>, the proxy will lazily resolve it and delegate the call to the actual session-scoped <em>ShoppingCart</em> bean.<br>
As configured, <em>proxyMode</em> is set to <em>ScopedProxyMode.INTERFACES</em>, indicating that the proxy should implement the <em>ShoppingCart</em> interface and delegate to the implementation bean.<br>
This is fine (and the most ideal proxy mode) as long as <em>ShoppingCart</em> is an interface and not a class. But if <em>ShoppingCart</em> is a concrete class, there’s no way Spring can create an interface-based proxy. Instead, it must use CGLib to generate a class-based proxy. So, if the bean type is a concrete class, you must set <em>proxyMode</em> to <em>ScopedProxyMode.TARGET_CLASS</em> to indicate that the proxy should be generated as an extension of the target class.
</p>
</p>

<h3>4.4.2 Declaring scoped proxies in XML</h3>
<p>

<p>
To set the proxy mode, you must use a new element from Spring’s aop namespace:
</p>

```xml
<bean id="cart"
        class="com.myapp.ShoppingCart"
        scope="session">
    <aop:scoped-proxy />
</bean>
```

<p>
<em>aop:scoped-proxy</em> is the Spring XML configuration’s counterpart to the @Scope
annotation’s proxyMode attribute. It tells Spring to create a scoped proxy for the bean.
By default, it uses CGLib to create a target class proxy. But you can ask it to generate an
interface-based proxy by setting the proxy-target-class attribute to false:
</p>

```xml
<bean id="cart"
        class="com.myapp.ShoppingCart"
        scope="session">
    <aop:scoped-proxy proxy-target-class="false" />
</bean>
```

<p>
In order to use the <em>aop:scoped-proxy</em> element, you must declare Spring’s <em>aop</em> namespace in your XML configuration:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xsi:schemaLocation="
        http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">
    ...
</beans>
```

</p>
</p>

<h2>4.5 Runtime value injection</h2>
<p>

<p>
Sometimes hard-coded values are fine. Other times, however, you may want to avoid hard-coded values and let the values be determined at runtime. For those cases, Spring offers two ways of evaluating values at runtime:
<ul>
    <li>Property placeholders</li>
    <li>The Spring Expression Language (SpEL)</li>
</ul>
The application of these two techniques is similar, although their purposes and behavior are different.
</p>

<h3>4.5.1 Injecting external values</h3>
<p>

<p>
The simplest way to resolve external values in Spring is to declare a property source and retrieve the properties via the Spring <em>Environment</em>:
</p>

```java
@Configuration
@PropertySource("classpath:/com/soundsystem/app.properties") // Declare a property source
public class ExpressiveConfig {
    
    @Autowired
    Environment env;
    
    @Bean
    public BlankDisc disc() {
        return new BlankDisc(
            env.getProperty("disc.title"),
            env.getProperty("disc.artist"));    // Retrieve property values
    }
    
}
```

<p>
This properties file is loaded into Spring’s <em>Environment</em>, from which it can be retrieved later. Meanwhile, in the <em>disc()</em> method, a new <em>BlankDisc</em> is created; its constructor arguments are resolved from the properties file by calling <em>getProperty()</em>.
</p>

<h4>DIGGING INTO SPRING’S ENVIRONMENT</h4>
<p>
While we’re on the subject of <em>Environment</em>, you might find it helpful to know that the <em>getProperty()</em> method isn’t the only method you can use to fetch a property value. <em>getProperty()</em> is overloaded into four variations:
<ul>
    <li>String getProperty(String key)</li>
    <li>String getProperty(String key, String defaultValue)</li>
    <li>T getProperty(String key, Class<T> type)</li>
    <li>T getProperty(String key, Class<T> type, T defaultValue)</li>
</ul>
The first two forms of getProperty() always return a String value. But you can tweak the <em><code>@Bean</code></em> method slightly to work with default values if the specified properties don’t exist:
</p>

```java
@Bean
public BlankDisc disc() {
    return new BlankDisc(
        env.getProperty("disc.title", "Rattle and Hum"),
        env.getProperty("disc.artist", "U2"));
}
```

<p>
The second two forms of <em>getProperty()</em> work much like the first two, but they recognize that not all values may be <em>Strings</em>:
</p>

```java
int connectionCount = env.getProperty("db.connection.count", Integer.class, 30);
```

<p>
If you use either of the <em>getProperty()</em> methods without specifying a default value, you’ll receive <em>null</em> if the property isn’t defined. If you want to require that the property be defined, you can use <em>getRequiredProperty()</em> like this:
</p>

```java
@Bean
public BlankDisc disc() {
    return new BlankDisc(
        env.getRequiredProperty("disc.title"),
        env.getRequiredProperty("disc.artist"));
}
```

<p>
Finally, if you need to resolve a property into a Class, you can use the <em>getPropertyAsClass()</em> method:
</p>

```java
Class<CompactDisc> cdClass = env.getPropertyAsClass("disc.class", CompactDisc.class);
```

<p>
Environment also offers some methods for checking which profiles are active:
<ul>
    <li><em>String[] getActiveProfiles()</em>—Returns an array of active profile names</li>
    <li><em>String[] getDefaultProfiles()</em>—Returns an array of default profile names</li>
    <li><em>boolean acceptsProfiles(String... profiles)</em>—Returns true if the environment supports the given profile(s)</li>
</ul>
Retrieving properties directly from <em>Environment</em> is handy, especially when you’re wiring beans in Java configuration. But Spring also offers the option of wiring properties with placeholder values that are resolved from a property source.
</p>

<h4>RESOLVING PROPERTY PLACEHOLDERS</h4>
<p>
Spring has always supported the option of externalizing properties into a properties file and then plugging them into Spring beans using placeholder values. In Spring wiring, placeholder values are property names wrapped with ${ ... }.<br>
XML way:
</p>

```xml
<bean id="sgtPeppers"
    class="soundsystem.BlankDisc"
    c:_title="${disc.title}"
    c:_artist="${disc.artist}" />
```

<p>
In this way values are resolved from a source external to the configuration file.<br>
Java way: you can use the <em><code>@Value</code></em> annotation in much the same way as you might use the @Autowired annotation. In the <em>BlankDisc</em> class, for example, the constructor might be written like this:
</p>

```java
public BlankDisc(
        @Value("${disc.title}") String title,
        @Value("${disc.artist}") String artist) {
    this.title = title;
    this.artist = artist;
}
```

<p>
In order to use placeholder values, you must configure either a <em>PropertyPlaceholderConfigurer</em> bean or a <em>PropertySourcesPlaceholderConfigurer</em> bean. Starting with Spring 3.1, <em>PropertySourcesPlaceholderConfigurer</em> is preferred because it resolves placeholders against the Spring <em>Environment</em> and its set of property sources:
</p>

```java
@Bean
public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
}
```

<p>
If you’d rather use XML configuration, the <em>context:property-placeholder</em> element from Spring’s context namespace will give you a <em>PropertySourcesPlaceholderConfigurer</em> bean:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context.xsd">
        
    <context:property-placeholder />
    
</beans>
```

</p>

<h3>4.5.2 Wiring with the Spring Expression Language</h3>
<p>

<p>
Spring 3 introduced Spring Expression Language (SpEL), a powerful yet succinct way of wiring values into a bean’s properties or constructor arguments using expressions that are evaluated at runtime.<br>
SpEL has a lot of tricks up its sleeves, including the following:
<ul>
    <li>The ability to reference beans by their IDs</li>
    <li>Invoking methods and accessing properties on objects</li>
    <li>Mathematical, relational, and logical operations on values</li>
    <li>Regular expression matching</li>
    <li>Collection manipulation</li>
</ul>
SpEL can also be used for purposes other than dependency injection.
</p>

<h4>A FEW SPEL EXAMPLES</h4>
<p>
The first thing to know is that SpEL expressions are framed with <em><code>#{ ... }</code></em>, much as property placeholders are framed with <em><code>${ ... }</code></em>. What follows is possibly one of the simplest SpEL expressions you can write:
</p>

```spel
#{1}
```

<p>
Stripping away the <em><code>#{ ... }</code></em> markers, what’s left is the body of a SpEL expression, which is a numeric constant. It probably won’t surprise you much to learn that this expression evaluates to the numeric value of 1.<br>
You’re more likely to build up more interesting expressions, such as this one:
</p>

```spel
#{T(System).currentTimeMillis()}
```

<p>
Ultimately this expression evaluates to the current time in milliseconds at the moment when the expression is evaluated. The <em>T()</em> operator evaluates <em>java.lang.System</em> as a type so that the <em>staticcurrentTimeMillis()</em> method can be invoked.<br>
SpEL expressions can also refer to other beans or properties on those beans. For example, the following expression evaluates to the value of the <em>artist</em> property on a bean whose ID is <em>sgtPeppers</em>:
</p>

```spel
#{sgtPeppers.artist}
```

<p>
You can also refer to system properties via the <em>systemProperties</em> object:
</p>

```spel
#{systemProperties['disc.title']}
```

<p>
When injecting properties and constructor arguments on beans that are created via component-scanning, you can use the <em><code>@Value</code></em> annotation, much as you saw earlier with property placeholders. Rather than use a placeholder expression, however, you use a SpEL expression. For example, here’s what the <em>BlankDisc</em> constructor might look like, drawing the album title and artist from system properties:
</p>

```java
public BlankDisc(
        @Value("#{systemProperties['disc.title']}") String title,
        @Value("#{systemProperties['disc.artist']}") String artist) {
    this.title = title;
    this.artist = artist;
}
```

<p>
In XML configuration, you can pass in the SpEL expression to the value attribute of <em>property</em> or <em>constructor-arg</em>, or as the value given to a p-namespace or cnamespace entry:
</p>

```xml
<bean id="sgtPeppers"
    class="soundsystem.BlankDisc"
    c:_title="#{systemProperties['disc.title']}"
    c:_artist="#{systemProperties['disc.artist']}" />
```

<h4>EXPRESSING LITERAL VALUES</h4>
<p>
Here's an example of a SpEL expression that is a floating-point value:
</p>

```spel
#{3.14159}
```

<p>
Numbers can also be expressed in scientific notation. For example, the following expression evaluates to 98,700:
</p>

```spel
#{9.87E4}
```

<p>
A SpEL expression can also evaluate literal String values, such as
</p>

```spel
#{'Hello'}
```

<p>
Finally, Boolean literals true and false are evaluated to their Boolean value. For
example,
</p>

```spel
#{false}
```

<h4>REFERENCING BEANS, PROPERTIES, AND METHODS</h4>
<p>
Another basic thing that a SpEL expression can do is reference another bean by its ID.<br>
Let’s say that you want to refer to the artist property of the sgtPeppers bean in an expression:
</p>

```spel
#{sgtPeppers.artist}
```

<p>
In addition to referencing a bean’s properties, you can also call methods on a bean:
</p>

```spel
#{artistSelector.selectArtist()}
```

<p>
You can also call methods on the value returned from the invoked method:
</p>

```spel
#{artistSelector.selectArtist().toUpperCase()}
```

<p>
To guard against a <em>NullPointerException</em>, you can use the type-safe operator:
</p>

```spel
#{artistSelector.selectArtist()?.toUpperCase()}
```

<p>
Instead of a lonely dot (.) to access the <em>toUpperCase()</em> method, now you’re using the <em>?</em>. operator. This operator makes sure the item to its left isn’t null before accessing the thing on its right. So, if <em>selectArtist()</em> returns <em>null</em>, then SpEL won’t even try to invoke <em>toUpperCase()</em>. The expression will evaluate to <em>null</em>.
</p>

<h4>WORKING WITH TYPES IN EXPRESSIONS</h4>
<p>
The key to working with class-scoped methods and constants in SpEL is to use the <em>T()</em> operator. For example, to express Java’s <em>Math</em> class in SpEL, you need to use the <em>T()</em> operator like this:
</p>

```spel
T(java.lang.Math)
```

<p>
The result of the <em>T()</em> operator, as shown here, is a <em>Class</em> object that represents <em>java.lang.Math</em>. You can even wire it into a bean property of type <em<Class</em>, if you want. But the real value of the <em>T()</em> operator is that it gives you access to static methods and constants on the evaluated type.<br>
For example, suppose you need to wire the value of pi into a bean property. The following SpEL expression does the trick:
</p>

```spel
T(java.lang.Math).PI
```

<p>
Similarly, static methods can be invoked in the type resolved with the <em>T()</em> operator.<br>
Here’s another example that evaluates to a random value between 0 and 1:
</p>

```spel
T(java.lang.Math).random()
```

<h4>SPEL OPERATORS</h4>
<table>
    <thead>
        <tr>
            <th><b>Operator type</b></th>
            <th><b>Operators</b></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Arithmetic</td>
            <td>+, -, *, /, %, ^</td>
        </tr>
        <tr>
            <td>Comparison</td>
            <td><, lt, >, gt, ==, eq, <=, le, >=, ge</td>
        </tr>
        <tr>
            <td>Logical</td>
            <td>and, or, not, |</td>
        </tr>
        <tr>
            <td>Conditional</td>
            <td>?: (ternary), ?: (Elvis)</td>
        </tr>
        <tr>
            <td>Regular expression</td>
            <td>matches</td>
        </tr>
    </tbody>
</table>

<p>
As a simple example of using one of these operators, consider the following SpEL expression:
</p>

```spel
#{2 * T(java.lang.Math).PI * circle.radius}
```

<p>
When working with <em>String</em> values, the + operator performs concatenation, just as in Java:
</p>

```spel
#{disc.title + ' by ' + disc.artist}
```

<p>
To compare two numbers for equality, you can use the double-equal (==) operator:
</p>

```spel
#{counter.total == 100}
```

<p>
Or you can use the textual eq operator:
</p>

```spel
#{counter.total eq 100}
```

<p>
Either way, the result is the same.<br>
SpEL also offers a ternary operator that works much like Java’s ternary operator:
</p>

```spel
#{scoreboard.score > 1000 ? "Winner!" : "Loser"}
```

<p>
A common use of the ternary operator is to check for a <em>null</em> value and offer a default value in place of the <em>null</em>:
</p>

```spel
#{disc.title ?: 'Rattle and Hum'}
```

<p>
This expression is commonly referred to as the <em>Elvis</em> operator. This strange name comes from using the operator as an emoticon, where the question mark appears to form the shape of Elvis Presley’s hair style.
</p>

<h4>EVALUATING REGULAR EXPRESSIONS</h4>
<p>
SpEL supports pattern matching in expressions with its <em>matches</em> operator. The <em>matches</em> operator attempts to apply a regular expression (given as its rightside argument) against a <em>String</em> value (given as the left-side argument):
</p>

```spel
#{admin.email matches '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.com'}
```

<h4>EVALUATING COLLECTIONS</h4>
<p>
Some of SpEL’s most amazing tricks involve working with collections and arrays. The most basic thing you can do is reference a single element from a list:
</p>

```spel
#{jukebox.songs[4].title}
```

<p>
To spice things up a bit, I suppose you could randomly select a song from the jukebox:
</p>

```spel
#{jukebox.songs[T(java.lang.Math).random() * jukebox.songs.size()].title}
```

<p>
SpEL also offers a selection operator (<em>.?[]</em>) to filter a collection into a subset of the collection. As a demonstration, suppose you want a list of all songs in the jukebox where the <em>artist</em> property is Aerosmith. The following expression uses the selection operator to arrive at the list of available Aerosmith songs:
</p>

```spel
#{jukebox.songs.?[artist eq 'Aerosmith']}
```

<p>
As you can see, the selection operator accepts another expression within its square brackets. As SpEL iterates over the list of songs, it evaluates that expression for each entry in the songs collection. If the expression evaluates to true, then the entry is carried over into the new collection. Otherwise it’s left out of the new collection. In this case, the inner expression checks to see if the song’s <em>artist</em> property equals <em>Aerosmith</em>.<br>
SpEL also offers two other selection operations: <em>.^[]</em> for selecting the first matching entry and <em>.$[]</em> for selecting the last matching entry. To demonstrate, consider this expression, which finds the first song in the list whose <em>artist</em> property is <em>Aerosmith</em>:
</p>

```spel
#{jukebox.songs.^[artist eq 'Aerosmith']}
```

<p>
Finally, SpEL offers a projection operator (<em>.![]</em>) to project properties from the elements in the collection onto a new collection. As an example, suppose you don’t want a collection of the song objects, but a collection of all the song titles. The following expression projects the <em>title</em> property into a new collection of <em>String</em>s:
</p>

```spel
#{jukebox.songs.![title]}
```

<p>
Naturally, the projection operator can be combined with any of SpEL’s other operators, including the selection operator. For example, you could use this expression to obtain a list of all of Aerosmith’s songs:
</p>

```spel
#{jukebox.songs.?[artist eq 'Aerosmith'].![title]}
```

</p>
</p>
</p>

<h1>5. Aspect-oriented Spring</h1>
<p>

<p>
This chapter explores Spring’s support for aspects, including how to declare regular classes to be aspects and how to use annotations to create aspects. In addition, you’ll see how AspectJ—another popular AOP implementation—can complement Spring’s AOP framework. But first, before we get carried away with transactions, security, and caching, let’s see how aspects are implemented in Spring, starting with a primer on a few of AOP’s fundamentals.
</p>

<h2>5.1 What is aspect-oriented programming?</h2>
<p>

<p>
Aspects help to modularize cross-cutting concerns. In short, a crosscutting concern can be described as any functionality that affects multiple points of an application. Security, for example, is a cross-cutting concern, in that many methods in an application can have security rules applied to them.
</p>
<img src="./img/cross-cutting-concerns.png" title="Cross-cutting concerns" alt="Cross-cutting concerns">
<p>
Figure: <em>Aspects modularize crosscutting concerns, applying logic that spans multiple application objects.</em>
</p>
<p>
This figure represents a typical application that’s broken down into modules. Each module’s main concern is to provide services for its particular domain. But each module also requires similar ancillary functionality, such as security and transaction management.<br>
A common object-oriented technique for reusing common functionality is to apply inheritance or delegation. But inheritance can lead to a brittle object hierarchy if the same base class is used throughout an application, and delegation can be cumbersome because complicated calls to the delegate object may be required.<br>
Aspects offer an alternative to inheritance and delegation that can be cleaner in many circumstances. With AOP, you still define the common functionality in one place, but you can declaratively define how and where this functionality is applied without having to modify the class to which you’re applying the new feature. Crosscutting concerns can now be modularized into special classes called aspects. This has two benefits. First, the logic for each concern is in one place, as opposed to being scattered all over the code base. Second, your service modules are cleaner because they only contain code for their primary concern (or core functionality), and secondary concerns have been moved to aspects.
</p>

<h3>5.1.1 Defining AOP terminology</h3>
<p>

<p>
Aspects are often described in terms of advice, pointcuts, and join points.<br>
<em>Example reference: electric company.</em>
</p>

<h4>ADVICE</h4>
<p>
<em>The meter reader, his purpose is to report the number of kilowatt hours back to the electric company.</em>
</p>
<p>
Aspects have a purpose—a job they’re meant to do. In AOP terms, the job of an aspect is called <em>advice</em>.<br>
Advice defines both the what and the when of an aspect. In addition to describing the job that an aspect will perform, advice addresses the question of when to perform the job. Should it be applied before a method is invoked? After the method is invoked? Both before and after method invocation? Or should it be applied only if a method throws an exception?<br>
Spring aspects can work with five kinds of advice:
<ul>
    <li><em>Before</em>—The advice functionality takes place before the advised method is invoked.</li>
    <li><em>After</em>—The advice functionality takes place after the advised method completes, regardless of the outcome.</li>
    <li><em>After-returning</em>—The advice functionality takes place after the advised method successfully completes.</li>
    <li><em>After-throwing</em>—The advice functionality takes place after the advised method throws an exception.</li>
    <li><em>Around</em>—The advice wraps the advised method, providing some functionality before and after the advised method is invoked.</li>
</ul>
</p>

<h4>JOIN POINTS</h4>
<p>
<em>An electric company services several houses, perhaps even an entire city.</em>
</p>
<p>
Your application may have thousands of opportunities for advice to be applied. These opportunities are known as join points. A <em>join point</em> is a point in the execution of the application where an aspect can be plugged in. This point could be a method being called, an exception being thrown, or even a field being modified. These are the points where your aspect’s code can be inserted into the normal flow of your application to add new behavior.
</p>

<h4>POINTCUTS</h4>
<p>
<em>Each meter reader is assigned a subset of all the houses to visit.</em>
</p>
<p>
If <em>advice</em> defines the <em>what</em> and <em>when</em> of aspects, then pointcuts define the <em>where</em>. A pointcut definition matches one or more join points at which advice should be woven. Often you specify these pointcuts using explicit class and method names or through regular expressions that define matching class and method name patterns. Some AOP frameworks allow you to create dynamic pointcuts that determine whether to apply advice based on runtime decisions, such as the value of method parameters.
</p>

<h4>ASPECTS</h4>
<p>
An <em>aspect</em> is the merger of advice and pointcuts. Taken together, advice and pointcuts define everything there is to know about an aspect—what it does and where and when it does it.
</p>

<h4>INTRODUCTIONS</h4>
<p>
An <em>introduction</em> allows you to add new methods or attributes to existing classes.
</p>

<h4>WEAVING</h4>
<p>
<em>Weaving</em> is the process of applying aspects to a target object to create a new proxied object. The aspects are woven into the target object at the specified join points. The weaving can take place at several points in the target object’s lifetime:
<ul>
    <li><em>Compile time</em>—Aspects are woven in when the target class is compiled. This requires a special compiler. AspectJ’s weaving compiler weaves aspects this way.</li>
    <li><em>Class load time</em>—Aspects are woven in when the target class is loaded into the JVM. This requires a special ClassLoader that enhances the target class’s bytecode before the class is introduced into the application. AspectJ 5’s <em>load-time weaving</em> (LTW) support weaves aspects this way.</li>
    <li><em>Runtime</em>—Aspects are woven in sometime during the execution of the application. Typically, an AOP container dynamically generates a proxy object that delegates to the target object while weaving in the aspects. This is how Spring AOP aspects are woven.</li>
</ul>
You can now see how advice contains the cross-cutting behavior that needs to be applied to an application’s objects. The join points are all the points within the execution flow of the application that are candidates to have advice applied. The pointcut defines where (at what join points) that advice is applied. The key concept you should take from this is that pointcuts define which join points get advised.
</p>

</p>

<h3>5.1.2 Spring’s AOP support</h3>
<p>

<p>
Spring’s support for AOP comes in four styles:
<ul>
    <li>Classic Spring proxy-based AOP</li>
    <li>Pure-POJO aspects</li>
    <li><em><code>@AspectJ</code></em> annotation-driven aspects</li>
    <li>Injected AspectJ aspects (available in all versions of Spring)</li>
</ul>
The first three styles are all variations on Spring’s own AOP implementation. Spring AOP is built around dynamic proxies. Consequently, Spring’s AOP support is limited to method interception.<br>
Spring’s classic AOP programming model isn’t so great. It won't be covered.<br>
With Spring’s <em>aop</em> namespace, you can turn pure POJOs into aspects. In truth, those POJOs will only supply methods that are called in reaction to a pointcut. Unfortunately, this technique requires XML configuration, but it’s an easy way to declaratively turn any object into an aspect.<br>
Spring borrows AspectJ’s aspects to enable annotation-driven AOP. Under the covers, it’s still Spring’s proxy-based AOP, but the programming model is almost identical to writing full-blown AspectJ annotated aspects. The perk of this AOP style is that it can be done without any XML configuration.<br>
If your AOP needs exceed simple method interception (constructor or property interception, for example), you’ll want to consider implementing aspects in AspectJ. In that case, the fourth style listed will enable you to inject values into AspectJ-driven aspects.
</p>

<h4>SPRING ADVICE IS WRITTEN IN JAVA</h4>
<p>
All the advice you create in Spring is written in a standard Java class. The pointcuts that define where advice should be applied may be specified with annotations or configured in a Spring XML configuration.<br>
Although AspectJ now supports annotation-based aspects, it also comes as a language extension to Java. This approach has benefits and drawbacks. By having an AOP-specific language, you get more power and fine-grained control, as well as a richer AOP toolset. But you’re required to learn a new tool and syntax to accomplish this.
</p>

<h4>SPRING ADVISES OBJECTS AT RUNTIME</h4>
<p>
In Spring, aspects are woven into Spring-managed beans at runtime by wrapping them with a proxy class. The proxy class poses as the target bean,
intercepting advised method calls and forwarding those calls to the target bean. Between the time when the proxy intercepts the method call and the time when it invokes the target bean’s method, the proxy performs the aspect logic.<br>
Spring doesn’t create a proxied object until that proxied bean is needed by the application. If you’re using an ApplicationContext, the proxied objects will be created when it loads all the beans from the BeanFactory. Because Spring creates proxies at runtime, you don’t need a special compiler to weave aspects in Spring’s AOP.
</p>

<h4>SPRING ONLY SUPPORTS METHOD JOIN POINTS</h4>
<p>
As mentioned earlier, multiple join-point models are available through various AOP implementations. Because it’s based on dynamic proxies, Spring only supports method join points. This is in contrast to some other AOP frameworks, such as AspectJ and JBoss, which provide field and constructor join points in addition to method pointcuts. Spring’s lack of field pointcuts prevents you from creating very fine-grained advice, such as intercepting updates to an object’s field. And without constructor pointcuts, there’s no way to apply advice when a bean is instantiated.<br>
But method interception should suit most, if not all, of your needs. If you find yourself in need of more than method interception, you’ll want to complement Spring AOP with AspectJ.
</p>

</p>
</p>

<h2>5.2 Selecting join points with pointcuts</h2>
<p>

<p>
Pointcuts are among the most fundamental elements of an aspect. In Spring AOP, pointcuts are defined using AspectJ’s pointcut expression language.<br>
The most important thing to know about AspectJ pointcuts as they pertain to Spring AOP is that Spring only supports a subset of the pointcut designators available in AspectJ. Recall that Spring AOP is proxy-based, and certain pointcut expressions aren’t relevant to proxy-based AOP.
</p>

<table>
    <thead>
        <tr>
            <th width="20%">AspectJ designator</th>
            <th width="80%">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>args()</td>
            <td>Limits join-point matches to the execution of methods whose arguments are instances of the given types</td>
        </tr>
        <tr>
            <td>@args()</td>
            <td>Limits join-point matches to the execution of methods whose arguments are annotated with the given annotation types</td>
        </tr>
        <tr>
            <td>execution()</td>
            <td>Matches join points that are method executions</td>
        </tr>
        <tr>
            <td>this()</td>
            <td>Limits join-point matches to those where the bean reference of the AOP proxy is of a given type</td>
        </tr>
        <tr>
            <td>target()</td>
            <td>Limits join-point matches to those where the target object is of a given type</td>
        </tr>
        <tr>
            <td>@target()</td>
            <td>Limits matching to join points where the class of the executing object has an annotation of the given type</td>
        </tr>
        <tr>
            <td>within()</td>
            <td>Limits matching to join points within certain types</td>
        </tr>
        <tr>
            <td>@within()</td>
            <td>Limits matching to join points within types that have the given annotation (the execution of methods declared in types with the given annotation when using Spring AOP)</td>
        </tr>
        <tr>
            <td>@annotation</td>
            <td>Limits join-point matches to those where the subject of the join point has the given annotation</td>
        </tr>
    </tbody>
</table>
<p>
Attempting to use any of AspectJ’s other designators will result in an <em>IllegalArgumentException</em> being thrown.<br>
As you browse through the supported designators, note that the <em>execution</em> designator is the only one that actually performs matches. The other designators are used to limit those matches. This means <em>execution</em> is the primary designator you’ll use in every pointcut definition you write. You’ll use the other designators to constrain the pointcut’s reach.
</p>

<h3>5.2.1 Writing pointcuts</h3>
<p>

<p>
To demonstrate aspects in Spring, you need something to be the subject of the aspect’s pointcuts. For that purpose, let’s define a <em>Performance</em> interface:
</p>

```java
package concert;

public interface Performance {
	public void perform();
}
```

<p>
Let’s say that you want to write an aspect that triggers off <em>Performance</em>’s <em>perform()</em> method:
</p>
<img src="./img/execute-aop-annotation.png" />
<p>
Now let’s suppose that you want to confine the reach of that pointcut to only the <em>concert</em> package:
</p>
<img src="./img/execute-within-aop-annotation.png" />

</p>

<h3>5.2.2 Selecting beans in pointcuts</h3>
<p>

<p>
In addition to the designators listed in table the table above, Spring adds a <em>bean()</em> designator that lets you identify beans by their ID in a pointcut expression. <em>bean()</em> takes a bean ID or name as an argument and limits the pointcut’s effect to that specific bean:
</p>

```java
execution(* concert.Performance.perform()) and bean('woodstock')
```

<p>
Here you’re saying that you want to apply aspect advice to the execution of <em>Performance</em>’s <em>perform()</em> method, but limited to the bean whose ID is <em>woodstock</em>.<br>
Narrowing a pointcut to a specific bean may be valuable in some cases, but you can also use negation to apply an aspect to all beans that don’t have a specific ID:
</p>

```java
execution(* concert.Performance.perform()) and !bean('woodstock')
```

</p>
</p>

<h2>5.3 Creating annotated aspects</h2>
<p>

<p>
A key feature introduced in AspectJ 5 is the ability to use annotations to create aspects. Prior to AspectJ 5, writing AspectJ aspects involved learning a Java language extension. But AspectJ’s annotation-oriented model makes it simple to turn any class into an aspect by sprinkling a few annotations around.
</p>

<h3>5.3.1 Defining an aspect</h3>
<p>

<p>
Define an AspectJ Aspect using the <em><code>@Aspect</code></em> annotation:
</p>

```java
@Component
@Aspect
public class Audience {

	@Pointcut("execution(** com.springinaction.aop.concert.Performance.perform(..))")
	public void performance() {
	}

	@Before("performance()")
	public void silenceCellPhones() {
		System.out.println("Silencing cell phones");
	}

	@Before("performance()")
	public void takeSeats() {
		System.out.println("Taking seats");
	}

	@AfterReturning("performance()")
	public void applause() {
		System.out.println("CLAP CLAP CLAP!!!");
	}

	@AfterThrowing("performance()")
	public void demandRefund() {
		System.out.println("Demanding refund");
	}

}
```

<p>
The <em><code>@Pointcut</code></em> annotation defines a reusable pointcut within an <em><code>@AspectJ</code></em> aspect. The body of the <em>performance()</em> method is irrelevant and, in fact, should be empty. The method itself is just a marker, giving the <em><code>@Pointcut</code></em> annotation something to attach itself to.<br>
If you’re using JavaConfig, you can turn on auto-proxying by applying the <em><code>@EnableAspectJAutoProxy</code></em> annotation at the class level of the configuration class. The following configuration class shows how to enable auto-proxying in JavaConfig:
</p>

```java
@Configuration
@ComponentScan(basePackageClasses = Audience.class)
@EnableAspectJAutoProxy
public class ConcertConfig {

	@Autowired
	private Audience audience;
	@Autowired
	@GuitarPerformance
	private Performance performance;

	public Audience getAudience() {
		return audience;
	}

	public void setAudience(Audience audience) {
		this.audience = audience;
	}

	public Performance getPerformance() {
		return performance;
	}

	public void setPerformance(Performance performance) {
		this.performance = performance;
	}

}
```

<p>
Performance implementation using <em><code>@GuitarPerformance</code></em> qualifier:
</p>

```
@Component
@GuitarPerformance
public class GuitarSolo implements Performance {

	@Override
	public void perform() {
		System.out.println("Playing guitar solo");
	}

}
```

<p>
Test class using JUnit:
</p>

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertConfig.class)
public class ConcertTest {

	@Autowired
	private ApplicationContext context;
	@Autowired
	private Performance performance;

	@Test
	public void shouldNotBeNull() {
		assertNotNull(context.containsBean("audience"));
		assertNotNull(context.containsBean("performance"));
	}

	@Test
	public void testingAspect() {
		performance.perform();
	}

}
```

<p>
Whether you use JavaConfig or XML, AspectJ auto-proxying uses the <em><code>@Aspectannotated</code></em> bean to create a proxy around any other beans for which the aspect’s pointcuts are a match. In this case, a proxy will be created for the <em>Concert</em> bean, with the advice methods in <em>Audience</em> being applied before and after the <em>perform()</em> method.<br>
It’s important to understand that Spring’s AspectJ auto-proxying only uses <em><code>@AspectJ</code></em> annotations as a guide for creating proxy-based aspects. Under the covers, it’s still Spring’s proxy-based aspects. This is significant because it means that although you’re using <em><code>@AspectJ</code></em> annotations, you’re still limited to proxying method invocations. If you want to be able to exploit the full power of AspectJ, you’ll have to use the AspectJ runtime and not rely on Spring to create proxy-based aspects.
</p>
</p>

<h3>5.3.2 Creating around advice</h3>
<p>

<p>
Around advice is the most powerful advice type. It allows you to write logic that completely wraps the advised method. It’s essentially like writing both before advice and after advice in a single advice method:
</p>

```java
@Component
@Aspect
public class Audience {

	@Pointcut("execution(** com.springinaction.aop.concert.Performance.perform(..))")
	public void performance() {
	}

	@Around("performance()")
	public void watchPerformance(ProceedingJoinPoint jp) {
		try {
			System.out.println("Silencing cell phones");
			System.out.println("Taking seats");
			jp.proceed();
			System.out.println("CLAP CLAP CLAP!!!");
		} catch (Throwable e) {
			System.out.println("Demanding refund");
		}
	}

}
```

<p>
The first thing you’ll notice about this new advice method is that it’s given a <em>ProceedingJoinPoint</em> as a parameter. This object is necessary because it’s how you can invoke the advised method from within your advice. The advice method will do everything it needs to do; and when it’s ready to pass control to the advised method, it will call <em>ProceedingJoinPoint</em>’s <em<proceed()</em> method.
</p>

</p>

<h3>5.3.3 Handling parameters in advice</h3>
<p>

<p>
Suppose you want keep a count of how many times each song in a performance is played. Add the <em>perform(String songTitle)</em> to the <em>Performance</em> interface and implement it.<br>
Define a new Aspect:
</p>

```java
@Component
@Aspect
public class PerformanceCounter {

	private Map<String, Integer> performanceCounts = new HashMap<>();

	@Pointcut("execution(* com.springinaction.aop.concert.Performance.perform(String)) && args(songTitle)")
	public void performance(String songTitle) {
	}

	@Before("performance(songTitle)")
	public void countSong(String songTitle) {
		int currentCount = getSongPlayCount(songTitle);
		performanceCounts.put(songTitle, ++currentCount);
		System.out.println(currentCount + " times");
	}

	public int getSongPlayCount(String songTitle) {
		return performanceCounts.containsKey(songTitle) ? performanceCounts.get(songTitle) : 0;
	}

}
```

<p>
Define a new JavaConfig:
</p>

```java
@Configuration
@ComponentScan(basePackageClasses = PerformanceCounter.class)
@EnableAspectJAutoProxy
public class ConcertWithParametersConfig {

	@Autowired
	private Performance performance;
	@Autowired
	private PerformanceCounter performanceCounter;

	public Performance getPerformance() {
		return performance;
	}

	public void setPerformance(Performance performance) {
		this.performance = performance;
	}

	public PerformanceCounter getPerformanceCounter() {
		return performanceCounter;
	}

	public void setPerformanceCounter(PerformanceCounter performanceCounter) {
		this.performanceCounter = performanceCounter;
	}

}
```

<p>
Define a new ConcertTest JUnit class:
</p>

```
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertWithParametersConfig.class)
public class ConcertWithParametersTest {

	@Autowired
	private ApplicationContext context;
	@Autowired
	private Performance performance;

	@Test
	public void shouldNotBeNull() {
		assertNotNull(context.containsBean("performanceCounter"));
		assertNotNull(context.containsBean("performance"));
	}

	@Test
	public void testingAspect() {
		performance.perform("Alter Bridge - Blackbird");
		performance.perform("Foo Fighters - Walk");
		performance.perform("Biffy Clyro - Medicine");
		performance.perform("Alter Bridge - Blackbird");
	}

}
```

<p>
The aspects you’ve worked with thus far wrap existing methods on the advised object. But method wrapping is just one of the tricks that aspects can perform.
</p>

</p>

<h3>5.3.4 Annotating introductions</h3>
<p>

<p>
Using an AOP concept known as <em>introduction</em>, aspects can attach new methods to Spring beans.<br>
In Spring, aspects are proxies that implement the same interfaces as the beans they wrap. What if, in addition to implementing those interfaces, the proxy is also exposed through some new interface? Then any bean that’s advised by the aspect will appear to implement the new interface, even if its underlying implementation class doesn’t.<br>
Notice that when a method on the introduced interface is called, the proxy delegates the call to some other object that provides the implementation of the new interface. Effectively, this gives you one bean whose implementation is split across multiple classes.<br>
Putting this idea to work, let’s say you want to introduce the following <em>Encoreable</em> interface to any implementation of <em>Performance</em>:
</p>

```java
public interface Encoreable {
    void performEncore();
}
```

<p>
You need a way to apply this interface to your <em>Performance</em> implementations. Uou could visit all implementations of <em>Performance</em> and change them so that they also implement <em>Encoreable</em>. But from a design standpoint, that may not be the best move. Not all <em>Performances</em> will necessarily be <em>Encoreable</em>. Moreover, it may not be possible to change all implementations of <em>Performance</em>, especially if you’re working with thirdparty implementations and don’t have the source code.<br>
Fortunately, AOP introductions can help you without compromising design choices or requiring invasive changes to the existing implementations. To pull it off, you create a new aspect:
</p>

```java
@Component
@Aspect
public class EncoreableIntroducer {

	@DeclareParents(value = "com.springinaction.aop.concert.Performance+", defaultImpl = DefaultEncoerable.class)
	public static Encoreable encoreable;

}
```

<p>
<em>EncoreableIntroducer</em> is an aspect that doesn't provide <em>advice</em>s. Instead, it introduces the <em>Encoreable</em> interface to <em>Performance</em> beans using the <em><code>@DeclareParents</code></em> annotation.<br>
The @DeclareParents annotation is made up of three parts:
<ul>
    <li>The <em>value</em> attribute identifies the kinds of beans that should be introduced with the interface;</li>
    <li>The <em>defaultImpl</em> attribute identifies the class that will provide the implementation for the introduction;</li>
    <li>The static property that is annotated by <em><code>@DeclareParents</code></em> specifies the interface that’s to be introduced.</li>
</ul>
JavaConfig class:
</p>

```java
@Configuration
@ComponentScan(basePackageClasses = { Encoreable.class, Performance.class })
@EnableAspectJAutoProxy
public class ConcertIntroductionConfig {

	@Autowired
	private Performance performance;
	@Autowired
	private Encoreable encoreable;

	public Performance getPerformance() {
		return performance;
	}

	public void setPerformance(Performance performance) {
		this.performance = performance;
	}

	public Encoreable getEncoreable() {
		return encoreable;
	}

	public void setEncoreable(Encoreable encoreable) {
		this.encoreable = encoreable;
	}

}
```

<p>
JUnit test class:
</p>

```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertIntroductionConfig.class)
public class IntroductionTest {

	@Autowired
	private ApplicationContext context;
	@Autowired
	private Performance performance;

	@Test
	public void shouldNotBeNull() {
		assertNotNull(context.containsBean("performance"));
		assertNotNull(context.containsBean("encoreable"));
	}

	@Test
	public void testIntroduction() {
		Encoreable encoreable = (Encoreable) performance;
		performance.perform();
		encoreable.performEncore();
	}

}
```

</p>
</p>

<h2>5.4 Declaring aspects in XML</h2>
<p>

<p>
If you need to declare aspects without annotating the advice class, then you must turn to XML configuration.<br>
Spring’s <em>aop</em> namespace offers several elements that are useful for declaring aspects in XML.
</p>
<table>
    <thead>
        <tr>
            <th width="25%">AOP configuration element</th>
            <th width="75%">Purpose</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>aop:advisor</td>
            <td>Defines an AOP advisor.</td>
        </tr>
        <tr>
            <td>aop:after</td>
            <td>Defines an AOP after advice (regardless of whether the advised method returns successfully).</td>
        </tr>
        <tr>
            <td>aop:after-returning</td>
            <td>Defines an AOP after-returning advice.</td>
        </tr>
        <tr>
            <td>aop:after-throwing</td>
            <td>Defines an AOP after-throwing advice.</td>
        </tr>
        <tr>
            <td>aop:around</td>
            <td>Defines an AOP around advice.</td>
        </tr>
        <tr>
            <td>aop:aspect</td>
            <td>Defines an aspect.</td>
        </tr>
        <tr>
            <td>aop:aspectj-autoproxy</td>
            <td>Enables annotation-driven aspects using @AspectJ.</td>
        </tr>
        <tr>
            <td>aop:before</td>
            <td>Defines an AOP before advice.</td>
        </tr>
        <tr>
            <td>aop:config</td>
            <td>The top-level AOP element. Most aop: elements must be contained within aop:config.</td>
        </tr>
        <tr>
            <td>aop:declare-parents</td>
            <td>Introduces additional interfaces to advised objects that are transparently implemented.</td>
        </tr>
        <tr>
            <td>aop:pointcut</td>
            <td>Defines a pointcut.</td>
        </tr>
    </tbody>
</table>

<h3>5.4.1 Declaring before and after advice</h3>
<p>

<p>
You’ll use some of the elements from Spring’s <em>aop</em> namespace to turn the annotation-free <em>Audience</em> into an aspect:
</p>

```xml
<aop:config>
    <aop:aspect ref="audience">
        <aop:pointcut
            id="performance"
            expression="execution(** concert.Performance.perform(..))" />
            
        <aop:before
            pointcut-ref="performance"
            method="silenceCellPhones"/>
            
        <aop:before
            pointcut-ref="performance"
            method="takeSeats"/>
            
        <aop:after-returning
            pointcut-ref="performance"
            method="applause"/>
            
        <aop:after-throwing
            pointcut-ref="performance"
            method="demandRefund"/>
    </aop:aspect>
</aop:config>
```

</p>

<h3>5.4.2 Declaring around advice</h3>
<p>

<p>
Declaring around advice isn’t dramatically different from declaring other types of advice. All you need to do is use the <em>aop:around</em> element:
</p>

```xml
<aop:config>
    <aop:aspect ref="audience">
        <aop:pointcut
            id="performance"
            expression="execution(** concert.Performance.perform(..))" />
            
        <aop:around
            pointcut-ref="performance"
            method="watchPerformance"/>
    </aop:aspect>
</aop:config>
```

</p>

<h3>5.4.3 Passing parameters to advice</h3>
<p>

<p>
The following listing shows the complete Spring configuration that declares both the <em>TrackCounter</em> bean and the <em>BlankDisc</em> bean and enables <em>TrackCounter</em> as an aspect:
</p>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xsi:schemaLocation=
        "http://www.springframework.org/schema/aop
        http://www.springframework.org/schema/aop/spring-aop.xsd
        http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">
        
    <bean id="trackCounter"
        class="soundsystem.TrackCounter" />
        
    <bean id="cd"
        class="soundsystem.BlankDisc">
        <property name="title"
        value="Sgt. Pepper's Lonely Hearts Club Band" />
        <property name="artist" value="The Beatles" />
        <property name="tracks">
            <list>
                <value>Sgt. Pepper's Lonely Hearts Club Band</value>
                <value>With a Little Help from My Friends</value>
                <value>Lucy in the Sky with Diamonds</value>
                <value>Getting Better</value>
                <value>Fixing a Hole</value>
                <!-- ...other tracks omitted for brevity... -->
            </list>
        </property>
    </bean>
    
    <aop:config>
        <aop:aspect ref="trackCounter">
            <aop:pointcut id="trackPlayed" expression=
                "execution(* soundsystem.CompactDisc.playTrack(int))
                    and args(trackNumber)" />
            <aop:before
                pointcut-ref="trackPlayed"
                method="countTrack"/>
        </aop:aspect>
    </aop:config>
</beans>
```

</p>

<h3>5.4.4 Introducing new functionality with aspects</h3>
<p>

<p>
The following snippet of XML is equivalent to the AspectJ-based introduction you created earlier:
</p>

```xml
<aop:aspect>
    <aop:declare-parents
        types-matching="concert.Performance+"
        implement-interface="concert.Encoreable"
        default-impl="concert.DefaultEncoreable"
        />
</aop:aspect>
```

<p>
Another way to identify the implementation is using the <em>delegate-ref</em> attribute:
</p>

```xml
<aop:aspect>
    <aop:declare-parents
        types-matching="concert.Performance+"
        implement-interface="concert.Encoreable"
        delegate-ref="encoreableDelegate"
    />
</aop:aspect>
```

<p>
The <em>delegate-ref</em> attribute refers to a Spring bean as the introduction delegate. This assumes that a bean with an ID of <em>encoreableDelegate</em> exists in the Spring context:
</p>

```xml
<bean id="encoreableDelegate"
    class="concert.DefaultEncoreable" />
```

<p>
The difference between directly identifying the delegate using <em>default-impl</em> and indirectly using <em>delegate-ref</em> is that the latter will be a Spring bean that itself may be injected, advised, or otherwise configured through Spring.
</p>

</p>
</p>

<h2>5.5 Injecting AspectJ aspects</h2>
<p>

<p>
Although Spring AOP is sufficient for many applications of aspects, it’s a weak AOP solution when contrasted with AspectJ. AspectJ offers many types of pointcuts that aren’t possible with Spring AOP.<br>
Constructor pointcuts, for example, are convenient when you need to apply advice on the creation of an object. Unlike constructors in some other object-oriented languages, Java constructors are different from normal methods. This makes Spring’s proxy-based AOP woefully inadequate for advising the creation of an object.<br>
For the most part, AspectJ aspects are independent of Spring. Although they can be woven into any Java-based application, including Spring applications, there’s little involvement on Spring’s part in applying AspectJ aspects.<br>
But any well-designed and meaningful aspect will likely depend on other classes to assist in its work. If an aspect depends on one or more classes when executing its advice, you can instantiate those collaborating objects with the aspect itself. Or, better yet, you can use Spring’s dependency injection to inject beans into AspectJ aspects.<br>
Example page 125 using <em>CriticAspect</em>.
</p>

</p>
</p>

<h1>6. Building Spring web applications</h1>
<p>

<h2>6.1 Getting started with Spring MVC</h2>
<p>

<h3>6.1.1 Following the life of a request</h3>
<p>

</p><!-- 6.1.1 close -->

</p><!-- 6.1 close -->

</p><!-- 6. close -->

<h1>A. Notes</h1>
<p>
</p>