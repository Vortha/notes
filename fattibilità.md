<h1>Domande</h1>
<ul>
<li>Il sistema contribuirà agli obiettivi generali dell'organizzazione?</li>
<li>Il sistema può essere implementato usando la tecnologia attuale, secondo costi prefissati e con vincoli sulla tempistica?</li>
<li>Il sistema può essere integrato con altri sistemi che sono già installati?</li>
<li>Come sarebbe affrontata la situazione dall'organizzazione senza l'implementazione di tale sistema?</li>
<li>Quali sono i problemi con i processi attuali, e come il nuovo sistema potrebbe attenuarli?</li>
<li>Quali contributi diretti darebbe il sistema agli obiettivi e ai requisiti aziendali?</li>
<li>Le informazioni possono essere trasferite a e da altri sistemi organizzativi?</li>
<li>Il sistema richiede tecnologia che non è stata usata prima d'ora nell'organizzazione?</li>
<li>Cosa deve essere supportato dal sistema e cosa non necessita di essere supportato?</li>
</ul>