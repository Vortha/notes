Master Sapienza: https://corsidilaurea.uniroma1.it/it/corso/2017/cybersecurity/insegnamenti

<table>
    <tr>
        <th></th>
        <th><b>Subject</b></th>
        <th><b>Link</b></th>
    </tr>
    <tr>
        <td><b>Incoming</b></td>
        <td>MongoDB</td>
        <td>https://university.mongodb.com/courses/M001/about</td>
    </tr>
    <tr>
        <td rowspan="2"><b>TODO</b></td>
        <td>Android</td>
        <td>https://developer.android.com/index.html</td>
    </tr>
    <tr>
        <td>Unity</td>
        <td>https://docs.unity3d.com/Manual/UnityManual.html</td>
    </tr>
</table>

<h1>Subjects</h1>
<ul>
    <li>AngularJS</li>
    <li>Hibernate</li>
    <li>JMS Queue</li>
    <li>NodeJS</li>
    <li>OOP-Pattern</li>
    <li>Spring</li>
</ul>    