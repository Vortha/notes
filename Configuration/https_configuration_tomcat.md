<h1>Introduction</h1>
This file is to help you on configuring the https connection on tomcat.

<h1>Certificate creation</h1>
<p>
Creation of a new JKS keystore from scratch, containing a single self-signed Certificate:
</p>

```bash
$JAVA_HOME/bin/keytool -genkey -alias tomcat -keyalg RSA
```

<h1>Tomcat configuration</h1>
<p>
Edit $CATALINA_BASE/conf/server.xml (where $CATALINA_BASE represents the base directory for the Tomcat instance) configuring the Connector for https connections. Add:
</p>

```xml
<Connector port="8092" protocol="org.apache.coyote.http11.Http11Protocol"
	maxThreads="200" scheme="https" secure="true" SSLEnabled="true"
	keystoreFile="${user.home}/.keystore" keystorePass="password"
	clientAuth="false" sslProtocol="TLS" />
```

<p>
In keystoreFile you must define the path to the Certificate.<br>
For further help: https://tomcat.apache.org/tomcat-6.0-doc/ssl-howto.html#Configuration
</p>

<h2>Configure https just for some paths:</h2>
<p>
Edit $CATALINA_BASE/conf/web.xml and add the following:
</p>

```xml
<!-- ================ Security constraint to enable HTTPS =============== -->
<!-- Defining the HTTPS url pattern                                       -->
  
  <security-constraint>
      <web-resource-collection>
          <web-resource-name>AuthServer</web-resource-name>
          <url-pattern>/*</url-pattern>
      </web-resource-collection>
      <user-data-constraint>
          <!-- If you uncomment <transport-guarantee> line, it allows     -->
          <!-- only https connection                                      -->
          <!-- <transport-guarantee>CONFIDENTIAL</transport-guarantee> -->
      </user-data-constraint>
  </security-constraint>
```

<h1>Import certificate to JAVA_HOME cacerts</h1>
<p>
The certificate created before wasn't of type X.509. Run the following and when asked for password insert "changeit" (if you didn't change the password):
</p>

```bash
$JAVA_HOME/bin/keytool -export -alias tomcat -keystore .keystore -rfc -file X509_keystore.cer
```

<p>
Now import the certificate to JAVA_HOME cacerts:
</p>

```bash
$JAVA_HOME/bin/keytool -import -alias tomcat -keystore $JAVA_HOME/jre/lib/security/cacerts -file X509_keystore.cer
```

<p>
If you want to delete the imported certificate, run the following:
</p>

```bash
$JAVA_HOME/bin/keytool -delete -alias tomcat -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit
```

<p>
Edit (or create if not exists) $CATALINA_BASE/bin/setenv.sh including the following:
</p>

```bash
JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStore=$JAVA_HOME/jre/lib/security/cacerts"
JAVA_OPTS="$JAVA_OPTS -Djavax.net.ssl.trustStorePassword=changeit"
```

<p>
For further help: http://jyotirbhandari.blogspot.com/2011/09/java-error-invalidalgorithmparameterexc.html<br>
https://www.sslshopper.com/article-how-to-create-a-self-signed-certificate-using-java-keytool.html
</p>