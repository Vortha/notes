# Index
<b>

1. <a href="#1-lesson-10-images-to-containers">Lesson 10 - Images to Containers</a>
2. <a href="#2-lesson-11-containers-to-images">Lesson 11 - Containers to images</a>
    1. <a href="#21-stopped-container">Stopped Container</a>
    2. <a href="#22-docker-commit">Docker commit</a>
3. <a href="#3-lesson-12-run-processes-in-containers">Lesson 12 - Run processes in Containers</a>
    1. <a href="#31-detached-container">Detached Container</a>
    2. <a href="#32-running-more-things-in-a-container">Running more things in a Container</a>
4. <a href="#4-lesson-13-manage-containers">Lesson 13 - Manage Containers</a>
	1. <a href="#41-logging">Logging</a>
	2. <a href="#42-stopping-and-removing-containers">Stopping and Removing Containers</a>
	3. <a href="#43-resource-constraints">Resource Constraints</a>
	4. <a href="#44-lessons-from-the-field">Lessons from the Field</a>
5. <a href="#5-lesson-14-network-between-containers">Lesson 14 - Network between Containers</a>
	1. <a href="#51-private-container-networking">Private Container Networking</a>
	2. <a href="#52-exposing-ports-dynamically">Exposing Ports Dynamically</a>
6. <a href="#6-lesson-15-link-containers">Lesson 15 - Link Containers</a>
    1. <a href="#61-connecting-between-containers">Connecting between Containers</a>
    2. <a href="#62-connecting-directly-between-containers">Connecting Directly between Containers</a>
7. <a href="#7-lesson-16-dynamic-and-legacy-linking">Lesson 16 - Dynamic and legacy linking</a>
	1. <a href="#71-how-to-make-links-not-break">How to Make Links Not Break</a>
	2. <a href="#72-legacy-linking">Legacy Linking</a>
	3. <a href="#73-ip-address-binding-in-your-services">IP Address Binding in Your Services</a>
8. <a href="#8-lesson-17-images">Lesson 17 - Images</a>
	1. <a href="#81-listing-images">Listing Images</a>
	2. <a href="#82-tagging-images">Tagging Images</a>
	3. <a href="#83-getting-images">Getting Images</a>
	4. <a href="#84-cleaning-up">Cleaning Up</a>
9. <a href="#9-lesson-18-volumes">Lesson 18 - Volumes</a>
	1. <a href="#91-sharing-data-with-the-host">Sharing Data with the Host</a>
	2. <a href="#92-sharing-data-between-containers">Sharing Data between Containers</a>
10. <a href="#10-lesson-19-docker-registries">Lesson 19 - Docker registries</a>
	1. <a href="#101-advices">Advices</a>
11. <a href="#11-lesson-20-what-are-dockerfiles">Lesson 20 - What are Dockerfiles</a>
	1. <a href="#111-what-is-a-dockerfile">What is a Dockerfile?</a>
	2. <a href="#112-producing-the-next-image-with-each-step">Producing the Next Image with Each Step</a>
	3. <a href="#113-caching-with-each-step">Caching with Each Step</a>
	4. <a href="#114-not-shell-scripts">Not Shell Scripts</a>
12. <a href="#12-lesson-21-building-dockerfiles">Lesson 21 - Building Dockerfiles</a>
	1. <a href="#121-the-most-basic-dockerfile">The Most Basic Dockerfile</a>
	2. <a href="#122-installing-a-program-with-docker-build">Installing a Program with Docker Build</a>
	3. <a href="#123-adding-a-file-through-docker-build">Adding a File through Docker Build</a>
13. <a href="#13-lesson-22-dockerfile-syntax">Lesson 22 - Dockerfile syntax</a>
	1. <a href="#131-the-from-statement">The FROM Statement</a>
	2. <a href="#132-the-maintainer-statement">The MAINTAINER Statement</a>
	3. <a href="#133-the-run-statement">The RUN Statement</a>
	4. <a href="#134-the-add-statement">The ADD Statement</a>
	5. <a href="#135-the-env-statement">The ENV Statement</a>
	6. <a href="#136-the-entrypoint-and-cmd-statements">The ENTRYPOINT and CMD Statements</a>
	7. <a href="#137-shell-form-vs-exec-form">Shell Form vs. Exec Form</a>
	8. <a href="#138-the-expose-statement">The EXPOSE Statement</a>
	9. <a href="#139-the-volume-statement">The VOLUME Statement</a>
	10. <a href="#1310-the-workdir-statement">The WORKDIR Statement</a>
	11. <a href="#1311-the-user-statement">The USER Statement</a>
14. <a href="#14-lesson-23-multi-project-dockerfiles">Lesson 23 - Multi-project Dockerfiles</a>
15. <a href="#15-lesson-24-avoid-golden-images">Lesson 24 - Avoid golden images</a>
16. <a href="#16-lesson-25-docker-the-program">Lesson 25 - Docker the program</a>
	1. <a href="#161-what-kernels-do">What Kernels Do</a>
	2. <a href="#162-what-docker-does">What Docker Does</a>
	3. <a href="#163-the-docker-control-socket">The Docker Control Socket</a>
17. <a href="#17-lesson-26-networking-and-namespaces">Lesson 26 - Networking and namespaces</a>
    1. <a href="#171-networking-in-brief">Networking in Brief</a>
    2. <a href="#172-bridging">Bridging</a>
	3. <a href="#173-routing">Routing</a>
	4. <a href="#174-namespaces">Namespaces</a>
18. <a href="#18-lesson-27-processes-and-cgroups">Lesson 27 - Processes and cgroups</a>
	1. <a href="#181-primer-on-linux-processes">Primer on Linux Processes</a>
	2. <a href="#182-resource-limiting">Resource Limiting</a>
19. <a href="#19-lesson-28-storage">Lesson 28 - Storage</a>
	1. <a href="#191-unix-storage-in-brief">Unix Storage in brief</a>
	2. <a href="#192-moving-cows">Moving Cows</a>
	3. <a href="#193-volumes-and-bind-mounting">Volumes and Bind Mounting</a>
20. <a href="#20-lesson-29-registries-in-detail">Lesson 29 - Registries in detail</a>
    1. <a href="#201-what-is-a-docker-registry">What is a Docker Registry?</a>
    2. <a href="#202-popular-docker-registry-programs">Popular Docker Registry Programs</a>
    3. <a href="#203-running-the-docker-registry-in-docker">Running the Docker Registry in Docker</a>
	4. <a href="#204-storage-options">Storage Options</a>
	5. <a href="#205-saving-and-loading-containers">Saving and Loading Containers</a>
21. <a href="#21-lesson-30-intro-to-orchestration">Lesson 30 - Intro to orchestration</a>
	1. <a href="#211-docker-compose">Docker Compose</a>
	2. <a href="#212-kubernetes">Kubernetes</a>
		1. <a href="#2121-advantages-of-kubernetes">Advantages of Kubernetes</a>
	3. <a href="#213-ec2-container-service-ecs">EC2 Container Service (ECS)</a>
		1. <a href="#2131-advantages-of-ecs">Advantages of ECS</a>
	4. <a href="#214-other-options">Other options</a>

&emsp;<b>A. <a href="#a-notes">Notes</a></b><br>
&emsp;&emsp;&emsp;<b>A.a <a href="#aa-install-netcat-and-commit">Install netcat and commit</a></b>

</b>

# 1. Lesson 10 - Images to Containers
<p>
Things you do in Containers stay in Containers, without changing the image they come from.
E.g.: if you create a file in a Container and then run another Container from the same image,
you don't see the file in the second Container.
</p>

# 2. Lesson 11 - Containers to images
<p>
<img src="img/dockerFlow.PNG" width="400px" height="auto" />
</p>

## 2.1 Stopped Container
<p>
The Container that's running is alive, it has a process in it.
When that process exits, the Container and files in it are still there.
</p>
<p>

Stopped Containers don't show up by default, you must specify `-a` (all) argument to see all Containers.<br>
To see the last Container exited, specify `-l` (last) argument.

</p>

```bash
> docker ps -a
```

## 2.2 Docker commit
<p>
It takes Containers and makes images out of them. It doesn't delete the Container, the image
has the same content that was in that Container.
</p>
<p>

`docker run` and `docker commit` are complementary to each other: the first takes images to Containers and the second
takes Containers back to new images. It doesn't override the images Containers came from.

</p>

```bash
> docker run -ti ubuntu bash
> touch MY-IMPORTANT-FILE
> exit

> docker ps -l
> docker commit 9076b32e4f5b
> docker tag 363266cdf828312768400696327bc1a78cda40b57ba624d8f477f88bc22a3d87 my-image
> docker run -ti my-image bash
```

<p>

To tag an image, you can also use the `docker commit` specifying the name of the Container and the name you'd like it
to be tagged as.

</p>

```bash
> docker commit serene_hamilton my-image-2
```

# 3. Lesson 12 - Run processes in Containers
<p>

`docker run` :
 - Starts a Container by giving an image name and a process to run in that Container. This is the main process;
 - The Container stops when the main process stops. You can start other processes in the same Container but the Container
   still exits when the main process stops;
 - Containers have names, if you don't give them names, it will give one up.

</p>

```bash
> docker run --rm -ti ubuntu sleep 5
```

<p>

The `--rm` (remove) argument is used if you don't want to keep the Container afterwards. That says, delete this Container
after exits.

</p>

```bash
> docker run -ti ubuntu bash -c "sleep 3; echo all done"
```

<p>

It starts a process (bash, the shell) and give it an argument `-c` that says "<i>run a few things</i>"

</p>

## 3.1 Detached Container

<p>

You can start a Container running and just let it go specifying `-d` (detached) argument.
It runs in the background.

</p>

```bash
> docker run -d -ti ubuntu bash
```

<p>

You can use the identifier or the name of the Container to attach it.

</p>

```bash
> docker attach priceless_galileo
```

<p>
To exit from the Container by beign detached, press <i>CTRL + P</i> or <i>CTRL + Q</i>. The Container can be attached again
because it is still running.
</p>
<p>

Containers can be found using `docker ps` and attached using `docker attach container-name`

</p>

`docker attach`:
 - Detached Containers;
 - Interactive Containers;
 - Detach with CTRL + P or CTRL + Q;

## 3.2 Running more things in a Container

`docker exec`:
 - Starts another process in an existing Container;
 - Great for debugging and DB administration;
 - Can't add ports, volumes, and so on.

```bash
> docker exec -ti priceless_galileo bash
> touch foo
```

<p>
The above command runs the bash process to priceless_galileo Container and creates a file called foo.
This file is shared between processes of the Container priceless_galileo (because the Container is the same).
When the main process exits, the second one dies with it.
</p>

# 4. Lesson 13 - Manage Containers

## 4.1 Logging
`docker logs`:
 - Keep the output of Containers;
 - View with `docker logs container-name`

<p>
E.g.: create an example detached Container that throws an error and use the `docker logs` command to see errors:
</p>

```bash
> docker run --name example -d ubuntu bash -c "lose /etc/password"
> docker logs example
bash: lose: command not found
```

## 4.2 Stopping and Removing Containers

<p>
You can kill a running Container, it goes to stopped state, and then remove it:
</p>

```bash
> docker kill container-name
> docker rm container-name
```

## 4.3 Resource Constraints
 - Memory limits
 
 ```bash
 docker run --memory maximum-allowed-memory image-name command
 ```
 
 - CPU limits
 
 ```bash
 docker run --cpu-shares relative to other containers
 docker run --cpu-quota to limit it in general
 ```
 
 - Orchestration: generally requires resource limiting

## 4.4 Lessons from the Field

 - Don't let your containers fetch dependencies when they start;
 - Don't leave important things in unnamed stopped Containers.

# 5. Lesson 14 - Network between Containers

<p>
For problems on executing examples below, see <a href="#aa-install-netcat-and-commit">Install netcat and commit</a>.
</p>

## 5.1 Private Container Networking

 - Programs in containers are isolated from the Internet by default;
 - You can group your containers into "private" networks;
 - You explicitly choose who can connect to whom;
 - This is done by "exposing" ports and "linking" Containers;
 - Docker helps you find other exposed ports with Compose services.

<p>
You can explicitly specify the port (as many as you want) inside the container and outside.<br>
netcat is required to run the below example.
</p>

```bash
> docker run --rm -ti -p 45678:45678 -p 45679:45679 --name echo-server ubuntu bash
> nc -lp 45678 | nc -lp 45679
```

In another shell:

```bash
> nc localhost 45678
```

And in another shell:

```bash
> nc localhost 45679
```

<p>
Writing on the two shells, will write the data in both.
</p>
<p>
You can also run two containers and connect through the 'ip port'.
</p>

```bash
> nc 10.81.52.210 45678
```

```bash
> nc 10.81.52.210 45679
```

<p>
As before, writing on the two shells, will write the data in both.
</p>
<p>
You can also specify only the inside port, docker will find the outside port.
</p>

```bash
> docker run --rm -ti -p 45678 -p 45679 --name echo-server ubuntu bash
```

<p>
In another shell:
</p>

```bash
> docker port echo-server
45678/tcp -> 0.0.0.0:32770
45679/tcp -> 0.0.0.0:32769
```

## 5.2 Exposing Ports Dynamically

 - The port inside the container is fixed;
 - The port on the host is chosen from the unused ports;
 - This allows many containers running programs with fixed ports;
 - This often is used with a service discovery program

<p>
To expose UDP ports:
</p>

```bash
> docker run -p outside-port:inside-port/protocol(tcp/udp)
```

<p>
Ports forward "from inside to outside."
</p>

# 6. Lesson 15 - Link Containers

## 6.1 Connecting between Containers
<img src="img/connectingBetweenContainers.PNG" width="400px" height="auto"/>
<p>
You can connect two containers together by just exposing a port on each of them
and having each of them connect to the host on that port which then gets forward
into the appropriate container.
</p>

```bash
> docker run --rm -ti -p 1234:1234 ubuntu bash
> nc -lp 1234
```

<p>
In another shell:
</p>

```bash
> docker run --rm -ti ubuntu bash
> nc 10.35.3.24 1234
> hello
```

## 6.2 Connecting Directly between Containers
<img src="img/connectingDirectlyBetweenContainers.PNG" width="400px" height="auto"/>
<p>
A more efficient approach and better in some ways, is to link your containers so the data
goes directly from the client container to the server container while staying within Docker.
</p>
<p>

 - Generally used with orchestration (to keep track of what's running where);
 - Links all ports, though only one way (from client to server but the server doesn't know when a client connects to it or goes away or whatever);
 - Only for services that cannot ever be run on different machines;
 - A service and its health check-good example;
 - A service and its DB-not good.

</p>

```bash
> docker run --rm -ti --name server ubuntu bash
> nc -lp 1234
```

<p>
In another shell you can directly link the server container:
</p>

```bash
> docker run --rm -ti --link server --name client ubuntu bash
> nc server 1234
> hello
```

<p>

Docker when it was starting the second container put the name in /etc/hosts
`cat /etc/hosts` so Docker twiddled the container as it was starting up, so
it would know what the IP address of the server is, so we can see that it
automatically assigns a host name for the server at the time that the client
was started. If the server's IP address changes after that the link will break,
so can be a little risky if your services are not really being started together
and then cleaned up together.

</p>

# 7. Lesson 16 - Dynamic and legacy linking

## 7.1 How to Make Links Not Break

<p>

 - Docker has private networks that you can setup, put containers in and that
   will keep track of the names at the network level;
 - These have built in nameservers that fix the links;
 - You must create the networks in advance.
To make these networks, use the command:

</p>

```bash
> docker network create network-name
```

<p>
Create the network, the server container and start communication on port 1234:
</p>

```bash
> docker network create example
> docker run --rm -ti --net=example --name server ubuntu bash
> nc -lp 1234
```

<p>
Create the client container, link it to server using network created before:
</p>

```bash
> docker run --rm -ti --link server --net=example --name client ubuntu bash
> nc server 1234
> hello
```

<p>
In the server shell, kill the server, start it up again and test the connection:
</p>

```bash
> exit
> docker run --rm -ti --net=example --name server ubuntu bash
> nc -lp 1234
```

<p>
In the client shell:
</p>

```bash
> nc server 1234
> hello
```

## 7.2 Legacy Linking

<p>
This is the old way to link containers.

 - Sets environment variables in the linking container for host and port;
 - See this in many tutorials and instructions;
 - Try not to add more of it.

</p>

## 7.3 IP Address Binding in Your Services

<p>

 - Services that listen "locally" by default are only available in the container;
 - To allow connections, you need to use the "bind address" "0.0.0.0" inside the container;
 - You should use Docker to limit access to only the host;

</p>

```bash
docker run -p 127.0.0.1:1234:1234/tcp
```

# 8. Lesson 17 - Images

## 8.1 Listing Images

<p>

To list downloaded images use `docker images`.<br>
It's not the tool for finding images to download.

</p>

## 8.2 Tagging Images

<p>

You can tag images using the command `docker commit container-id tag-name`.<br>
You can also specify a version tag:

</p>

```bash
> docker commit container-id tag-name:version
```

<p>
An example of the name structure is:<br>
<em>registry.example.com:port/organization/image-name:version-tag</em>
</p>

<p>
Usually <em>Organization/image-name</em> is enough when you're tagging your images, unless
you're using custom repo with custom ports and you have version tags.
</p>

## 8.3 Getting Images

<p>

Images come from `docker pull` which is actually run automatically for you by `docker run`.<br>
It is useful for offline work if you wanna pull some images in advance.<br>
The opposite is `docker push`.

</p>

## 8.4 Cleaning Up

<p>
Images can accumulate quicly, to remove them from the system you can use:
</p>

```bash
> docker rmi image-name:tag
```

or

```bash
> docker rmi image-id
```

# 9. Lesson 18 - Volumes

<p>
Sharing data between containers and between containers and hosts. Docker offers this feature called volumes.
Volumes are sort of like shared folders, they're virtual discs that you can store data in and share them between
the containers and between containers and the hosts, or both.<br>
There are two main varieties of Volumes:

	- Persistent: you can put data there and it will be available on the host, and when 
	  the container goes away, the data will still be there.
	- Ephemeral: these exist as long as the container is using them, but when no container
	  is using them, they evaporate.

These are not part of images, no part of volumes will be included when you download an image,
and no part of volumes is gonna be involved if you upload an image.
</p>

## 9.1 Sharing Data with the Host

<p>
Create a directory to use as a shared folder and refer to it when running docker image:
</p>

```bash
> mkdir /shared-folders/example
> docker run -ti -v /shared_folders/example:/shared-folder ubuntu bash
> touch /shared-folder/my-data
> exit
```

<p>
Now if you go on your host, the file <em>my-data</em> still exists because it was shared with the host in a volume.
</p>

<p>
To share a file instead of a folder is the same, just specify the path to the file. Be sure the file exists or
Docker will think it is a folder.
</p>

## 9.2 Sharing Data between Containers

<p>

Introduction of the new argument `volumes-from`. These are shared discs that exist only as long
as they're being used, and when they're shared between containers, they'll be common for the
containers that are using them.

</p>

<p>
Run a new container from image specifying the volume (shared folder) and create a new file inside that folder:
</p>

```bash
> docker run -ti -v /shared-data ubuntu bash
> echo hello > /shared-data/data-file
```

<p>

Now run a new container specifying the `volumes-from` argument, passing it the machine name of the first container:

</p>

```bash
> docker run -ti --volumes-from confident_curie ubuntu bash
> echo more > /shared-data/more-data
> ls /shared-data/data-file
data-file  more-data
```

<p>

If you run `ls` from the first container, you can see both <em>data-file</em> and <em>more-data</em>.<br>
Exiting the first container and running a new one specifying the second container's name for the `volumes-from`
argument, you can still use the same volume and files created before are still available.<br>
The volume expires when all containers using it exit.

</p>

# 10. Lesson 19 - Docker registries

<p>

Docker images are retrieved from registries and published through registries. Registries are pieces of software,
and there are several options that you can choose if you want to run a registry.

 - Registries manage and distribute images (search, download and upload images);
 - Docker (the company) offers these registries for free;
 - You can run your own, as well (used from companies to to stay safe and private).

To search an image, use the `docker search` command. You can also search images on <a href="https://hub.docker.com/">hub.docker.com</a>,
results are the same, but you have useful informations on using what you're looking for.<br>
<b>Login is needed if you want to push images.</b>

</p>

## 10.1 Advices

<p>

- Don't push images containing passwords to Docker Hub;
- Clean up your images regularly;
- Be aware of how much you are trusting the containers you fetch.

</p>

# 11. Lesson 20 - What are Dockerfiles

## 11.1 What is a Dockerfile?

<p>
This is a small "program" to create an image, you run it with:
</p>

```bash
docker build -t name-of-the-result path-to-dockerfiles
```

<p>
It says to tag the image built using the name <em>name-of-the-result</em> and <em>path-to-dockerfiles</em> specify
the location where you can find the Dockerfiles.<br>
When it finishes to build the image, the result will be in your local docker registry.
</p>

## 11.2 Producing the Next Image with Each Step

<p>

Each step produces a new image. It's got a series of steps: start with a new image, make a container out of it,
run something in it, make a new image.<br>
Each line takes the image from the previous line and makes another image. The previous image is unchanged,
it's an entry point to start the new one. The state is not carried forward from line to line: if you start
a program on one line, it runs only for the duration of that line.<br>
As result, if part of your build process is download a large file, do something with it and delete it,
if you do that all in one line, then the resulting image will have only the result of that. If you
do these operations in separate lines, the second line (do something with the downloaded large file) will
have the image with the large file saved there, and the space occupied by the big downloaded file will be
carried all the way through and the Dockerfile can get pretty big.<br>
<b>Be careful about having operations on large files span lines in Dockerfiles.</b>

</p>

<p>
<a href="https://docs.docker.com/engine/reference/builder/#dockerfile-examples">Dockerfiles example</a>
</p>

## 11.3 Caching with Each Step

<p>

Each step of running a Dockerfile is cached, it means that means that the next time you run your build,
if nothing changed, it doesn't have to rerun that step. Docker skips lines that have not changed since
the last build. So, if the first line in your Dockerfile is "download this big file and save the latest copy"
and then 20 minutes later you run that again, the file will have already been downloaded, so it won't run
that line.<br>
The parts that change the most belong at the end of the Dockerfile.

</p>

## 11.4 Not Shell Scripts

<p>

Dockerfiles look like shell scripts but they're <b>not</b> shell scripts. Processes you start on one line
will <b>not</b> be running on the next line. You run them, they run for the duration of that container then
that container gets shut down, saved into an image, and you have a fresh start on the next line. So you can't
treat it like a shell script and say start a program in one line, then send a message to that program on the next
line: the program won't be running. If you need to have one program start and then another program start,
these two operations need to be on the same line, so that they run in the same container.

</p>

<p>

Environment variables you set <b>will</b> be set on the next line. If you use the <em>ENV</em> command,
remember that each line in a Dockerfile is its own call to `docker run` command.

</p>

# 12. Lesson 21 - Building Dockerfiles

<p>Let's build a Dockerfile</p>

## 12.1 The Most Basic Dockerfile

<p>

Build the most basic dockerfile that is still interesting.<br>
Create a folder inside docker workspace, make a file called <em>Dockerfile</em> inside it, put the below lines
in the file, build it and run it.

</p>

### Dockerfile
```bash
FROM busybox	# image to start FROM
RUN echo "building simple docker image."	# create a container, in that container RUN echo
CMD echo "hello container"	# command to run when this image is started
```

### Shell
```bash
> docker build -t hello .
Sending build context to Docker daemon  2.048kB
Step 1/3 : FROM busybox
latest: Pulling from library/busybox
90e01955edcd: Pull complete
Digest: sha256:2a03a6059f21e150ae84b0973863609494aad70f0a80eaeb64bddd8d92465812
Status: Downloaded newer image for busybox:latest
 ---> 59788edf1f3e
Step 2/3 : RUN echo "building simple docker image." # create a container, in that container RUN echo
 ---> Running in 65085ae43cbe
building simple docker image.
Removing intermediate container 65085ae43cbe
 ---> b9a3595cb225
Step 3/3 : CMD echo "hello container" # command to run when this image is started
 ---> Running in 9f60a6fc8588
Removing intermediate container 9f60a6fc8588
 ---> cc831cd570f1
Successfully built cc831cd570f1
Successfully tagged hello:latest

> docker run --rm hello
hello container
```

## 12.2 Installing a Program with Docker Build

<p>
A more interesting image which actually installs something in the image.
</p>

<p>
In the next <em>Dockerfile</em> example, it downloads <em>debian:sid</em>, installs <em>nano</em> and executes it. When you run
the image created using this <em>Dockerfile</em>, it opens the <em>/tmp/notes</em> file using <em>nano</em>.
</p>

### Dockerfile

```bash
FROM debian:sid
RUN apt-get -y update			# -y to say "yes"
RUN apt-get install nano
CMD ["/bin/nano", "/tmp/notes"]
```

### Shell
```bash
> docker build -t example/nanoer .
Sending build context to Docker daemon  2.048kB
Step 1/4 : FROM debian:sid
sid: Pulling from library/debian
16e82e17faef: Pull complete
Digest: sha256:79188c5c8eae45282fcb9c72769faf0ca2ac69b66dcc86ba6338119653798caf
Status: Downloaded newer image for debian:sid
 ---> ff12142764fa
Step 2/4 : RUN apt-get -y update                        # -y to say "yes"
 ---> Running in afd7369ad98c
Get:1 http://cdn-fastly.deb.debian.org/debian sid InRelease [237 kB]
Get:2 http://cdn-fastly.deb.debian.org/debian sid/main amd64 Packages [8309 kB]
Fetched 8547 kB in 1min 42s (84.0 kB/s)
Reading package lists...
Removing intermediate container afd7369ad98c
 ---> c89287b86e0e
Step 3/4 : RUN apt-get install nano
 ---> Running in a3d406e56afa
Reading package lists...
Building dependency tree...
Reading state information...
Suggested packages:
  spell
The following NEW packages will be installed:
  nano
0 upgraded, 1 newly installed, 0 to remove and 32 not upgraded.
Need to get 542 kB of archives.
After this operation, 2264 kB of additional disk space will be used.
Get:1 http://cdn-fastly.deb.debian.org/debian sid/main amd64 nano amd64 3.2-1 [542 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 542 kB in 1s (379 kB/s)
Selecting previously unselected package nano.
(Reading database ... 6567 files and directories currently installed.)
Preparing to unpack .../archives/nano_3.2-1_amd64.deb ...
Unpacking nano (3.2-1) ...
Setting up nano (3.2-1) ...
update-alternatives: using /bin/nano to provide /usr/bin/editor (editor) in auto mode
update-alternatives: using /bin/nano to provide /usr/bin/pico (pico) in auto mode
Removing intermediate container a3d406e56afa
 ---> 093c19537400
Step 4/4 : CMD ["/bin/nano", "/tmp/notes"]
 ---> Running in 5009ab27fa0f
Removing intermediate container 5009ab27fa0f
 ---> ceed00ffe2b1
Successfully built ceed00ffe2b1
Successfully tagged example/nanoer:latest

> docker run --rm -ti example/nanoer
```

## 12.3 Adding a File through Docker Build

<p>
This example uses the previous image defined by <em>Dockerfile</em> and adds new things to it.
</p>

### Dockerfile

```bash
FROM example/nanoer	# the previous image
ADD notes.txt /notes.txt	# add notes.txt file (local) in /notes.txt (container)
CMD ["/bin/nano", "/notes.txt"]	# open /notes.txt using nano
```

### Shell

```bash
> docker build -t example/notes .
Sending build context to Docker daemon  3.072kB
Step 1/3 : FROM example/nanoer
 ---> ceed00ffe2b1
Step 2/3 : ADD notes.txt ./notes.txt
 ---> 5ed8ec5cab13
Step 3/3 : CMD ["/bin/nano", "/notes.txt"]
 ---> Running in 9479de77336a
Removing intermediate container 9479de77336a
 ---> 4e324e4e6fcf
Successfully built 4e324e4e6fcf
Successfully tagged example/notes:latest

> docker run -ti --rm example/notes
```

# 13. Lesson 22 - Dockerfile syntax

## 13.1 The FROM Statement

<p>

The FROM statement says which image to download and start running from. This should be alwasy
the first command in the Dockerfile. It's ok to put multiples of them in a Dockerfile, it means
the Dockerfile produces more than one image. They look like `FROM java:8` build my image.

</p>

## 13.2 The MAINTAINER Statement

<p>

Defines the author of this Dockerfile. It looks like `MAINTAINER Firstname Lastname <email@example.com>`

</p>

## 13.3 The RUN Statement

<p>

Runs the command line, waits for it to finish, and saves the result. `RUN unzip install.zip /opt/install/`
or `RUN echo hello docker`.

</p>

## 13.4 The ADD Statement

<p>

Adds local files `ADD run.sh /run.sh`, but can do a whole lot more.<br>
Adds the contents of tar archives `ADD project.tar.gz /install/`: it doesn't copy the file tar.gz into that
directory, it uncompresses all the files to that directory.<br>
Works with URLs as well `ADD https://project.example.com/download/1.0/project.rpm /project/` adds the thing
that you download from this URL to <em>/project</em>.

</p>

## 13.5 The ENV Statement

<p>

Sets environment variables, both during the build and when running the resulting image `ENV DB_HOST=db.production.example.com`.

</p>

## 13.6 The ENTRYPOINT and CMD Statements

<p>

<b>ENTRYPOINT</b> specifies the start of the command to run when starting your container and lets you tack more on the end.
So, if your container has an entry point of LS, then anything you type when you say `docker run my-image-name` would be
treated as arguments to the LS command.

</p>

<p>

<b>CMD</b> specifies the whole command to run, and if someone write something when running that image, that will be run
instead of CMD.

</p>

<p>

If you have both <b>ENTRYPOINT</b> and <b>CMD</b>, they are combined together one after the other.<br>
If your container acts like a command-line program, you can use <b>ENTRYPOINT</b>. If you're unsure,
you can use <b>CMD</b>.

</p>

## 13.7 Shell Form vs. Exec Form

<p>

<b>ENTRYPOINT RUN</b> and <b>CMD</b> can use either form. Shell form looks like `nano notes.txt`,
exec form looks like `["bin/nano", "notes.txt"]` and this causes nano to be run directly, not surrounded
by a call to a shell such as Bash. You can use the form you prefer.

</p>

## 13.8 The EXPOSE Statement

<p>

Maps a port into the container: `EXPOSE 8080` (does the same thing of examples where we used `-p 1234:1234`).

</p>

## 13.9 The VOLUME Statement

<p>

Defines shared or ephemeral volumes, depending if you have one or two arguments. If you have two arguments,
it maps a host path into a container path. If you have one argument, it creates a volume that can be
inherited by later containers.

</p>

```bash
VOLUME ["/host/path/", "/container/path/"]
VOLUME ["/shared-data"]
```

<p>

Avoid defining shared folders in Dockerfile because it probably would work only in your computer.

</p>

## 13.10 The WORKDIR Statement

<p>

Sets the directory the container starts in. It's like typing `cd` at the beginning of every run expression
after that: `WORKDIR /install/`

</p>

## 13.11 The USER Statement

<p>

Sets which user the container will run as `USER arthur` or `USER 1000`. Commands in this container would run as the user
<em>arthur</em> or user identified by <em>1000</em>.

</p>

<p>
More commands on the <a href="https://docs.docker.com/engine/reference/builder/">official documentation</a>.
</p>

# 14. Lesson 23 - Multi-project Dockerfiles

<p>
When we build Dockerfiles that go beyond the very simplest projects, it often runs into two competing desires.
On the one hand, we want the Dockerfile to be totally complete, have everything required
to build this project from scratch, so years from now, we will be absolutely certain that we can rebuild it, and
that it will have everything. On the other hand, we want them to be super small, minimal, and really quick to deploy.<br>
So people often do things like make two Docker files, one for their daily life, and one for production.
These get out of sync and stress ensues. So recently, a new feature was added to Docker files called multi-stage builds.
</p>

<p>Make a small Dockerfile</p>

### Dockerfile

```bash
FROM ubuntu
RUN apt-get update
RUN apt-get -y install curl
RUN curl https://google.com | wc -c > google-size
ENTRYPOINT echo google is this big; cat google-size
```

### Shell

```bash
> docker build -t tooo-big .
...
...

> docker run tooo-big
google is this big
220
```

<p>
If you check the size of <em>tooo-big</em> image, it is about 124MB. Too much for an image that just prints
some characters. To improve it, split the Dockerfile into two parts: name the first part (largely unchanged)
and add another from. The last image (from <em>alpine</em>) size is about 4.41MB.
</p>

### Dockerfile

```bash
FROM ubuntu as builder
RUN apt-get update
RUN apt-get -y install curl
RUN curl https://google.com | wc -c > google-size

FROM alpine
COPY --from=builder /google-size /google-size
ENTRYPOINT echo google is this big; cat google-size
```

### Shell

```bash
> docker build -t google-size .
...
...

> docker run google-size
google is this big
220
```

# 15. Lesson 24 - Avoid golden images

- Include installers in your project;
- Have a canonical build that builds everything completely from scratch;
- Tag your builds with the git hash of the code that built it;
- Use small base images, such as Alpine;
- Build images you share publicly from Dockerfiles, always;
- Don't ever leave passwords in layers; delete files in the same step!

# 16. Lesson 25 - Docker the program

## 16.1 What Kernels Do

- Respond to messages from the hardware;
- Start and schedule programs;
- Control and organize storage;
- Pass messages between programs;
- Allocate resources, memory, CPU, network, and so on;
- Create containers by Docker configuring the kernel.

## 16.2 What Docker Does

- Program written in Go-an upcoming systems language;
- Manages kernel features
	- Uses "cgroups" to contain processes;
	- Uses "namespaces" to contain networks;
	- Uses "copy-on-write" filesystems to build images.
- Used for years before Docker

<p>
Docker makes scripting distributed systems "easy" (for a very peculiar definition of "easy").
</p>

## 16.3 The Docker Control Socket

<p>
Docker is two programs: a client and a server. The server receives commands over a socket (either
over a network or through a special "file" called <em>socket</em>, the second case when they're running
on the same computer).<br>
The client can even run inside docker itself.
</p>

### Running Docker Locally

<img src="img/runningDockerLocally.PNG" width="400px" height="auto"/>

### Running the Client Inside Docker

<img src="img/runningClientInsideDocker.PNG" width="400px" height="auto"/>

<p>
Let's take an idea of what this second one means: control Docker through its socket.<br>
<em>/var/run/docker.sock</em> it's the Docker socket file. If you write the proper data
into this file in the right format, then it will cause the Docker server to do things.<br>
<b>For Windows, this file won't be directly accessible though it's still available from
other Docker containers.</b>
</p>

<p>
Run the Docker client inside a Container and give it access to the Docker control socket on the computer:
</p>

```bash
> docker run -ti --rm -v /var/run/docker.sock:/var/run/docker.sock docker sh
Unable to find image 'docker:latest' locally
latest: Pulling from library/docker
4fe2ade4980c: Already exists
2e793f0ebe8a: Pull complete
77995fba1918: Pull complete
138bee096c72: Pull complete
5ed9d4b111c6: Pull complete
0fc49bcc693c: Pull complete
Digest: sha256:201c31cae6daa97001fe9c0cd2e2d3a157ca6447aaa74b070189790445627511
Status: Downloaded newer image for docker:latest
```

<p>
Now start another container from a client within a container:
</p>

```bash
> docker run -ti --rm ubuntu bash
```

<p>
This is not Docker in Docker, this is a client within a Docker container controlling a server that's outside
that container. This flexibility in where you control Docker from is one of the key ideas
behind Docker and has been a major contributor to its success and popularity.
</p>

# 17. Lesson 26 - Networking and namespaces

<p>
One of the main things Docker does is manage your networking for you to create containers. Let's take a little
brief look at some of the things Docker does for you.
</p>

## 17.1 Networking in Brief

- Ethernet: moves "frames" on a wire (Wi-Fi)
- IP layer: moves packets on a local network
- Routing: forwards packets between networks
- Ports: address particular programs on a computer

## 17.2 Bridging

<p>
Docker uses bridges to create virtual networks in your computer. When you create a private network in Docker,
it creates a bridge. These are software switches. It's equivalent to having a little blue box on your desk
and plugging a bunch of different wires into it, except it's all within your computer and you're plugging containers
into it with virtual network wires. These are used to control the Ethernet layer, containers that actually talk
directly to each other.
</p>

<p>
Start a container with ubuntu image in a network called <em>host</em> and install <em>bridge-utils</em>.
After the installation is finished, show bridges.
</p>

```bash
> docker run -ti --rm --net=host ubuntu bash
> apt-get update && apt-get install bridge-utils
...
> brctl show
bridge name     bridge id               STP enabled     interfaces
br-626f9dea8a14 8000.0242e895dc59       no
docker0         8000.02428679ed36       no
```

<p>
In another shell, create a new network:
</p>

```bash
> docker network create my-new-network
ef8343791e100a18dae19b065f1fb9d76a5daccd9891a5ab73d4a9d9a1143933
```

<p>

Now, if you run `brctl show` again in the first shell, you can see the new created network.

</p>

## 17.3 Routing

<p>
Let's see how Docker moves packets between networks and between containers and the internet.<br>
It uses the built-in firewall features of the Linux kernel, namely the iptables command, to create
firewall rules that control when packets get sent between the bridges and thus become available to
the containers that are attached to those bridges. This whole system is commonly referred to as NAT
(Network Address Translation). That means when a packet is on its way out towards the internet, you
change the source address, so it'll come back to you. And then when it's on the way back in, you change
the destination address, so it looks like it came directly from the machine you were connecting to.<br>
Let's take a look at how Docker accomplishes port forwarding under  the hood.
</p>

<p>

Run a container with ubuntu image giving it access to networking and letting it have full control over
the system that's hosting it. Then install iptables and take a look at networking in your host:

</p>

```bash
> docker run -ti --rm --net=host --privileged=true ubuntu bash
> apt-get update && apt-get install iptables
...
> iptables -n -L -t nat
Chain PREROUTING (policy ACCEPT)
target     prot opt source               destination
DOCKER     all  --  0.0.0.0/0            0.0.0.0/0            ADDRTYPE match dst-type LOCAL

Chain INPUT (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
DOCKER     all  --  0.0.0.0/0           !127.0.0.0/8          ADDRTYPE match dst-type LOCAL

Chain POSTROUTING (policy ACCEPT)
target     prot opt source               destination
MASQUERADE  all  --  172.19.0.0/16        0.0.0.0/0
MASQUERADE  all  --  172.17.0.0/16        0.0.0.0/0
MASQUERADE  all  --  172.18.0.0/16        0.0.0.0/0

Chain DOCKER (2 references)
target     prot opt source               destination
RETURN     all  --  0.0.0.0/0            0.0.0.0/0
RETURN     all  --  0.0.0.0/0            0.0.0.0/0
RETURN     all  --  0.0.0.0/0            0.0.0.0/0
```

<p>
In another shell, start up a container with some ports to forward:
</p>

```bash
> docker run -ti --rm -p 8080:8080 ubuntu bash
```

<p>

After starting up this container, if you go back into your privileged container and run again the `iptables`
command you can see that you have a port forward rule that says to forward anything with destination port 8080
to that Docker container's IP address on port 8080.<br>
So exposing port with Docker is really just port forwarding at the networking layer.

</p>

```bash
...
DNAT     tcp  --  0.0.0.0/0            0.0.0.0/0            tcp dpt:8080 to:172.17.0.2:8080
```

## 17.4 Namespaces

<p>
Namespaces are a feature in the Linux kernel that allows you to provide complete network isolation to different
processes in the system. So it enforces the rule that you're not allowed to mess with the networking of other
processes.<br>
Processes running in containers are attached to virtual network devices, and those virtual network devices are
attached to bridges, which lets them talk to any other containers attached to the same bridges. This is how
it creates the virtual networking. But each container has its own copy of all of the Linux networking stack.
All of the different pieces that make up the networking are isolated to each container, so that they can't do
things like reach in and reconfigure other containers. Namespaces enforce the rules of Docker and keep containers
safe from each other.
</p>

# 18. Lesson 27 - Processes and cgroups

<p>
One of Docker's jobs is to manage the processes in containers, keep them isolated and keep them talking to each other
where appropriate.
</p>

## 18.1 Primer on Linux Processes

<p>
Processes come from other processes, it's a parent-child relationship. When a process exits, it returns that package to the
process that started it. There's a special process, Process Zero called init, that's the process that starts it all and
any other process in a Linux system comes from dividing that process into other processes.
</p>

<p>
Like in Linux, in Docker your container starts with one process, the init process. When the process exits, the container
vanishes and the other processes that were in it at the time that the init process exits, get shut down unceremoniously.
</p>

<p>
In a shell, start a docker container:
</p>

```bash
> docker run -ti --rm --name hello ubuntu bash
```

<p>
In another shell find the container main process PID, start a privileged container and kill the first container main process:
</p>

```bash
> docker inspect --format '{{.State.Pid}}' hello
'2984'
> docker run -ti --rm --privileged=true --pid=host ubuntu bash
> kill 2984
```

## 18.2 Resource Limiting

<p>
A very important job of Docker is to control access to the limited resources on the machine. That's the amount of time the CPU
can spend doing things and the amount of memory that can be allocated between containers. These limitations are inherited,
and this is very important. If a container is given a certain amount of memory and CPU time, no matter how many processes
it starts, the sum total of all those processes can't exceed the quota that was given to the initial process. They can split
it up differently amongst themselves, however they'd like, but <b>you can't escape your limits by starting more processes in
order to consume more limits.</b>
</p>

# 19. Lesson 28 - Storage

<p>
Let's talk briefly about how Docker accomplishes the idea of images and containers.<br>
How does it do the storage behind them?<br>
Keeping them isolated, letting them be stacked on top of each other, all of that stuff requires a little bit of an introduction
to the layers of the Unix file systems method.
</p>

## 19.1 Unix Storage in Brief

- <b>Actual storage devices</b>: at the very lowest level you have actual things, stuff that stores bits. Things you can set a value 1 or 0
and it will remember it and you can come back later and get it. The kernel manages these.<br>
- <b>Logical storage devices</b>: on top of those it forms them into logical groups. So you could say these four groups form a ray to ray,
or these two drives should be treated as one drive, or this one drive should be treated as 432 drives. So it has a layer that lets it sort
of partition up drives arbitrarily into groups and then partition those groups up. Docker makes extensive use of this capability.
- <b>Filesystems</b>: on top of that we have final systems. That determines which bits on the drive are part of which part of which file.
- <b>FUSE filesystems and network filesystems</b>: on top of all that, you can take programs and programs can pretend to be file systems.
We call this FUSE file systems.

<p>
The secret in Docker is COWs: Copy On Write.<br>
When I would write to an image, instead of writing directly to the image, I write to my own layer which gets layered on top of the image.
When I wanna look at the image I just look at the whole thing. I only copy the pieces that I write. I read from the original and copy when
you write, copy on write.
</p>

<img src="img/COWs.PNG" width="400px" height="auto" />

## 19.2 Moving Cows

<p>
The contents of layers are moved between containers in gzip files.<br>
The containers are independent of the storage engine.<br>
Any container can be loaded (almost) everywhere.<br>
It is possible to run out of layers on some of the storage engines. Some of the storage engines have a limited number of layers and others
don't. If you make a very deeply nested image on a machine that allows a great number of layers, and then you package it up and try to download
it on a machine that uses a storage engine with less layers, you can run out of layers. This is worth being vaguely aware of.
</p>

## 19.3 Volumes and Bind Mounting

<p>
Another part of images and storage in Docker is how we do volumes and shared folders with the host. They're built right in to the Linux file system.<br>
So the Linux file system starts with an assumed root directory. You can also take a directory from anywhere in the file system tree and just attach it
somewhere else.
</p>

<p>
Start a privileged container and create folders <em>work</em> and <em>another-work</em> and files inside them. Then mount (to manipulate the file system)
<em>other-work</em> on top of <em>work</em> (attach one directory on top of another):
</p>

```bash
> docker run -ti --rm --privileged=true ubuntu bash
> mkdir example
> cd example
> mkdir work
> touch work/a work/b work/c work/d work/e work/f
> mkdir other-work
> touch other-work/other-a other-work/other-b other-work/other-c other-work/other-d
> ls -R
.:
other-work  work

./other-work:
other-a  other-b  other-c  other-d

./work:
a  b  c  d  e  f
> mount -o bind other-work work
> ls -R
.:
other-work  work

./other-work:
other-a  other-b  other-c  other-d

./work:
other-a  other-b  other-c  other-d
> umount work
> ls -R
.:
other-work  work

./other-work:
other-a  other-b  other-c  other-d

./work:
a  b  c  d  e  f
```

<p>
There's an important side effect of this, which is: you have to get the mount order correct. If you want to mount a folder and then a particular file within
that folder, you have to do in that order. If you mount the file so it bind mounts that file right onto that position, and then you mount the folder and then
you mount folder on top of it, the file will be underneath the folder and will be hidden.<br>
Another bit is: mounting volumes always mounts the host's file system over the guest. Docker doesn't give you a wave saying mount this guest file system into
the host preserving what was on the guest. It always chooses to mount the host file system over the guest file system.
</p>

# 20. Lesson 29 - Registries in detail

<p>
"Where's my data? Is it safe?"<br>
Many companies choose to run their own Docker Registry so they can know that their data is safe and somewhere they can protect it.
</p>

## 20.1 What is a Docker Registry?

<p>
It's just a program. You can run it anywhere you run other programs using existing infrastructure.<br>
It stores layers, images, keeps track of them, stores the tags, generally the meta data around the images, along with the images,
and it's just a service that listens on port 5000 for instructions like: push this image, pull that image, load this image from a disk,
or search for images containing these key words.<br>
It also keeps track of who's allowed to log in, provided that you've configured that.
</p>

## 20.2 Popular Docker Registry Programs

<p>
There are a couple of popular choices.<br>
There's an official Docker Registry produced by Docker, the company, and the popular maven caching repo, Nexus. Also happens to provide in the newest versions,
a Docker repo built in, so there's a good chance you already have a Docker Registry running in your organization and nobody's using it.
</p>

## 20.3 Running the Docker Registry in Docker

<p>
Since the Registry is just a program, designed to provide a network service, why not go ahead and run it in Docker?<br>
Docker makes installing network services pretty easy, that's what it's for, and Registry is a network service. Let's just run it in Docker. 
</p>

```bash
> docker run -d -p 5000:5000 --restart=always --name registry registry:2
844c454fb73523666a551807f9e9043e4a40782ac0a79e186185e24b9939c880
```

<p>

`--restart=always` means: if this containers dies, restart it immediately.

</p>

<p>
Now add a tag to an image to associate it with the local registry just started, then push it to save it locally:
</p>

```bash
> docker tag ubuntu localhost:5000/mycompany/my-ubuntu:99
> docker push localhost:5000/mycompany/my-ubuntu:99
...
```

<p>
One thing to note: you must set up authentication and security on this registry before you expose it to any network,
especially the internet.<br>
To do that, head on over to the [official Docker Registry documentation page](https://docs.docker.com/registry/).
</p>

## 20.4 Storage Options

<p>
There are several options for preserving your images, one of them is just store it on the same machine that's running the registry.
If you have that machine backed up, good solid back up regime, this works fine.<br>
Other great options include the [Docker Trusted Registry](https://docs.docker.com/ee/dtr) made by the people who make Docker, or if
you're on the Amazon ecosystem, the Elastic Container Registry integrates nicely with the rest of the AWS services.<br>
Google provides the [Google Cloud Container Registry](https://cloud.google.com/container-registry) which tightly integrates with the rest
of the Google ecosystem.<br>
On the Microsoft side of things, there's the [Azure Container Registry](https://azure.microsoft.com/en-us/services/container-registry) which
is an excellent option for services that need to integrate with other things using Microsoft Azure.
</p>

## 20.5 Saving and Loading Containers

<p>

Another important option is saving and loading your images locally without using a registry at all.<br>
Docker offers a pair of commands: `docker save` and `docker load` that do the obvious thing.

</p>

<p>
Save some images into an archive so that you'll have them available:
</p>

```bash
> docker save -o my-images.tar.gz ubuntu myflaskapp
```

<p>
Saving your images locally is a great way to make them available when you're traveling, making backups, and a good way to ship images to
customers, and never underestimate the bandwidth of a 747 full of Docker images on backup tapes.
</p>

<p>
Now delete the local copy of those images and then load them from the archive:
</p>

```bash
> docker rmi ubuntu myflaskapp
> docker load -i my-images.tar.gz
> docker images
...
```

<p>
The docker save and load commands are also very important for allowing you to move images between storage types on the same server. If you
switch back end storage engines in Docker, you have to save the images out then load them back in after switching, and it's also a really
good way to get your images to other people literally by mailing them.
</p>

# 21. Lesson 30 - Intro to orchestration

<p>
Now let's briefly touch on how large systems are being built using Docker today. So, just to start with, one container is about as useful
as one hand clapping. It's cool, it's philosophically neat, maybe you can get something out of it, but most people want more.<br>
There are many options out there for orchestrating large systems of Docker containers. We're just gonna touch on a few and give you a place
to start.<br>
Orchestration systems start your containers, keep them running, and restart them if they fail. They also allow the containers to find each other.
If a container gets restarted all the machines that were connecting to it need to be able to find its replacement. So service discovery's a big
part of any Docker orchestration system and making sure that the containers are running in a place where the resources exist for them to actually
do their job. Is the storage they need available at that location? Is there enough RAM? Is there enough CPU? Is that machine being taken offline
for maintenance? Does this container need to be moved somewhere else?
</p>

## 21.1 Docker Compose

<p>
The easiest to get started with and the simplest is Docker Compose. For single machine coordination, this is the de facto standard. In fact, you
already have it on your computer. It's designed for testing, development, staging, generally working on projects that have more than one container.
But not for serving them in large scale systems and not for things that scale dynamically. What it does is it brings up all your containers and
volumes, et cetera, with one command. You just run Docker Compose up and it starts all your containers.
</p>

## 21.2 Kubernetes

<p>
For larger systems, there are many choices.
</p>

<p>
Kubernetes brings a couple of ideas that are fairly common to all of the orchestration systems, but expressed differently everywhere.<br>
It has <b>containers</b> which run programs. Those are containers in the way that we usually think of them.<br>
<b>Pods</b> are groups of containers that are intended to be run together always on the same system. So, pods give you approximately as
much as Docker Compose, but it's dynamically distributed, Kubernetes finds a place to run it, and it provides orchestration and service
discovery and all the other stuff around these pods.<br>
Kubernetes has the idea of services, which make pods discoverable by others, accessible to others, if a connection to a pod that's a part
of a service gets restarted somewhere, then that service will redirect the traffic to the new instance.<br>
Kubernetes has an amazingly powerful system of labels for describing every aspect of your system. So, you can say, "My service needs to
connect to an instance of version 5.2, running on this hardware, and I need an instance that is in the same data center as this other service."
it's very extensive.
</p>

### 21.2.1 Advantages of Kubernetes

<p>

The `kubectl` command makes scripting large operations in Kubernetes if not easy, possible to the extent that other systems don't. It gives
you sort of a one command interface to do almost everything with Kubernetes.<br>
Kubernetes provides a very flexible system of overlay networking to allow your containers to find each other and connect to each other regardless
of how things move around throughout your infrastructure.<br>
Kubernetes runs equally well on your own hardware or on a cloud provider, or even across both of these. Its overlay networking system is quite
well-suited to a wide variety of deployment scenarios.<br>
Kubernetes has service discovery built in.<br>
To get started, head on over to http://kubernetes.io, head over to the documentation page, and click on Get Started.

</p>

## 21.3 EC2 Container Service (ECS)

<p>
Another great option is the Amazon EC2 Container Service, or ECS. This service uses an analogous vocabulary but with a slight different twist
to each part.<br>
Task definitions provide all the information required to start a container and a set of containers that are designed to run together. It's
somewhat analogous to a pod, a little bit in Kubernetes, it's all the information required for services that will run together on the same machine.
It doesn't actually run anything, creating a task definition just defines a task that will run.<br>
When a task definition is actually run, it's called a task, and that's a bunch of containers that are running right now, shared together on a
single host.<br>
Services take tasks, expose them to the Net as a whole, and ensure that they stay up all the time.<br>
So, you'd create a service that has 14 copies of a particular task, and it will ensure that many copies are running all the time, even if it has
to split it across several hosts.<br>
</p>

### 21.3.1 Advantages of ECS

<p>
It ties into the existing Amazon infrastructure very well. So, Amazon load balancers tie into ECS services to make your traffic available to the
Internet using the existing systems.<br>
You create your own instances in AWS. You have lots of control over that.<br>
Once you've got your instances running, you run an agent on them and cause them to join the cluster.<br>
This is kinda cool because you take the docker control socket, pass it to the agent that you run in docker,
and that allows the agent to control the host that it's running on. Kind of a neat system that makes running EC2 clusters very convenient from an
operational perspective.<br>
ECS provides a set of docker repos built in, so you don't have to run your own repo. ECS has them built in and they're made available to the machines
on ECS. It's perfectly easy to run your own repo along with ECS, you don't have to use the built-in one if you already have your own.<br>
Containers and tasks can be part of CloudFormation stacks, which makes deployment along with other resources in AWS very, very easy. So, if you have
containers that need to access to queues, and they need access to EC2 volumes, you can deploy all of this in one go and have it be cleaned up together
when the service ends.<br>
To get started with ECS, head on over to the Amazon ECS product page and click on Getting Started.
</p>

## 21.4 Other options

<p>
Other popular options include AWS Fargate from Amazon, which is a more automated version of AWS ECS.<br>
Docker Swarm from the people who make Docker.<br>
Google offers its Kubernetes engine.<br>
Microsoft offers the Azure Kubernetes Service for orchestrating your containers.
</p>


# A. Notes

## A.a Install netcat and commit

<p>
The ubuntu image used during the writing of this document hasn't netcat installed by default.
The workaround used is to install netcat on the image, commit the image and use this image for exercising.
</p>

```bash
> docker run -ti ubuntu bash
> apt-get update && apt-get install -y netcat
> exit
> docker commit container-name image-name
```

<p>

After that, always use `image-name` instead of `ubuntu`.

</p>