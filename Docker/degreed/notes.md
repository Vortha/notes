[[_TOC_]]

# TODO
https://dockerlabs.collabnix.com/docker/dca.html

---
# Notes
## Docker Compose

`depends_on` defines that the service must start after the one defined in this property

```yaml
services:
    node:
        image: nodeapp
        build:
            context: .
            dockerfile: node.dockerfile
        ports:
            - "3000:3000"
        volumes:
            - ./logs:/var/www/logs
        depends_on:
            - mongodb
```

The *node* service will just wait for the *mongodb* one to startup, but it won't wait until the database is actually up and running because there's not a great way for it to know that. So it's not like it's going to start *mongodb* and wait once is up, it's simply going to start *mongodb* first, and then it will start *node* right after that, so you would still have to add some retry type of logic built in.

Some commands:
- `docker-compose ps` lists the running containers
- `docker-compose stop` stops a given container or all containers
- `docker-compose start` starts a given container or all containers
- `docker-compose rm` removes a given container

When you have `depends_on` property in a service, let's say *node*, the *node* container has been removed and you want to start just this service, use the `--no-deps` arg

```shell
docker-compose up
docker-compose stop node
docker-compose rm node
docker-compose start node # returns an error
docker-compose up -d --no-deps node
docker-compose ps
```

This will recreate the *node* container without touching the *mongodb* one.

Some other commands:
- `docker-compose logs [service ...]` shows the logs
- `docker-compose logs --tail=5` shows the last 5 lines from the end of the log files
- `docker-compose logs --follow` follows the logs so that if there is any error that pops up, you would see it live as the container experiences that error.
- `docker-compose exec node sh` shell into *node* container

### Scale containers
`docker-compose up -d --scale api=4` scales *api* service to 4 containers

```yaml
services:
    api:
        image: my-api
        build:
            ...
        deploy:
            replicas: 2 # define number of replicas to create for a service deployment
            restart_policy: # define the restart policy
                condition: on-failure
                delay: 5s
                max_attempts: 3
                window: 120s
```

When defining replicas, don't define the local port for the service (E.g.: `ports: - "3000:3000"` must be just `ports: - "3000"`). The local port will be automatically assigned and you can retrieve it by listing the containers using `docker-compose ps`.

## Architecture and Theory
We use the command line to create a new container- The client takes the command and makes the appropriate API request to the containers/create endpoint in the engine. The engine pulls together all of the required kernel stuff, and out pops a container.

### Kernel Internals
We use two main building blocks when we're building containers: **namespaces** and **control groups**. Both of them are Linux kernel primitives. Namespaces are about isolation, and control groups are about grouping objects and setting limits.

Namespaces allow us to take an OS and curve it into multiple isolated virtual operating systems: containers. Each container gets its own virtual or containerized root file system, its own process tree, its own Eth0 interface, its own root user, the full monty (everything). The kernel is shared, but everything is isolated so that the stuff inside of one container doesn't even know about the other ones.

In Linux we have multiple namespaces - Process ID (pid), Network (net), Filesystem/mount (mnt), Inter-proc comms (ipc), UTS (uts), User (user) - and Docker Container is basically an organized collection of them.

We need something to police the consumption of system resources shared by the containers. In the Linux world, this is control groups (cgroups). The idea is to group processes and impose limits.

### The Docker Engine
The engine's right at the core of what Docker does, and it's what makes containers easy. It exposes an API for us, it interfaces with all the kernel magic and out pop containers.

We have got:
* The (Docker) **client** where we slap in commands like docker container run;
* A **daemon** - Docker API... - implementing the REST API;
* **Containerd** - Execution/lifecycle -  that is the container supervisor that handles execution of the lifecycle operations, things like start, stop, pause, and unpause;
* **OCI** - Runtime - layer that does the interfacing with the kernel.

How it works in Linux: the client asks the daemon for a new container. The daemon gets containerd to start and manage containers and runc at the OCI layer actually builds them.

## Working with Images
An image is a read-only template for creating application containers. Inside of it is all the code and supporting files to run an application. Now, images are build-time constructs, and containers are their runtime siblings.

An image is a bunch of layers that are stacked on top of each other, but it looks and feels like a unified file system.

We store images in a registry, which could be cloud or on-prem, and we pull them down to our hosts using the docker image pull command.

The image is read-only, so for each container we create a thin writable layer, and effectively lashing on top of the read-only image. Then any writes and updates the container needs to make get performed in this layer.

### Images in Detail
We do `docker image pull redis` and each of the "Pull complete" in the output is a layer. So, an image is actually a bunch of independent layers that are very loosely connected by a manifest file. This manifest file describes the image, like its ID and tags and when it was created. But importantly it also includes a list of the layers (a bunch of files - an independent layer) that get stacked and how to stack them. One layer in an image has no reference to other layers. All of that's done in the manifest.

Pulling an image is actually a two-step process:

1. Get manifest
2. Pull layers

When getting the manifest, the client first looks for something called a fat manifest, and that's like a manifest of manifests. Basically it's a list of architectures supported and a manifest for each of those. So if your architecture is on the list, it points you to the image manifest that you need.

Once we've got the manifest for our architecture, we pass it for the list of layers, and we download them from the registry's blob store.

`docker image ls --digest` shows images' hash used to pull them.

We remove layers by executing `docker image rm redis`.

### Registries
Images live in registries. When we pull one to our Docker host, we are pulling it from a registry. Docker defaults to using Docker Hub, but other registries do exist. You can also get on-premises registries as well (Docker has got one of its own called Docker Trusted Registry that you get as part of Docker Enterprise Edition).

When we pull an image to localhost, we could say it gets pulled to a local registry. In Linux that's under /var/lib/docker and the name of your storage driver. On Windows, it's under C:\ProgramData\Docker\windowsfilter.

There are registries, in which there are repos, in which there are images.

registry/repo/image(tag)
```
docker.io/redis/lates
docker.io/nginx/1.13.5
```

Pushing a newer image to a repo does not automatically tag it as `latest`, it's a manual thing. That means just because an image is called or tagged as latest **does not mean** it actually is the latest.

We've got layers containing data, and for each one we compute its content hash that we use as its ID (content hash). When we push an image to a registry, we do a couple of things. We create a manifest that defines the image, what layers are in it, and we push the manifest in layers indipendently, but when we push the layers, we compress them (that's normal when we're hauling stuff over the internet). The layer IDs are content hashes, and compressing a layer changes its content. So when they arrive at the registry and a hash is computed to verify integrity, we'll get a fail that won't match. So, to get around this, when we build the manifest that will push to the registry, we populate it with new hashes of the compressed layers. On the host, we've got the uncompressed layers and their content hashes. Over the wire and in the registry's blob store, we've got compressed layers and what we're calling **distribution hashes**. That way the compressed layers and manifest get pushed to the registry and the registry uses the distribution hashes to verify that everything arrived as it should.

There are official and unofficial repos. Don't trust unofficial ones.

### Best Practices

* First and foremost, where possible, **use official images**: they're vetted, documented, and, generally speaking, moderately free from vulnerabilities;
* **Keep your images small**. Security principle numero uno, smaller is better;
* If you can't find what you're looking for, **use official images and build on them**. That way you know you're starting from a decent base and you add your app on top of it;
* **Be explicit referencing images**: there's a danger in always addressing images using `latest` tag.

## Containerizing an App
To get our app code into an image, we create a **Dockerfile**, that is basically a list of instructions on how to build an image with our code inside. Then we use the `docker image build` command to build the image.

### Containerizing an App
It's normal and probably a good practice to put the Dockerfile in the root folder of the app.

- It's a list of instructions for building images;
- It's convention that instructions in a Dockerfile are capitalized;
- \<INSTRUCTION\> \<value\>;
- FROM always first instruction;
- FROM = base image
- Good practice to list maintainer `LABEL maintainer="vortha@gitlab.com"`
- RUN = execute command and create layer
- COPY = copy code into image as new layer
- Some instructions add metadata instead of layers
- ENTRYPOINT = default app for image/container


`docker image build -t psweb .` build the Docker file in the current directory

`docker container run -d --name web1 -p 8080:8080 psweb` run the container web1 using image psweb

### Digging Deeper
Dockerfile notes:

- It's a text instructions for building images;
- Read by docker image build command;
- Read from the top, one instruction at a time;
- Write instructions UPPERCASE;
- FROM = first instruction & base image/layer;
- Good idea to start from official images;
- RUN = executes commands and creates layers;
- COPY = copy code into image as new layer;
- Some instructions add metadata instead of layers.

Build context: location of your code. Eg.: "My Dockerfile is located in the build context".

When you type `docker image build` command, whatever is in there gets sent to the daemon and processed as part of the build. Now for us, the client and daemon are on the same host, but it is totally possible to have clients talking to remote daemons over the network. So, the build context can absolutely be a remote Git repo.

`docker image build -t psweb https://github.com/nigelpoulton/psweb.git` the daemon pulls the build context across the wire from GitHub, but the process is just the same.

The layers created by the Dockerfile, use temporary containers that will be removed after the build has finished. **Only the layers that contain actual data are kept.**

Using the `docker history psweb` command, we can see the layers that contain data (SIZE > 0B).

### Multi-stage Builds
Small images is what we're aiming for. Multi-stage Builds to the rescue. This is official supported technology from the core Docker project, and it's simple. We're talking a single Docker file and zero scripting wizardry. The Dockerfile contains more than one `FROM` instruction, each one marks a distinct build stage. Internally they're numbered from 0 at the top. But even better, we can give them friendly names.

```
FROM node:latest AS storefront
# some instructions that produce a heavy image

FROM maven:latest AS appserver
# some instructions that produce a heavy image...

FROM java:8-jdk-alpine AS production
COPY --from=storefront /usr/src/atsea/app/react-app/build/ .
COPY --from=appserver /usr/src/atsea/target/AtSea-0.0.1-SNAPSHOT.jar .
...
```

So stage 0 is storefront, and stage 1 is appserver, and then the last stage is production. A regular Docker image build steps through this from top to bottom. The first two stages produce two heavy images. The third stage referes to the other ones in order to copy stuff and keeps the image small.

The `COPY --from` instruction is where the magic of multi-stage builds really comes into play. It grabs that image created by the storefront build stage, and it picks out just the application code that we want, and it leaves the rest of the image behind. Same magic for the appserver code.

## Working with Containers
In the Docker world, the most atomic unit of scheduling is the container. It's the smallest unit of work we can create. The container is a thin, writable layer lashed on top of the image.

Containers have lifecycle: start, stop, pause, restart. The data inside a container doesn't get deleted when the container is stopped, but only if the container gets deleted.

**Linux containers need a Linux kernel and Windows containers need a Windows kernel.** So, if you build your apps for Linux, they are only going to run as Linux containers on a Linux Docker host.

Containers are all about apps, and the so-called gold standard when it comes to containerized apps is microservices. But, you can use them for almost anything (like traditional apps).

The ideal for a container is that they should be treated as ephemeral (they're short lived) and immutable (we really don't want to be logging into them and poking around). The idea with containers is that we build the image and deploy the container. Then, when we need to fix and change stuff, we build a new image with the fix, and then we switch out the old containers for new ones.

### Diving Deeper

`docker container ls` lists active containers

`docker container run -it apline sh` run a container and log into it (-it = interactive terminal) - **ctrl + P + Q** to get out of the container

When we start a new container, we tell it an application to run. In the above example it's just a shell (sh), in the real world though, this is going to be our application. **When we kill an app once we started the container, we also kill the container.**

We can sometimes start the container without telling it a process to run: `docker container run -it apline` this prompts the shell. This is because every image has a default process that it'll run if we don't tell it something different at runtime. If we inspect the alpine image `docker image inspect apline` we can find **"Cmd"** property telling containers based on this image what to run if we don't tell it something different.

Default processes for new containers:

* CMD: run-time arguments override CMD instructions
* ENTRYPOINT: run-time arguments are appended to ENTRYPOINT

`docker container rm $(docker container ls -aq) -f` removes all docker containers on the system given the IDs returned by the command, -f to force the operation so there's no need to do a separate stop first

### Logging
Two types of logs: daemon logs and container logs.

* Daemon logs are the logs from the Docker daemon, or the Docker Engine, depending on how you refer to it.
    * Linux:
        * systemd: `journalctl -u docker.service`
        * Non-systemd: `/var/log/messages`
    * Windows: `~/AppData/Local/Docker`
* Container logs/App logs: Docker is hoping that apps log to STDOUT and STDERR. Basically, the PID 1 process in every container is getting captured and it's getting forwarded. So, design your containers so that your applications are running his PID 1 and ideally, logging to STDOUT and STDERR (it is possible to do symlinks and shunt writes to those files to STDOUT and STDERR). Most of the major logging solution have Docker driver:
    * You can set default logging driver in daemon.json: any new containers will start using that driver;
    * Override per-container with `--log-driver` and `--log-opts`

You can inspect logs with `docker logs <container>` but it doesn't work with all drivers.

## Building a Secure Swarm
Despite the rise of Kubernetes, including its native support in certain versions of Docker, Swarm is absolutely still at the heart of everything Docker is doing. The reason is Swarm has two parts: the secure cluster part and the orchestrator part. The secure clustering bits, absolutely key to the future of Docker.

Secure Swarm cluster: at the highest level, it's a cluster of Docker nodes. We've got managers and workers and everything secure. So we've got mutual TLS where workers and managers mutually authenticate each other and all of the network chatter is encrypted. Plus, the cluster store is encrypted, and it gets automatically distributed to all managers. And we can use labels to tag nodes and customize the cluster how we want it. Once we've got the cluster, then we can start scheduling containers to it. So instead of running individual docker container run commands against specific nodes and every time having to think about which node we should be running them on, well instead of that, we just throw commands at the cluster and we let Swarm decide. So Swarm does all the workload balancing and the likes.

We can run two types of work on a cluster: native Swarm work and Kubernetes. Though at the time of recording, you can only run Kubernetes work on Docker Enterprise edition.

### Swarm Clustering Deep Dive
SwarmKit, a Docker's tool, lives on GitHub, and importantly, it's what powers swarm mode. It's fully integrated into the Docker Engine from v1.12+. You can choose between single-engine mode and swarm mode. Single engine is where you install individual Docker instances, and you work with them all separately. Swarm mode though, that's where you bring them all together as a cluster, and we can start throwing work at the cluster instead of having to hand pick a node for each and every container.

`docker swarm init` to swap Docker into swarm mode

When you swap to swarm mode, the node is the first manager of the swarm, and the first manager in any swarm is automatically elected as its leader. It's the root CA of the swarm. 
`docker swarm init --external-ca ...` to pass external CAs. This node is also issued itself a client certificate, built a secure cluster store, which, by the way, is etcd, and that's automatically distributed to every other manager in the swarm, and it's encrypted. And all of this for free, as in we didn't have to do anything, it's done for us, including a default certificate rotation policy. It's also created a set of cryptographic join tokens, one for joining new managers and another for joining new workers.

In order to add a manager to the cluster, take another node running Docker in single-engine mode, and run `docker swarm join <manager_join_token>` command on it. When joining the cluster, it's been issued its own client certificate, that identifies who it is, the Swarm it's a member of, and it's role as a manager.

Every swarm has a single-leader manager. When you issue commands at the cluster,  you can issue them at any of the managers: if you hit a follower manager, it's going to proxy commands to the leader. If a leader fails, we have an election, and one of the followers gets elected as a new leader, but all this is handled by Raft. Within a swarm, all the distributed consensus stuff is done by Raft (the same as Kubernetes). Raft's the go-to solution for distributed consensus these days.

The ideal number of managers is 3, 5 or 7. Any more than 7, and you can end up spending more time thinking about decisions than actually acting on them. Odd numbers increase the chances of achieving quorum and therefore avoiding split brain.

Connect your managers over decent, reliable networks. So for instance, if you're in Amazon Web Services, don't go putting them in different regions. Across availability zones within a regoin, that's probably all right, but going cross region is just asking for pain.

Adding workers is the same `docker swarm join <worker_join_token>`. When you join a worker, it does not get access to the cluster store, that's just for managers, but what each worker does get is the full list of IPs for all the managers. So if one of them dies, the workers can just talk to the others. Also, they all get their own certificates, which identify who the worker is, the swarm that it's a member of, and what its role is. The workers do all the application work, and on a swarm, that's either native Swarm work or it's Kubernetes.

### Building a Secure Swarm

`docker system info` we can check if the swarm is active

`docker swarm init` init swarm with the current node as manager and leader

`docker swarm join-token manager` to get the token to use when you want to add managers to the cluster

`docker swarm join-token worker` to get the token to use when you want to add workers to the cluster

`docker node ls` to list Docker nodes in the swarm

`docker swarm join-token --rotate worker` - must be executed on a manager node - to generate a new worker token that must be used when joining workers' cluster. The old token can't be used anymore. Existing node membership is unaffected


> Manager join token `SWMTKN-1-3jxvdt8j...r3ispd5p-1gtve8fo...eukpij29`

> Worker join token  `SWMTKN-1-3jxvdt8j...r3ispd5p-cm1ymqlf...1wx7j47e`

The first part of the join token `SWMTKN` identifies what it is, and you can pattern match on that to stop them beign accidentally posted on your website or whatever.

The second part `1-3jxvdt8j...r3ispd5p` is a hash of the cluster's certificate, mutual authentication.

After the dash `cm1ymqlf...1wx7j47e` is the bit that determines if it's going to be admitted as a worker or a manager.

It's just the last part the difference between the two tokens

**Lock your Swarm with Autolock:**

* Prevents restarted Managers from automatically re-joining the Swarm;
* Prevents accidentally restoring old copies of the Swarm;

Autolock is not enabled by default:
* `docker swarm init --autolock` Autolock new Swarm
* `docker swarm update --autolock=true` Autolock existing Swarm

Enabling Autolock will require to enter the unlock key (returned by the enable autolock command) for certain operations. So, use `docker swarm unlock` and enter the unlock key, then execute the *"locked"* command.

In order to update the certificate expiry date we use `docker swarm update --cert-expiry 48h` (replace 48h with the amount of time you want).

## Container Networking
### Container Types
**Bridge Network** (sometimes we call it single house networking) is the oldest, and it's the most common, and honestly, it's the crappiest, but it's turned on by default. So you've got a host and it's running Docker, and it's got a built-in network called bridge (or nat on Windows). The default bridge network is also called docker0. We put containers on this network and they can all talk to each other, but containers on separate bridges, that's a bit of a hassle because each one of these bridges is isolated (Layer 2 networks isolation and it'd be the same even if they were on the same host). The only way to get in or out is to map ports to the host. That's not really good.

If we run a new container without specifying the network, it's going to use the default (bridge). So if we need to reach a port on the container, we need to expose it on the host. `docker port <container>` to get the port mapping.

`docker network create -d bridge golden-gate` creates a new bridge network **golden-gate**

`docker network ls` to get the list of networks

`docker container run --rm -d --network golden-gate alpine sleep id` runs the container over the previously created **golden-gate** network

**Overlay Networks** (called also multi-host networks) are powerful as heck. So instead of isolated bridges scoped to a single host, an overlay is a single Layer 2 network spanning multiple hosts, and it doesn't matter if these are all on different networks. It's a single command to create one of these `docker network create` then you just attach containers and they all can talk to each other. Also, encryption is a walk in the park: the control plane, that is encrypted out of the box, and encrypting the data plane, it's a single command-line flag `docker network create -o encrypted`. But, the built-in overlay is container only. What if you need your containers to talk to VMs or physicals out on your existing VLANs?

`docker network create -d overlay overnet` creates a new overlay network **overnet**

`docker service create -d --name pinger --replicas 2 --network overnet alpine sleep id` creates a new service with two replicas over the previously created **overnet** network. As these replicas are on the same overlay, they can ping each other.

`docker service ls` to list the services

`docker service ps pinger` check which nodes replicas are on

**MACVLAN** (**transparent** on Windows) gives every container its own IP address and MAC address on the existing network, meaning containers can be visible as first class citizens on your existing VLANs, no bridges and no port mapping, directly on the wire if you will, but it requires promiscuous mode on the host NIC. That's bad if you're living in the public cloud because cloud providers generally don't allow promiscuous mode. So maybe take a look at IPVLAN for that, it's similar, but it doesn't require promiscuous mode.

### Network Services
**Service Discovery** is all about being able to locate services in a swarm, and **Load Balancing** lets us access a service from any node in the swarm, even nodes that aren't hosting the service, and as well, it balances load across them.

**Service Discovery**: every new service gets a name. That name gets registered with DNS on the swarm, and every service task, so every container in a service, gets a DNS resolver that forwards lookups to that swarm-based DNS service. **Long story short, all swarm services are pingable by name, with one caveat actually, so long as you're trying to ping it from on the same network.**

`docker service rm $(docker service ls -q)` removes all services

`docker service create -d --name ping --network overnet --replicas 3 alpine sleep id` create **ping** service with three replicas over the overnet network

`docker service create -d --name pong --network overnet --replicas 3 alpine sleep id` create **pong** service with three replicas over the overnet network

Ping is able to ping pong, same for pong that is able to ping ping. It's service scoped, meaning services on different overlays can't find each other.

**Load Balancing** is a couple of things:

* First ingress load balancing: it's where external clients can access a swarm service via any of the nodes in the swarm. And, those nodes don't even need to be running the service. So say we've got like 10 nodes in a swarm, and a service with just 2 replicas. You can still hit any node in the swarm from the outside, even a node that's not running a replica from the service and still reach the service.
* The other aspect is load balancing work across all replicas in the service: we create a new service `docker service create -d --name web --network overnet --replicas 1 -p 8080:80 ngix` because this is a service, it maps this port swarm wide, so on every node in the swarm. And that's important because we're going with just one replica. So, let's say we've got three nodes in the swarm, and we want to be able to hit this one replica from any of those three nodes. That means 8080 on every node in the swarm gets mapped back to 80 on this service replica. Every node in this swarm gets the ingress network.

## Working with Volumes and Persistent Data
Container are ideal for nonpersistent, ephemeral, stateless stuff. They're also great for immutable design patterns. That's where we deploy something, and then it's hand off, never to be touched again. If you do need to change it, you just build a new one and deploy that. They usually don't generate data that you want to keep.

Two types of data:

* **Persistent**, is the stuff we want to keep;
* **Non-persistent**, that's the stuff we don't care about.

Containers are a cracking fit for the nonpersistent stuff.

If we want persistent data, we use the persistent storage, that is what we call volume storage (we have to specifically create it using `docker volume create` command). And as such, it lives outside of the world of the graph driver, so away from all that union mount stuff. Generally speaking, a volume is a directory on the Docker host that's mounted straight into the container at a specific mount point. But behind the scenes, it can be sitting on your fancy done, high-speed, uber resilient SAN or NAS with all the bells and whistles. Just so long as your storage system has a Docker volume driver, it's going to work. It's also an object in Docker that is managed and handled on its own, entirely separate from containers.

So, when you split containers, each one automagically gets its own nonpersistent graph driver storage (`/var/lib/docker` for Linux, `C:\ProgramData\Docker\windowsfilter` for WIndows). It's local block storage, and it lives and dies with the container ephemeral, but it is tied to the lifecycle of the container.

**Volume data** though, that's different. This exists outside of the container space and has its own docker volume subcommand, but it seamlessly plugs into containers and services while still being fully independent. What it means is **we can create and delete containers without touching volume data.** We can also attach a volume to more than one container (be careful to avoid corruption).

### Managing Volumes

`docker volume ls` to list volumes

`docker create myvol` to create new volume

`docker volume inspect myvol` to inspect volume

`docker volume prune` to remove all unused local volumes

**Volumes are independent from containers.**

### Attaching Volumes to Containers

`docker container run -dit --name voltest --mount source=ubervol,target=/vol alipine:latest` create a new container using the volume defined in --mount parameter. If you specify a volume that doesn't already exists, Docker is going to create it for you (which is all right, but if you think it does exist, but it's not, Docker is not going to tell you, is just going to create it

`ls -l /var/lib/docker/volumes/` lists all volumes in a Linux Docker host

If we put data into **voltest** container under the `/vol` directory, this will be available into `/var/lib/docker/volumes/ubervol/_data/` host directory.

If we remove the container `docker container rm voltest -f` the volume still exists (as it's fully independet, not tied in any way to the lifecycle of the container) and still has the data.

As long as a volume is in use by a container, you can't delete it, it's a safety latch. But if we delete that container and try the `docker volume rm ubervol` again, this time it goes, and it's removed from the file system as well.

**They work with services as well**. Use the `--mount` flag onto the `docker service create` command, exactly in the same way as the `docker run` command above.

You can also use it in a Dockerfile with the `VOLUME` instruction.

## Working with Secrets
A secret it's anything that you want to tell your app that's sensitive. No things like passwords and certificates, but it could be things like names of servers and services, anything that you think would be a security threat if it got exposed.

A Docker Secret is a text blob and it's up to 500k.

So long as you're running in swarm mode, secrets are baked right in. In any platform and cloud/premise and you get Docker, you've got secrets. It needs swarm mode because it leverages the security stuff. 

We've got a swarm, so you can create a secret `docker secret create`. This gets sent to the manager over a secure connection, and the manager puts it in the store where it's encrypted at rest. Then you create your service `docker service create` or maybe update one, you explicitly grant the service access to the secret. After that, a manager then sends the secret over a secured connection to just the nodes in the swarm that are running a replica for the service we just authorized. It's a least privilege model: only the workers that are running a replica for a service explicitly granted access to the secret get it. Workers not running replicas for an authorized service, they don't get it. In fact, they don't even know it exists. Once it's delivered to the node, it gets mounted inside the service task in its unencrypted form. On Linux, that's a file in `/run/secrets`, and importantly, that's a tmpfs volume, so an in-memory file system, meaning at no point is the secret ever persisted to disk on the node; it's only ever in memory. This part is a bit different on Windows, because Windows doesn't do in-memory file systems. So on Windows, it does get persisted to disk on the node. So, you might want to mount the Docker root directory using BitLocker or something. But that's it. And when the service is terminated or the secret's revoked `docker service rm green`, the worker node is instructed to flush it from memory.

Only Linux 1.13+ and Windows 17.06+.

### Secrets on the CLI
`docker secret create wp-sec-v1 ./classified` to create the secret using the *classified* file. The Docker Client sents the secret to the swarm manager daemon over a secure channel, and it is then securely stored in the swarm's Raft.

`docker secret ls` to list secrets

`docker secret inspect wp-sec-v1` to inspect a secret. It doesn't show the unencrypted contents of the secret. The only way to see it is to grant the service access to it.

`docker service create -d --name secret-service --secret wp-sec-v1 \`microsoft/powershell:windowsservercore \` PowerShell Start-Sleep -s 86400` creates a new **secret-service** in Windows

If we inspect the service, we can see the secret config `docker service inspect secret-service`

If we exec onto the container, and it's in Windows, `docker container exec -it <container_id> PowerShell` we can see the secret in `C:\ProgramData\Docker\secrets\` (`/run/secrets/` on a Linux container). The content of the secret is unencrypted `cat C:\ProgramData\Docker\secrets\wp-sec-v1`.

You can't delete a secret that's in use `docker secret rm wp-sec-v1` but you can if you delete the service first `docker service rm secret-service`.

### Secrets in Apps
Configure secrets via UI.

## Deploying in Production with Stacks and Services
**Stacks** are a bunch of services that work together. They work on the command line, the Universal Control Plane GUI, and even Docker Cloud.

We build an app, and that's a bunch of different pieces of code that talk to each other and work together to form a meaningful app. An they can all be different languages and all that, but we make each one a container. Then, for things like scalability and self-healing, we deploy them as Services. We're talking about deploying them as Docker Services. But each Service is still deployed and managed separately, and that's not ideal really. So, we group them as a **stack** (the highest layer of the Docker application hierarchy).

We deploy and manage stacks with the docker stack subcommand `docker stack deploy` or through Docker Cloud on the web UI. The stack file is basically a Compose file. So if you know your Docker Compose, this will be a breeze. But this all brings a host of things that are simply game changing. For starters, defining the desired state of an entire application in a spec file, this is at the heart of things like self-healing and the define once, deploy many philosophy. So we define the app and how many replicas and all that kind of stuff, we feed it to Swarm, and Swarm deploys it and manages it. It records the spec of each service in the cluster, and then it implements a bunch of background reconciliation loops that watch it. And if things fail, it fixes them. As well as all of that though, a stack file is a great way to document your app. So, a developer defines an Appwrite in a stack file, and it hands the stack file to operations. And right there, ops has a great description of the application. So it knows what services make it up, what images are in use, networks, volumes, number of replicas, all of that goodness. And it's ideal for change control. You can also fork different versions of your stack file for things like dev, test and prod. So, if you're deploying containerized apps that fit the microservice model and you want a way to deploy and manage them as a single app, stacks are a great way to go. And you get all the self-documentation, version control friendliness, and all the declarative self-healing magic thrown in for free.

### Stack Files
Basically **Compose files**, but they need to call at leas the Compose v3 file format, because all the stack-related stuff in them is only supported in that version and later. 

### Deploy and Manage Stacks
`docker stack deploy -c stackfile.yml voter` to deploy the stack defined in **stackfile.yml** file

`docker stack ls` to list stacks

`docker stack ps voter` to list **voter**'s services

`docker stack services voter` to list **voter**'s services

`docker service scale voter_vote=20` to HOT scale up to 20 replicas. If you redeploy the stack, it will get the replicas configured in the yml file. The right way to do it is to edit the config file and redeploy it `docker stack deploy -c stackfile.yml voter`.

## Enterprise Tooling
It's basically a bit of a hardened engine, and Ops UI, and a secure on-premises registry. Then as part of that, the bringing things like RBAC, image signing, and image scanning, image promotions, and a bunch more, all value add. Without EE (Docker Enterprise Edition), you get **CE**, **Community Edition**, and the boundaries between the two are a bit fluid. CE is basically the Community Edition of the engine. So things like the OSI layer and the containerd stuff, but all bundled with the API and daemon to give that core Docker experience, the opinionated runtime, if you will, and its on its own pretty aggressive release cycle. Then, stuff that's good in CE finds its way into EE, which has its own separate release cycle, a bunch of additional products, a support package, and a bunch of certified this and that. Now, at the lowest level of the engine, CE and EE are pretty much the same, just EE has fewer really cycles, and stricter configs, and stuff. Then, the other EE stuffs wraps around that and brings all the value add, and you pay for it.

## Definitions

**Container**: isolated area of an OS with resource usage limits applied
