Link: 
https://degreed.com/pathway/3pmez2n38n?path=docker

Circa 1 ora al giorno

| Title | Duration | Deadline | Completed at |
| ---      | ---      | ---      | ---      |
| 1.1 Getting Started with Docker | 1.44 H | 27/04 | 10/05 |
| 1.2  Building Docker App & Docker Compose - Building and Running Your First Docker App | 2.15 H | 03/05 | 16/05 |
| 1.2  Building Docker App & Docker Compose - Building and Orchestrating Containers with Docker Compose | 1.63 H | 17/08 | 12/08 |
| 1.3  Docker Deep Dive | 4.59 H | 21/08 | 22/08 |
| 2. Guided Labs - Introduction to Docker : The Basics (Coursera for Capgemini) | 1 H | 22/08 | 7/09 |
| 2. Guided Labs - Docker Essentials & Building a Containerized Web Application (Coursera for Capgemini) | 1 H | 22/08 | 15/09 |
| 2. Guided Labs - Containerization Using Docker (Coursera for Capgemini) | 1 H | 23/08 | 16/09 |
| 3. Docker Assessment | 0.5 H | 25/08 | N.A. |

---

TODO:
https://degreed.com/pathway/3pmez2n38n?path=docker 3 domains per day. Due date: 20/09

---

Circa 2 ore al giorno

Link:
https://app.pluralsight.com/paths/skills/managing-docker-in-production

Duration: 22 H
Deadline: 06/06

Link:
https://app.pluralsight.com/paths/skills/using-kubernetes-as-a-developer

Duration: 13 H
Deadline: 14/06

Link:
https://app.pluralsight.com/library/courses/kubernetes-getting-started/table-of-contents

Duration: 3.3 H
Deadline: 16/06

Link:
https://app.pluralsight.com/library/courses/docker-kubernetes-big-picture/table-of-contents

Duration: 1.78 H
Deadline: 20/06
