<p>Dependency Injection - Design pattern</p>

<p>HelloWorld Spring/Maven: http://www.programcreek.com/2014/02/spring-mvc-helloworld-using-maven-in-eclipse/</p>

<p>Spring Tutorial: https://www.tutorialspoint.com/spring/spring_bean_scopes.htm</p>

<p>
    <h2>Associazione: Aggregazione vs Composizione</h2>
    <p>
        <p>
            Data un'Associazione fra due classi, risolvo l'Associazione inserendo come attributo di una classe un oggetto dell'altra e lo istanzio:
            <p>
<code>public class Quadro {
    private String titolo;
    private int altezza;
    private int larghezza;
    private Autore autore = new Autore();
}</code>
            </p>
            In questo modo indico l'Associazione fra Quadro e Autore.
        </p>
        <p>
            Per identificare una relazione di Composizione, devo istanziare l'oggetto attributo con l'oggetto passato come parametro del costruttore della classe dell'Associazione:
            <p>
<code>public class Quadro {
    private String titolo;
    private int altezza;
    private int larghezza;
    private Autore autore = new Autore();

    public Quadro(String titolo, int altezza, int larghezza, Autore autore) {
		super();
		this.titolo = titolo;
		this.altezza = altezza;
		this.larghezza = larghezza;
		this.autore = autore; // Composizione
	}
}</code>
            </p>
        </p>
        <p>
            Per identificare l'Aggregazione, semplicemente predispongo lo spazio per l'oggetto attributo all'interno del costruttore:
            <p>
<code>public class Museo {
	private String nome;
	private String citta;
	private String tipo;
	private Quadro[] quadri;

	// Predispongo lo spazio per i quadri senza doverli assegnare sul momento
	// per rispettare la relazione di Aggregazione
	public Museo(String nome, String citta, String tipo, int dimMuseo) {
		super();
		this.nome = nome;
		this.citta = citta;
		this.tipo = tipo;
		this.quadri = new Quadro[dimMuseo]; // Aggregazione
	}
}</code>
            </p>
        </p>
    </p>
</p>
<hr>
<p>
    <b>c3p0 per connessioni cached (Hibernate)</b>
</p>

<hr>
<p>
    Ogni DAO rilancia l'eccezione al service specifico, sottoforma di eccezione trasformata per il service:
    <p>
<code>public class DAO {

    public void method() {
        try {
            // codice che lancia una DAOException
        } catch (DAOException e) {
            throw new ServiceException("Messaggio di errore");
        }
    }
}</code>
</p>

<h2>Java Web</h2>

<p>Resource: oggetto la cui classe ha il metodo <code>close()</code></p>

<p>
    Quando realizzo il codice Java, specchio le relazioni del DB: relazioni 1-N si traducono mettendo l'identificativo dell'entità dell'1 nella classe dell'N:<br>
    <b>Database</b><br>
<code>    ______                  _____________
    |Utente|________        |ContoCorrente|______________________
    |id|nome|cognome|-n---1-|numero|saldo|data_apertura|id_utente|</code><br>
    <b>Java</b><br>
    <code>Utente:
    int id;
    String nome;
    String cognome;</code><br><br>
    
    <code>ContoCorrente:
    int numero;
    double saldo;
    Date dataApertura;
    int idUtente;</code><br>
</p>

<h2>Java EE</h2>
<p>Java con un'ulteriore libreria per le applicazioni distribuite.</p>

<p>
    Java EE offre due container separati:
    <ul>
        <li>Web container (parte web)</li>
        <li>EJB container (parte business)</li>
    </ul>
</p>

<p>
    Web container: libreria di java che ci aiuta a realizzare una web application server side.
    EEJB container: libreria di java che ci aiuta a realizzare un'applicazione di business back-end.
</p>

<p>
    Container: speciale tipo di framework. Cioè un framework + gestione del ciclo di vita degli oggetti.
    Framework: è una libreria che utilizza il pattern InversionOfControl.
    Pattern: soluzione architetturale ad una problematica ricorrente.
</p>

<p>Quando un framework gestisce la creazione e la distruzione degli oggetti, si chiama container (esempio: main parte del framework).</p>

<p>
    (Alcune) regole HTTP:
    <ul>
        <li>Il Client inizia (sempre) la comunicazione richiedendo qualcosa e il Server risponde;</li>
        <li>L'indirizzo (URL) del servizio deve essere formato come: http:\\IP:PORT\WEBAPP\RESOURCE (RESOURCE: path ad una risorsa sul server).</li>
    </ul>
</p>

<p>Il Server è un programma (Tomcat) ma anche la macchina che ospita il programma.</p>

<p>
    JSP:<br>
    <code><% // codice Java %></code>
    <code><%= // codice Java da stampare %></code>
    
    <code><%@page import="java.util.Date"%>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    	pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
    </head>
    <body>
    
    <%
    	Date date = new Date();
    %>
    
    <h2>La data corrente è: <%= date %></h2>
    
    </body>
    </html></code>
</p>

<h2>Spring</h2>
<p>
    <ol>
        <li>Creo un nuovo Web Dynamic Project;</li>
        <li>Click destro sul progetto -> Configure -> Convert to a Maven Project;</li>
        <li>Aggiungo le librerie di Spring per Maven*</li>
    </ol>
    
    *
<code>  <dependencies>
		<!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>4.3.9.RELEASE</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework/spring-aop -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
			<version>4.3.9.RELEASE</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>4.3.9.RELEASE</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework/spring-web -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>4.3.9.RELEASE</version>
		</dependency>
	</dependencies></code>
    
    <p>
        Creo una classe controller per ogni funzionalità della web app.<br>
        <b>Packaging</b><br>
        <ul>
            <li>java.src.controller</li>
            <li>java.src.model</li>
            <li>WEB-INF/jsp</li>
        </ul>
        
        Tramite l'annotation <code>@Controller</code> si determina una classe Controller. Quando Spring legge l'annotazione e fa il resto.<br>
        Definisco una classe Controller la quale ha i metodi che implementano i vari servizi (contestualizzati). I metodi del Controller richiamano le classi Service per soddisfare la richiesta. Sui metodi della classe Controller che identificano un servizio, inserisco l'annotation <code>@RequestMapping("/NomeServizio")</code>. Il metodo della classe Controller restituisce (come minimo) il nome della JSP da visualizzare. Se ci sono dati da passare, restituisco un oggetto di tipo ModelAndView:
<p><code>@Controller
public class MathController {

    // Se non ci fossero dati da passare
	public String somma(double n1, double n2) {
		MathService ms = new MathServiceImpl();
		double ris = ms.somma(n1, n2);

		return "risSomma";
	}

	// Se ci fossero dati da passare
	@RequestMapping("/Somma")
	public ModelAndView somma(double n1, double n2) {
		MathService ms = new MathServiceImpl();
		double ris = ms.somma(n1, n2);

		return new ModelAndView("risSomma", "somma", ris);
	}
}</code></p><br><br>
<p><code>public interface MathService {

	public double somma(double a, double b);
}</code></p><br><br>
<p><code>public class MathServiceImpl implements MathService {

	@Override
	public double somma(double a, double b) {
		return a + b;
	}

}</code></p></p>
        <p>
            Per più package controller:<br>
            <code><context:component-scan base-package="controller, x, y" /></code>
        </p>
    </p>
</p>

<p>
    <h2>OracleDB</h2>
    <p>Operazioni preliminari: avviare il DB dai servizi di Windows, connessione al DB come amministratore e avviarne l'istanza.</p>
    
    <table>
        <tr>
            <th colspan="2"><b>Comandi</b></th>
        </tr>
        <tr>
            <td>SQLPLUS</td>
            <td>Avvio oracleDB</td>
        </tr>
        <tr>
            <td>sys/{password} as sysdba</td>
            <td>Usato per fare l'accesso al db come amministratore</td>
        </tr>
        <tr>
            <td>startup</td>
            <td>Avvio dell'istanza</td>
        </tr>
        <tr>
            <td>connect {user}/{password}</td>
            <td>Connessione al db con utente {user} e password {password}</td>
        </tr>
        <tr>
            <td>select table_name from user_tables</td>
            <td>Mostra le tabelle visibili all'utente</td>
        </tr>
        <tr>
            <td>commit</td>
            <td>Rende effettive le operazioni DML</td>
        </tr>
        <tr>
            <td>set oracle_sid = {dbname}</td>
            <td>setta {dbname} come db di default</td>
        </tr>
        <tr>
            <td>alter user {username} identified by {password} account unlock</td>
            <td>sblocca lo user {username} e gli setta la password {password}</td>
        </tr>
        
    </table>
    <p>Per disabilitare la connessione come sys (as sysdba) per tutti gli utenti, andare sulla configurazione dei gruppi e cancellare tutti gli utenti dal gruppo ora_dba: strumenti di amministrazione -> gestione computer -> utenti e gruppi locali -> gruppi.</p>
    
    <p>I file vengono salvati sul disco rigido e nel DB viene salvato un puntatore al file.</p>
    
    <p>user_tables, all_tables, dba_tables: le prime due possono essere da chiunque ma si adattano all'utente che le visualizza: la prima mostra solo ciò che appartiene all'utente, la seconda mostra ciò che appartiene all'utente e ciò su cui l'utente ha i privilegi. La dba_tables mostra tutte le tabelle (amministratore).<br>
    Le V${...} tables sono consultabili solo dall'amministratore e hanno informazioni sul database.</p>

    <p>Identificativi e nomi tabelle vengono convertite in uppercase. Singoli apici per memorizzare dati così come li si scrive, altrimenti vengono inseriti in uppercase.</p>

    <p>dual - tabella formata da una riga e una colonna</p>
    
    <p>Transazione: insieme di comandi accettati o rifiutati insieme. I comandi DDL fanno una transazione a sé.<br>
    Se devo dare un comando DDL, blocco il lavoro che sto facendo, apro un'altra finestra e do i comandi DDL.</p>
    
    <p>user_tables (vista che) fa parte dello schema esterno (slide 43 - feb02_BD01.ppt).<br>
    Ogni utente vede solo lo schema esterno (viste visibili dall'utente), le tabelle costituiscono lo schema logico. Solo SYS vede lo schema fisico del DB intero.</p>
    
    <p>PL/SQL estensione procedurale di SQL. Non può essere compilato ed eseguito esternamente, è solo interno al database Oracle.</p>
    
    <p>Istanza: insieme delle righe di una tabella / dbms in ram.</p>
    
    <p><code>Grant</code> e <code>Revoke</code> fanno parte del DDL.</p>
    
    <p>Modello logico -> Relazione (tabelle che non hanno due righe uguali)
    Modello concettuale -> Entità-Relazione</p>
    
    <p><h4>Filastrocca</h4>
        <blockquote>
        Chi scrive non blocca chi legge, chi legge non blocca chi scrive, chi scrive blocca chi scrive sulle stesse righe, chi legge blocca chi scrive solo nel caso di select for update.
        </blockquote>
    </p>
    
    <p>Relazione uno a molti: una identificazione esterna è possibile solo attraverso una relationship a cui l’entità da identificare partecipa con cardinalità (1,1).</p>
</p>