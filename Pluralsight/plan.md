## Links
* Degreed: https://degreed.com/pathway/3pmez2n38n?path=docker
* Pluralsight: https://app.pluralsight.com/course-player?clipId=608b3405-18d6-4b5c-abdc-3b5592906b30
* Coursera: https://www.coursera.org/programs/capgemini-learning-program-71mtd?authProvider=capgemini&isExternal=true&productId=o6b4PczvEeqgiQpd1tLriw&productType=course&showMiniModal=true


| Ref | Duration | Due date |
| ---      | ---      | ---      |
| Pluralsight - Swarm Mode and Microservices | 63 min | 8/2 |
| Pluralsight - Setting up Your Development Environment | 26 min | 9/2 |
| Pluralsight - Building and Running Your First Docker App - Create an Application Image | 36 min | 9/2 |
| Pluralsight - Building and Running Your First Docker App - Run an Application Container | 28 min | 10/2 |
| Pluralsight - Building and Running Your First Docker App - Communicate between Multiple Containers | 37 min | 10/2 |
| Pluralsight - Building and Orchestrating Containers with Docker Compose - Getting Started with Docker Compose | 21 min | 11/2 |
| Pluralsight - Building and Orchestrating Containers with Docker Compose - Building Images with Docker Compose | 24 min | 11/2 |
| Pluralsight - Building and Orchestrating Containers with Docker Compose - Orchestrating Containers with Docker Compose | 29 min | 14/2 |
| Pluralsight - Building and Orchestrating Containers with Docker Compose - Additional Docker Compose Features | 22 min | 14/2 |
| Pluralsight - Docker Deep Dive - Architecture and Theory | 37 min | 15/2 |
| Pluralsight - Docker Deep Dive - Working with Images | 35 min | 15/2 |
| Pluralsight - Docker Deep Dive - Building a Secure Swarm | 24 min | 16/2 |
| Pluralsight - Docker Deep Dive - Container Networking | 18 min | 16/2 |
| Pluralsight - Docker Deep Dive - Working with Volumes and Persistent Data | 14 min | 16/2 |
| Pluralsight - Docker Deep Dive - Working with Secrets | 16 min | 17/2 |
| Pluralsight - Docker Deep Dive - Deploying in Production with Stacks and Services | 19 min | 17/2 |
| Pluralsight - Docker Deep Dive - Enterprise Tooling | 17 min | 17/2 |
| Coursera - Introduction to Docker : The Basics (Coursera for Capgemini) | 60 min | 18/2 |
| Coursera - Docker Essentials & Building a Containerized Web Application | 180 min | 22/2  |
| Coursera - Containerization Using Docker | 75 min | 23/2 |
| Pluralsight - Managing Docker in Production - Getting Started with Docker | 86 min | 25/2 |
| Pluralsight - Managing Docker in Production - Managing Docker on Linux Servers | 192 min | 2/3 |
| Pluralsight - Managing Docker in Production - Managing Docker on Windows Servers | 76 min | 4/3 |
| Pluralsight - Managing Docker in Production - Managing Container Images | 65 min  | 7/3 |
| Pluralsight - Managing Docker in Production - Managing Docker Networking | 180 min | 9/3 |
| Pluralsight - Managing Docker in Production - Monitoring Containerized Application Health with Docker | 143 min | 11/3 |
| Pluralsight - Managing Docker in Production - Preparing Docker Apps for Production | 175 min | 15/3 |
| Pluralsight - Managing Docker in Production - Getting Started with Docker Swarm | 252 | 22/3 |
