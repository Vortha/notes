[[_TOC_]]

# Util

Virtual machine vs container: the first virtualizes the hardware, the second virtualizes the operating system.

Images - Stopped containers
Containers - Running images

To exit the container without letting the container stop, press Ctrl+P+Q.

To stop all containers:
`docker stop $(docker ps -aq)`

To remove all containers:
`docker rmi $(docker images -q)`


![Docker recap 1](./img/docker-recap1.PNG)

# Swarm Mode and Microservices

## Swarm Mode Theory
The new stuff in Docker 1.12 and later:
* It's all about true native clustering

## Terminology
A native cluster = A swarm
A collection of Docker engines joined into a cluster is called a swarm. The Docker engines running on each node in  the swarm are said to be running in swarm mode. The swarm mode is entirely optional
Manager nodes maintain the swarm
* H/A - recommended 3 or 5
* Only one is leader

A swarm itself consists of one or more manager nodes and then one or more working nodes. The manager nodes look after the state of the cluster and dispatch tasks and the likes to the worker nodes.
Managers are highly available, meaning that if one or two, or however many of them go away, the ones remaining will keep the swarm going.
Behind the scenes: while you can have X number of managers, an odd number is highly recommended, and only one of them is ever the leader or primary. All managers maintain the state of the cluster, but if a manager that's not the leader receives a command, it's going to proxy that command over to the leader. And then the leader is going to action it against the swarm, and you can spread managers across availability zones, regions, data centers, whatever suits your high availability needs. But of course, that's alla going to be dependent on the kind of networks that you have. You're going to want reliable networks.

Raft: the protocol that's used behind the scenes to bring order ensuring we achieve a distributed consensus.
Manager nodes are working out as well, so it's totally possible to have a swarm where every node is a manager node, though more than five manager nodes generally isn't thought of as a good idea. The premise here is that the more managers you have, the harder it is to get consensus or the longer it takes to get consensus.

Worker nodes execute tasks: they just accept tasks from managers and execute them. That leads us nicely to services.
Services are also a new concept introduced with swarm mode, meaning if you're not running in swarm mode, then you can't do services. A service is a declarative way of running and scaling tasks.

A task is the atomic unit of work assigned to a worker node. We as developers or sys-admins or whatever, tell the manager about services, then the manager assigns the work of that service out to worker nodes as tasks.
For now, task means container (actually it is more, as it has metadata info to run the container).

To summarize: we've got a swarm consisting of a bunch of manager nodes and worker nodes. We define services, declare them to the manager via the standard Docker API, albeit new endpoints in the API. The manager then splits the service into tasks and schedules those tasks against available nodes in the swarm, and then in order to deploy complex apps consisting of multiple distributed, indipendently scalable services, we've got stacks and distributed application bundles. 

## Building a Swarm
Enable swarm mode:
`docker swarm init --advertise-addr 127.0.0.1:2377 --listen-addr 127.0.0.1:2377`

*--advertise-addr* tells Docker, no matter how many NICs and IP addresses this machine's got, this is the one to use for swarm-related stuff like exposing the API.

*--listen-addr* this is what the node listens on for swarm manager traffic.

Any other nodes that want to be part of this swarm are going to need to be either on the same network or you're going to need roots or routes in place on your network so that those other nodes can reach this IP.

Add a worker to this swarm:
`docker swarm join --token SWMTKN-1-56fzzv2wbku1i0pgbhqsdyk9v62zfdgbij5pwunba9ooe5bdf7-450ua3s2bwkddn6ygyd1zmiph 127.0.0.1:2377`

Get the command to run in order to add managers or workers:
`docker swarm join-token [worker|manager]`

The only thing that distinguish managers from workers is the token they use to join.
Use `docker info` in order to get also swarm info.

Add a manager to this swarm:
`docker swarm join --token SWMTKN-1-56fzzv2wbku1i0pgbhqsdyk9v62zfdgbij5pwunba9ooe5bdf7-49s0vq7f0yvqa5rdg94ypd062 127.0.0.1:2377 --advertise-addr 127.0.0.2:2377 --listen-addr 127.0.0.2:2377`

**notice the --advertise-addr 127.0.0.2:2377 --listen-addr 127.0.0.2:2377**

Add a worker to this swarm:
`docker swarm join --token SWMTKN-1-56fzzv2wbku1i0pgbhqsdyk9v62zfdgbij5pwunba9ooe5bdf7-450ua3s2bwkddn6ygyd1zmiph 127.0.0.1:2377 --advertise-addr 127.0.0.3:2377 --listen-addr 127.0.0.3:2377`

**notice the --advertise-addr 127.0.0.3:2377 --listen-addr 127.0.0.3:2377**

List the swarm (can be run only on manager nodes):
`docker node ls`

Promote a worker to a manager (can be run only on manager nodes):
`docker node promote <node_id>`

Managers act also as workers, this is why the docker info returns the number of managers doubled.

## Services
Services, one of the major, major constructs introduced in 1.12. They're all about simplifying and robustifying, large scale production deployments.

Services are all about declarativeness, but also the concepts of desired state and actual state with a form of reconciliation process running in the background, doing all the heavy lifting in the background required to make sure that actual match is desired.

`docker service create --name psight1 -p 8080:8080 --replicas 5 nigelpoulton/pluralsight-docker-ci`

```bash
gg62ezo7e96sonelsi7ex4mve
overall progress: 5 out of 5 tasks
1/5: running   [==================================================>]
2/5: running   [==================================================>]
3/5: running   [==================================================>]
4/5: running   [==================================================>]
5/5: running   [==================================================>]
verify: Service converged

C:\Users\Valerio>docker service ps psight1
ID             NAME        IMAGE                                       NODE             DESIRED STATE   CURRENT STATE            ERROR     PORTS
hgsgkwey5uqt   psight1.1   nigelpoulton/pluralsight-docker-ci:latest   docker-desktop   Running         Running 50 seconds ago
q994pfzy5r0u   psight1.2   nigelpoulton/pluralsight-docker-ci:latest   docker-desktop   Running         Running 49 seconds ago
uetvwnegcpbz   psight1.3   nigelpoulton/pluralsight-docker-ci:latest   docker-desktop   Running         Running 50 seconds ago
u2eqe4q3y4ta   psight1.4   nigelpoulton/pluralsight-docker-ci:latest   docker-desktop   Running         Running 49 seconds ago
xy7jz7bjxylc   psight1.5   nigelpoulton/pluralsight-docker-ci:latest   docker-desktop   Running         Running 49 seconds ago
```

To get service info:
`docker service inspect psight1`

We literally can hit any node in the swarm (even the ones that aren't running tasks) and will always get to our service.

Docker, Inc are calling this native container-aware load balancer the "routing mesh". It can augment existing non-container-aware load balancers!

## Scaling services
To scale services:
`docker service scale psight1=7`

This is the shortcut for:
`docker service update --replicas 7 psight1`

At the time of recording, newly added nodes to the service don't get existing running tasks automatically rebalanced across them.
At the time of writing (11/02/2022) they do.

## Rolling Updates
First up, create a new overlay network for this service:
`docker network create -d overlay ps-net`

Deploy a service over this network:
`docker service create --name psight2 --network ps-net -p 80:80 --replicas 12 nigelpoulton/tu-demo:v1`

Get the service info:
`docker service inspect --pretty psight2`

```bash
ID:             ws05bnn8q0scphq95s5ot93a8
Name:           psight2
Service Mode:   Replicated
 Replicas:      12
Placement:
UpdateConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:   1
 On failure:    pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:         nigelpoulton/tu-demo:v1@sha256:fa07d8c1e404df50aa7acadd23176fb9f688ab1b4338a795f115033e9e6a9c71
 Init:          false
Resources:
Networks: ps-net
Endpoint Mode:  vip
Ports:
 PublishedPort = 80
  Protocol = tcp
  TargetPort = 80
  PublishMode = ingress
```

**UpdateConfig:** when you initially create a service, so with docker service create, you can set a couple of update related settings.
`docker service create --update-parallelism 2 --update-delay 10m` set defaults for the service, meaning, let's say if we were to update the image in this service from v1 to v2, based on the config *--update-parallelism 2*, it's going to do it two tasks at a time. So roll two tasks to v2, wait for a 10 minute delay, and then roll another set, wait for 10 minutes, roll another set and so on for all the tasks in the service.
But we didn't set any of these when we started the service. So for us to update it, we can go:
`docker service update --image nigelpoulton/tu-demo:v2 --update-parallelism 2 --update-delay 10s psight2`

We can check the status (**Update Status**) with:
`docker service inspect --pretty psight2`


# Default ports
* Engine port: 2375
* Secured engine port: 2376
* Swarm port: 2377
