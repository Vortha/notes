# 1. Overview
## 1.1 Cloud Computing Key Enabling Technologies

- Cloud datacenters:
	- Provide scalability and reliability required for cloud computing
	- Strategically located in cost-effective areas
	- Trending towards a modular datacenter design
- Virtualization:
	- Most important technological driver in cloud computing
	- Multiple, virtual, operating systems run on a single physical computer
	- Available resources are optimized and more efficiently accessed
	- Generally offered under a pay-as-you-go pricing model
- Cloud APIs:
	- Cloud application programming interface, or API, serves as a layer between users and cloud services and resources
	- Commonly used API architecture is Representational State Transfer, or REST
- Cloud Storage:
	- Accessed using a web-based HTTP interface offered by cloud service providers
- Cloud Database:
	- Cloud computing isn't well suited to relational databases
		- Limited scalability and integrity with SQL base language
	- Nonrelational databases are better suited
		- Key-valye, item-oriented
		- Easily scalable

## 1.2 Definition of Cloud Computing

- NIST has loosely described cloud computing as:
  "A model for enabling convenient, on-demand network access to a shared pool of configurable
  computing resources, such as networks, servers, storage, applications, and services, that
  can be rapidly provisioned and released with minimal management effort or service provider
  interaction."
 
- The word "cloud" makes reference to two concepts associated with the cloud computing model:
	- Abstraction: a process in which specific details are hidden or ignored to simplify or
	  generalize something (I don't care where the physical server resides)
	- Virtualization: a process in which software manages hardware resources of physical
	  hosts so resources can be used more efficiently by multiple virtualized guest systems

## 1.3 Cloud Computing Core Attributes

- Core attributes of a cloud computing service include:
	- Elasticity
	- On-demand
	- Provider pooled computing resources
	- Metered service usage
	- Broad network access
<br>

- Elasticity: rapidly adding (provision) and/or removing (de-provision) resources
  and/or services as needed
- On-demand:
	- Cloud services available 24 hours a day
	- From anywhere, given network connectivity
- Provider pooled computing resources: cloud provider acquires and maintains all
  computing hardware on which cloud services run
- Metered service usage:
	- Subscription fee plus per use billing model (you pay for what you use):
		- Storage space and bandwidth consumption
- Broad network access:
	- Cloud services can be accessed from any type of device (Desktop, laptop,
	  mobile, ...)

## 1.4 Core Cloud Computing Service Models

- There are three primary cloud service models:
	- IaaS - Infrastructure as a Service: targeted at IT managers, not end users
	- PaaS - Platform as a Service: targeted at IT developers and software developers
	- SaaS - Software as a Service: targeted at end users, not IT admins or developers
<br>

- IaaS:
	- IT compute resources on which other cloud services run
	- Resources available remotely over a network<br>
    An example is a virtual machine: you do have to have some kind of server to host
    storage or host an application, so the virtual machine itself provides that
    infrastracture. At a more basic level, it's the physical server itself on which
    that virtual machine is running. As a customer you don't want to concern about
    that, we just want to be able to create a new virtual machine. The provider
    manages the hardware that ultimately drives that system.
    So virtual machines, storage, virtual networks all of those would fall under
    the category of IaaS.
<br>

- PaaS:
	- Create applications for users
	- Test applications
	- Deploy applications in the cloud
	- Performed using resources remotely<br>
    Developers these days need a fairly robust environment to develop their applications.
    They need to test them. They need to deploy them. They need to make sure that they are
    going to support the needs of the users. So you can certainly create your own development
    environment within your organization or you can simply subscribe to it in the cloud.
    They will provide the necessary virtual machines. They will provide all of the
    necessary services such as messaging or database, things like that. And they will
    even provide a means to test and deploy that application. So this way, we can simply
    subscribe to everything that is necessary and move the entire development
    environment into the cloud.
<br>

- SaaS:
	- Perform specific business tasks
	- Service delivered remotely over a network
	- Users can use any type of device
	- User device requires only a thin client
	- User may have limited application configuration settings<br>
    You don't want the user to have control over the configuration, that's done by
    administration. So this way, they gain access to their application. They can't
    disable something that they shouldn't be able to or enable something that they
    also shouldn't be able to. You can control its configuration so, in a lot of cases,
    that's very desirable. But all of them can be put together. You can have any or
    all of these services as part of your subscription. You don't generally go in
    and say "I want an SaaS subscription". It really does comes down to what you use.
    With all cloud services, you only pay for what you use. So if you decide, well,
    we do need some IaaS and some SaaS, well, that's perfectly fine. You don't have
    to have that PaaS, for example. You can leave really any part out. You can add
    any part in at any time and, again, pay only for what you need.

## 1.5 XaaS Cloud Computing Service Models

- XaaS - Anything as a Service:
    - Any IT service hosted on provider infrastructure
    - Remotely available over network

<br>

- XaaS cloud service models can fall under:
    - DaaS - Data as a Service
    - STaaS - Storage as a Service
    - CaaS - Communication as a Service
    - BPaaS - Business Processes as a Service

<br>

- Data as a Service:
    - Data is available on-demand and encrypted in transit (E.g.: applications accessing a database)
    - Geographic location is irrelevant
    - User can use any type of device
    - One version of a data file

<br>

- STaaS - Storage as a Service:
    - Off-site data backup storage and disaster recovery
    - Long-term data retention

<br>

- CaaS - Communication as a Service:
    - Communications infrastructure hosted on provider equipment
    - Remotely available over a network
    - Complex equipment setup handled by provider
    - Fault tolerance assured by provider
    - Pay-as-you-go <br>
    An example is Voice over IP

<br>

- BPaaS - Business Processes as a Service:
    - Outsourced business process automation
    - Relies on SaaS, PaaS, IaaS
    - Business process delivered over the cloud
    - Complexity of business process automation runs elsewhere
    - Faster time to market
    - Regulatory compliance handled by provider

## 1.6 Evaluating Cloud Security

- Important to consider data security when evaluating cloud computing solutions
- Data stored off-site, in the cloud infrastructure
- Less direct control over own data

<br>

- Data may be more vulnerable under third-party control <br>
  Usually these are very robust companies that provide these services, so they do invest
  an awful lot into their own security.

- Cloud computing solutions can offer:
    - Encryption
    - Password authentication
    - Basic identity management

<br>

- Cloud providers strive to progressively improve data security:
    - Data regulatory compliance
    - Handling of sensitive data
- Aim to ensure that users' data is as, or more secure in the cloud than on their own, on-site system

## 1.7 Cloud Service Evaluation

Factors to consider when determining the best option for cloud services:

- Network connectivity:
    - Internet hosted services
    - Flexible service options depending on business needs
- Scalability:
    - Capacity resources easily expandable on service provider's infrastructure
    - Subscription fee dependent
- Speed of implementation:
    - Time saving with immediacy of resource availability
- Greenness:
    - Shared resources equates to reductions in power consumption
- Control:
    - Less direct control over own data and services than traditional computing models
- Interoperability:
    - Measure cloud provider ease of service interoperability
- Compliance:
    - Importance of regulatory compliance requirements with the security and privacy of personal data
    - Sarbanes-Oxley Act and the Federal Information Security Management Act (FISMA)

## 1.8 Cloud Service Level Agreements

A Service Level Agreement, or SLA, is an agreement between a client and a service provider.<br>
It basically says "Here's what I need as a client", "Here's what we can provider as the provider", "Do they match up?"<br>
What you'll typically find in an SLA are things like:

- Service scope:<br>
    Defines expected level of cloud provider technical support (things like: are you available for support 24x7 or is it just during standard business hours?)
- Service availability:<br>
    Defines service uptime expectations, usually very high but they usually can't guarantee a 100%
- Customer requirements:<br>
    Negotiated and defined based on type of organization (private company, governmental agency, ...)
- Payment terms:<br>
    Defines payment frequency and subscription fees, whether or not IT service usage is measured (metering), so you can find out how much we are actually using

## 1.9 Business and Consumer Cloud Services

Cloud computing impacts consumers and businesses differently:

<br>Consumers:

- Personal storage and computing requirements
    - Documents
    - Photographs
    - Videos
    - Audio files
- Social networking purposes

<br>Businesses. Various types of businesses make use of cloud computing:
- Self-employed individuals
- Start-up businesses
- Small and medium-sized businesses
- Enterprises

<br>

Cloud computing entry costs can be affordable, so it's attractive to start-up and small business.<br>
Easier to transition a small business to the cloud:
- Minimal legacy data
- Fewer processes and applications to migrate

<br>

Personal data security and privacy paramount to some large businesses:
- Subject to strict corporate regulations
- Onsite data storage more appropriate

For some large businesses personal data security and privacy is a minimal concern:
- Cloud computing can improve time to market for their products and services