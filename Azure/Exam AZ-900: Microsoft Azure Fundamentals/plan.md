Total: 204 Units - 789 min (13.85 h)

| Part | Units | Time | Status | Deadline | Finish Date |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Part 1: Describe core Azure concepts | 22 | 86 | OK | // | // |
| Part 2: Describe core Azure services | 42 | 196 | OK | 4/02 | 4/02 |
| Part 3: Describe core solutions and management tools on Azure | 57 | 176 | OK | 5/02 | 5/02 |
| Part 4: Describe general security and network security features | 25 | 93 | OK | 6/02 | 6/02 |
| Part 5: Describe identity, governance, privacy, and compliance features | 35 | 130 | KO | 7/02 | 11/02 |
| Part 6: Describe Azure cost management and service level agreements | 23 | 108 | KO | 8/02 | 17/02 |

| Part | Module | Units | Time | Status |
| ------ | ------ | ------ | ------ | ------ |
| Part 1: Describe core Azure concepts | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 1: Describe core Azure concepts | Discuss Azure fundamental concepts | 6 | 24 | OK |
| Part 1: Describe core Azure concepts | Describe core Azure architectural components | 8 | 34 | OK |
| Part 2: Describe core Azure services | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 2: Describe core Azure services | Explore Azure database and analytics services | 10 | 41 | OK |
| Part 2: Describe core Azure services | Explore Azure compute services | 9 | 52 | OK |
| Part 2: Describe core Azure services | Explore Azure Storage services | 8 | 27 | OK |
| Part 2: Describe core Azure services | Explore Azure networking services | 7 | 38 | OK |
| Part 3: Describe core solutions and management tools on Azure | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best AI service for your needs | 8 | 23 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best tools to help organizations build better solutions | 8 | 21 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best monitoring service for visibility, insight, and outage mitigation | 8 | 22 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best tools for managing and configuring your Azure environment | 10 | 27 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best Azure serverless technology for your business scenario | 7 | 25 | OK |
| Part 3: Describe core solutions and management tools on Azure | Choose the best Azure IoT service for your application | 8 | 25 | OK |
| Part 4: Describe general security and network security features | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 4: Describe general security and network security features | Protect against security threats on Azure | 8 | 23 | OK |
| Part 4: Describe general security and network security features | Secure network connectivity on Azure | 9 | 32 | OK |
| Part 5: Describe identity, governance, privacy, and compliance features | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 5: Describe identity, governance, privacy, and compliance features | Secure access to your applications by using Azure identity services | 6 | 17 | OK |
| Part 5: Describe identity, governance, privacy, and compliance features | Build a cloud governance strategy on Azure | 12 | 48 | OK |
| Part 5: Describe identity, governance, privacy, and compliance features | Examine privacy, compliance, and data protection standards on Azure | 9 | 27 | OK |
| Part 6: Describe Azure cost management and service level agreements | Introduction to Azure fundamentals | 8 | 38 | OK |
| Part 6: Describe Azure cost management and service level agreements | Plan and manage your Azure costs | 8 | 43 | OK |
| Part 6: Describe Azure cost management and service level agreements | Choose the right Azure services by examining SLAs and service lifecycle | 7 | 27 | OK |



