package it.ats.formazione.spring.scuola.model.dao;

import javax.sql.DataSource;

import it.ats.formazione.spring.scuola.utility.SHA512;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminDAOImpl implements AdminDAOI {

	private DataSource dataSource;
	private Connection connection;

	@Override
	public boolean login(String username, String password) throws SQLException, NoSuchAlgorithmException {
		String query = "select * from utente where username = ? and password = ?";
		
		connection = dataSource.getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setString(1, username);
		preparedStatement.setString(2, SHA512.hashText(password));

		ResultSet resultSet = preparedStatement.executeQuery();

		if (resultSet.next()) {
			preparedStatement.close();
			connection.close();
			return true;
		}

		preparedStatement.close();
		connection.close();
		return false;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
