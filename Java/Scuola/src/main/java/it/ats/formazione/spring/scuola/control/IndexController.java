package it.ats.formazione.spring.scuola.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.ats.formazione.spring.scuola.model.Admin;

@RequestMapping("/")
@Controller
public class IndexController {

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView index() {
		Admin admin = new Admin();
		ModelAndView modelAndView = new ModelAndView("/index");
		modelAndView.addObject(admin);

		return modelAndView;
	}

}
