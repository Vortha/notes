package it.ats.formazione.spring.scuola.utility;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA512 {

	public synchronized static String convertByteToHex(byte data[]) {
		StringBuffer hexData = new StringBuffer();
		for (int byteIndex = 0; byteIndex < data.length; byteIndex++)
			hexData.append(Integer.toString((data[byteIndex] & 0xff) + 0x100, 16).substring(1));

		return hexData.toString();
	}

	public synchronized static String hashText(String textToHash) throws NoSuchAlgorithmException {
		final MessageDigest sha512 = MessageDigest.getInstance("SHA-512");
		sha512.update(textToHash.getBytes());

		return convertByteToHex(sha512.digest());
	}
	
	public static void main(String args[]) throws Exception {
		String password = "admin";

		if ((args.length == 1) && (args[0].length() > 0)) {
			password = args[0];
		}
		System.out.println("Password: " + password + " in SHA512 is:");
		System.out.println(hashText(password));
	}
}