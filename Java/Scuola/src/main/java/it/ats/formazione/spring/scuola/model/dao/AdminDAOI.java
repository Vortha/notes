package it.ats.formazione.spring.scuola.model.dao;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public interface AdminDAOI {
	public boolean login(String username, String password) throws SQLException, NoSuchAlgorithmException;
}
