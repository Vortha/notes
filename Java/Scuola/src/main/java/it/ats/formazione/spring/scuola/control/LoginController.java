package it.ats.formazione.spring.scuola.control;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sun.istack.internal.NotNull;

import it.ats.formazione.spring.scuola.model.Admin;
import it.ats.formazione.spring.scuola.model.dao.AdminDAOImpl;

@Controller
public class LoginController {

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@ModelAttribute("admin") Admin admin, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("home");

		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");

		try {
			AdminDAOImpl adminDAOImpl = new AdminDAOImpl();
			adminDAOImpl.setDataSource((DataSource) context.getBean("dataSource"));
			if (adminDAOImpl.login(admin.getUsername(), admin.getPassword())) {
				modelAndView.addObject("admin", admin);
				modelAndView.addObject("message", "Bentornato");
				session.setAttribute("admin", admin);
			} else {
				modelAndView.addObject("message", "Credenziali errate!");
			}
		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return modelAndView;
	}
}
