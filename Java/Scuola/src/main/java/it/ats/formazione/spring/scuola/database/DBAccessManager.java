package it.ats.formazione.spring.scuola.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;

public class DBAccessManager {

	public ResultSet select(String query) throws SQLException {

		Connection connection = null;
		DBLocator dbLocator = DBLocator.getDBLocator();
		connection = dbLocator.getConnection();
		PreparedStatement statement = connection.prepareStatement(query);
		ResultSet resultSet = statement.executeQuery();
		CachedRowSet cachedRowSet = new CachedRowSetImpl();
		cachedRowSet.populate(resultSet);
		connection.close();

		return cachedRowSet;

		// Connection connection = null;
		// DBLocator dbLocator = DBLocator.getDBLocator();
		// connection = dbLocator.getConnection();
		// PreparedStatement statement = connection.prepareStatement(query);
		// ResultSet resultSet = statement.executeQuery();
		// CachedRowSet rowSet = new CachedRowSetImpl();
		// rowSet.populate(resultSet);
		// connection.close();
		// return rowSet;
	}
}
