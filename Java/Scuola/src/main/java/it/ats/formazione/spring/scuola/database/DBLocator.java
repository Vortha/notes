package it.ats.formazione.spring.scuola.database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

public class DBLocator {

	private DataSource dataSource;
	private static DBLocator me;

	private DBLocator() {
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}

	public static DBLocator getDBLocator() {
		if (me == null) {
			me = new DBLocator();
		}

		return me;
	}
}
