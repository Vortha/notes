package controllo;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import modello.Persona;

public class Principale {

	public static void main(String[] args) {

		Principale principale = new Principale();
		principale.stampaSuFile();
		principale.leggiDaFileModoSemplice();
		// principale.leggiTestoDaFileModoComplicato();
		// principale.salvaECaricaOggettoSuFile();
		// principale.leggiListaOggettiDaFile();

	}

	public void stampaSuFile() {

		try {

			PrintWriter writer = new PrintWriter("dati_salvati.txt", "UTF-8");
			writer.println("Ciao questa e' la prima linea");
			writer.println("E questa e' la seconda");
			writer.close();

		} catch (IOException e) {

			System.out.println("Non riesco ad effettuare il salvataggio su file");

			e.printStackTrace();

		}

		System.out.println("Salvataggio effettuato correttamente...");

	}

	public void leggiDaFileModoSemplice() {

		FileReader reader;
		BufferedReader in;

		System.out.println("Sto leggendo i valori...\n");

		try {

			reader = new FileReader("dati_salvati.txt");
			in = new BufferedReader(reader);
			String sCurrentLine;

			while ((sCurrentLine = in.readLine()) != null) {
				System.out.println(sCurrentLine);
			}

		} catch (FileNotFoundException fnfe) {

			// il metodo printStackTrace() mi dice dove si � verificata
			// l'eccezione
			fnfe.printStackTrace();

			System.out.println("File non trovato");

		} catch (IOException e) {

			e.printStackTrace();

			System.out.println("Non riesco ad effettuare la lettura da file");

		}

	}

	public void leggiTestoDaFileModoComplicato() {

		try {

			// Non otteniamo granch�, se non la stampa del numero di byte letti
			// nel buffer:

			/*
			 * FileInputStream fileInputStream = new
			 * FileInputStream("dati.txt"); int numByteLetti =
			 * fileInputStream.read(); System.out.println("Byte letti: " +
			 * numByteLetti); fileInputStream.close();
			 */

			// Leggo da file:
			FileInputStream fileInputStreamDue = new FileInputStream("dati.txt");
			BufferedInputStream bis = new BufferedInputStream(fileInputStreamDue);
			DataInputStream dis = new DataInputStream(bis);

			String line = dis.readLine();
			while (line != null) {
				System.out.println(line);
				line = dis.readLine();
			}

			fileInputStreamDue.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	// Per poter salvare l'oggetto su file, quest'ultimo deve implementare
	// l'interfaccia Serializable, altrimenti d� errore
	public void salvaECaricaOggettoSuFile() {

		try {

			FileOutputStream fileOutputStream = new FileOutputStream("oggetto_salvato.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);

			Persona persona = new Persona();
			persona.setNome("Mario");
			persona.setCognome("Rossi");
			persona.setEta(28);
			persona.setSesso('M');

			out.writeObject(persona);

			System.out.println("Salvataggio effettuato correttamente");

			// Per esercizio, si provi a cambiare il serialVersionUID di Persona
			// e ad effettuare solo un caricamento, dopo averlo salvato con un
			// altro
			// serialVersionUID

			try {

				System.out.println("Recupero l'oggetto da file....");

				Thread.sleep(1000);

			} catch (InterruptedException e) {

				e.printStackTrace();

			}

			FileInputStream fis = new FileInputStream("oggetto_salvato.txt");
			ObjectInputStream in = new ObjectInputStream(fis);
			Persona personaRecuperata = (Persona) in.readObject();
			System.out.println(personaRecuperata);

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		} catch (ClassNotFoundException e) {

			e.printStackTrace();

		}

	}

	private void leggiListaOggettiDaFile() {

		Scanner input = null;
		List<Persona> listaPersone = new ArrayList<Persona>();

		try {

			input = new Scanner(new File("lista.txt"));
			input.useDelimiter("-|\n");

			while (input.hasNext()) {
				String nome = input.next();
				String cognome = input.next();
				int eta = input.nextInt();
				char sesso = input.next().charAt(0);

				Persona persona = new Persona();
				persona.setNome(nome);
				persona.setCognome(cognome);
				persona.setEta(eta);
				persona.setSesso(sesso);

				listaPersone.add(persona);

			}

			for (int i = 0; i < listaPersone.size(); i++) {

				System.out.println(listaPersone.get(i));

			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} finally {

			input.close();

		}

	}

}
