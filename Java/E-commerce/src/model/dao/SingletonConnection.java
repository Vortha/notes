package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

import javax.activation.DataSource;

public class SingletonConnection {

	private static Connection conn;
	
	private SingletonConnection() throws Exception{
		
		ResourceBundle rb = ResourceBundle.getBundle("model.dao.datasource");
		String uC = rb.getString("urlConnection");
		String uN = rb.getString("username");
		String pW = rb.getString("password");
		
		conn = DriverManager.getConnection(uC, uN, pW);
				
	}
	
	public static Connection getConnection() throws Exception{
		
		if(conn == null){
			new SingletonConnection();
		}
		
		return conn;
	}
	
}
