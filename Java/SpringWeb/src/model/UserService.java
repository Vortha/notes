package model;

import java.util.List;

import model.bean.Utente;

public interface UserService {

	/**
	 * Registra un utente al sistema.
	 * 
	 * @param u
	 *            - l'utente da registrare
	 */
	public void registraUtente(Utente u);

	/**
	 * Ottiene l'utente identificato dal parametro id.
	 * 
	 * @param id
	 *            - identificativo dell'utente da richiedere
	 * @return
	 */
	public Utente getSingoloUtente(int id);

	/**
	 * Cancella un utente dal sistema.
	 * 
	 * @param id
	 *            - identificativo dell'utente da cancellare
	 */
	public void cancellaUtente(int id);

	/**
	 * Modifica i dati di un utente. L'utente passato come parametro deve essere
	 * l'utente da modificare con i parametri modificati per quelli che si
	 * vogliono modificare, mentre l'id deve rimanere uguale.
	 * 
	 * @param u
	 *            - l'utente con i nuovi dati
	 */
	public void modificaDatiUtente(Utente u);

	/**
	 * Restituisce tutti gli utenti presenti sul sistema.
	 * 
	 * @return la lista degli utenti del sistema
	 */
	public List<Utente> getTuttiUtenti();
}
