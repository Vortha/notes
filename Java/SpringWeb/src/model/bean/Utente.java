package model.bean;

public class Utente {

	/** Identificativo dell'utente */
	private int id;
	/** Nome dell'utente */
	private String nome;
	/** Cognome dell'utente */
	private String cognome;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

}
