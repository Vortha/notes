package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import model.bean.Utente;

@Service
public class UserServiceFake implements UserService {

	private static Map<Integer, Utente> tabella = new HashMap<>();

	@Override
	public void registraUtente(Utente u) {
		if (!tabella.containsKey(u.getId())) {
			tabella.put(u.getId(), u);
		}

	}

	@Override
	public Utente getSingoloUtente(int id) {

		return tabella.get(id);
	}

	@Override
	public void cancellaUtente(int id) {
		tabella.remove(id);

	}

	@Override
	public void modificaDatiUtente(Utente u) {
		if (tabella.containsKey(u.getId())) {
			tabella.put(u.getId(), u);
		}
	}

	@Override
	public List<Utente> getTuttiUtenti() {

		Collection<Utente> c = tabella.values();

		return new ArrayList<>(c);
	}

}
