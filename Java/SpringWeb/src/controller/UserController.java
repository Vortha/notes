package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import model.UserService;
import model.bean.Utente;

@Controller
public class UserController {

	/*
	 * (Dependency Injection) - Con l'annotation @Autowired voglio dire a spring
	 * di risolvere da solo il setting di una determinata propriet�: se non ci
	 * riesce restituisce un errore. Se c'� una sola classe ad implementare
	 * l'interfaccia UserService, istanzia un oggetto di quella classe,
	 * altrimenti va in eccezione. Quest'annotation funziona solo per classi
	 * annotate. Tre stereotipi: Controller, Service e Repository. Se una classe
	 * non appartiene a nessuno di questi tre, allora appartiene a Component.
	 */
	@Autowired
	private UserService us;

	@RequestMapping(value = "/registrazione", method = RequestMethod.POST)
	public ModelAndView registraUtente(Utente u) {

		us.registraUtente(u);
		List<Utente> utenti = us.getTuttiUtenti();

		// return new ModelAndView("registrazioneOK", "user", u);

		ModelAndView mv = new ModelAndView("users");
		mv.addObject("user", u);
		mv.addObject("users", utenti);

		return mv;
	}

	@RequestMapping(value = "/modifica")
	public ModelAndView modificaUtente(Utente u) {

		us.modificaDatiUtente(u);
		List<Utente> utenti = us.getTuttiUtenti();

		ModelAndView mv = new ModelAndView("users");
		mv.addObject("user", u);
		mv.addObject("users", utenti);

		return mv;
	}

	@RequestMapping(value = "/cancella/{id}", method = RequestMethod.POST)
	public ModelAndView cancellaUtente(@PathVariable("id") int id) {

		us.cancellaUtente(id);
		List<Utente> utenti = us.getTuttiUtenti();

		ModelAndView mv = new ModelAndView("users");
		mv.addObject("users", utenti);

		return mv;
	}

}
