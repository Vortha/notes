package controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import model.MathService;
import model.MathServiceImpl;

@Controller
public class MathController {

	// Senza dati da passare
	// public String somma(double n1, double n2) {
	// MathService ms = new MathServiceImpl();
	// double ris = ms.somma(n1, n2);
	//
	// return "risSomma";
	// }

	// Con i dati da passare
	@RequestMapping("/Somma")
	public ModelAndView somma(double n1, double n2) {
		MathService ms = new MathServiceImpl();
		double ris = ms.somma(n1, n2);

		return new ModelAndView("risSomma", "somma", ris);
	}

	@RequestMapping("/Sommatoria")
	public ModelAndView sommatoria(double n1, HttpSession session) {
		double somma = n1;
		Double n = (Double) session.getAttribute("n");
		if (n == null) {
			session.setMaxInactiveInterval(10);
			session.setAttribute("n", n1);
		} else {
			somma = n + n1;
			session.setAttribute("n", n + n1);
		}
		
		session.setAttribute("prova", 12);

		return new ModelAndView("risSommatoria", "somma", somma);

	}
}
