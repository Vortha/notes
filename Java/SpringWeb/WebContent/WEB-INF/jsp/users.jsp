<%@page import="java.util.List"%>
<%@page import="model.bean.Utente"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Utenti</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</head>
<body>

	<div class="container">
		<h2>Utente registrato con successo!</h2>

		ID: ${user.id} <br> Nome: ${user.nome} <br> Cognome:
		${user.cognome}

		<hr>
		<h2>Utenti registrati ad oggi</h2>

		<table class="table table-condensed">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nome</th>
					<th>Cognome</th>
					<th>Modifica</th>
					<th>Cancella</th>
				</tr>
			</thead>
			<tbody>
				<%
					List<Utente> utenti = (List<Utente>) request.getAttribute("users");
					for (Utente u : utenti) {
				%>
				<!-- out.println("<form method='post'>");
						out.println("<tr>");
						out.println("<td><input type='text' value='" + u.getId() + "' name='id' readonly></td>");
						out.println("<td><input type='text' value='" + u.getNome() + "' name='nome'></td>");
						out.println("<td><input type='text' value='" + u.getCognome() + "' name='cognome'></td>");
						out.println("<td><input type='submit' class='btn btn-primary' name='example/modifica' value='Modifica'></td>");
						out.println("<td><input type='submit' class='btn btn-danger' name='example/cancella' value='Cancella'></td>");
						out.println("</tr>");
						out.println("</form>"); -->
				<tr>
					<td><input type="text" class="form-control"
						value="<%=u.getId()%>" name="id" readonly></td>
					<td><input type="text" class="form-control"
						value="<%=u.getNome()%>" name="nome"></td>
					<td><input type="text" class="form-control"
						value="<%=u.getCognome()%>" name="cognome"></td>
					<td><form method="post">
							<input type="submit" class="btn btn-primary" name="modifica"
								value="Modifica">
						</form></td>
					<td><form action="cancella/<%u.getId();%>" method="POST">
							<input type="submit" class="btn btn-danger" value="Cancella">
						</form></td>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>

</body>
</html>