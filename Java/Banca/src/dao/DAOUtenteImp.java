package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import bean.Utente;

public class DAOUtenteImp implements DAOUtente {

	@Override
	public void insert(Utente utente) throws Exception {

		try (Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle", "scott",
				"tiger");) {
			PreparedStatement ps = connection.prepareStatement("insert into utente values (?,?,?)");

			ps.setInt(1, utente.getId());
			ps.setString(2, utente.getNome());
			ps.setString(3, utente.getCognome());

			ps.executeUpdate();

		}

	}

	@Override
	public void update(Utente utente) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		PreparedStatement ps = connection.prepareStatement("update utente set nome = ?, cognome = ? where id = ?");

		ps.setString(1, utente.getNome());
		ps.setString(2, utente.getCognome());
		ps.setInt(3, utente.getId());

		ps.executeUpdate();

	}

	@Override
	public void delete(int idUtente) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		PreparedStatement ps = connection.prepareStatement("delete from utente where idUtente = ?");

		ps.setInt(1, idUtente);

		ps.executeUpdate();

	}

	@Override
	public Utente select(int idUtente) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from emp where empno = ?");

		ps.setInt(1, idUtente);

		ResultSet rs = ps.executeQuery();

		Utente utente = new Utente();

		if (rs.next()) {
			utente.setId(rs.getInt("id"));
			utente.setNome(rs.getString("nome"));
			utente.setCognome(rs.getString("cognome"));

		}
		return utente;

	}
}
