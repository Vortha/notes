package dao;

import java.util.List;

import bean.ContoCorrente;
import bean.Movimentazioni;

public interface DAOConti {

	/**
	 * Insert a new ContoCorrente to the db.
	 * @param contoCorrente - the ContoCorrente to insert
	 * @throws Exception
	 */
	void insert(ContoCorrente contoCorrente) throws Exception;
	
	/**
	 * Update a ContoCorrente
	 * @param contoCorrente - the updated ContoCorrente
	 * @throws Exception
	 */
	void update(ContoCorrente contoCorrente) throws Exception;
	
	/**
	 * Delete a ContoCorrente.
	 * @param numero - the ContoCorrente identifier to delete
	 * @throws Exception
	 */
	void delete(int numero) throws Exception;
	
	/**
	 * Select a ContoCorrente.
	 * @param numero - the ContoCorrente identifier to select
	 * @return the ContoCorrente identified by numero
	 * @throws Exception
	 */
	ContoCorrente select(int numero) throws Exception;
	
	/**
	 * Withdraw money.
	 * @param numeroConto
	 * @param importo
	 * @param canale
	 * @throws Exception
	 */
	void withdraw(int numeroConto, double importo, String canale) throws Exception;

	/**
	 * Pour money.
	 * @param numeroConto
	 * @param importo
	 * @throws Exception
	 */
	void pour(int numeroConto, double importo) throws Exception;
	
	/**
	 * Given the ContoCorrente identifier, return the list of Movimentazioni.
	 * @param numConto - the ContoCorrente identifier
	 * @return the list of Movimentazioni
	 */
	List<Movimentazioni> getMovimentazioni(int numConto) throws Exception;
}
