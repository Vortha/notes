package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.ContoCorrente;
import bean.Movimentazioni;

public class DAOContiImpl implements DAOConti {

	@Override
	public void insert(ContoCorrente contoCorrente) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "insert into conto_corrente values(?, ?, ?, ?)";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setInt(1, contoCorrente.getNumero());
		ps.setDouble(2, contoCorrente.getSaldo());
		ps.setDate(3, contoCorrente.getDataApertura());
		ps.setInt(4, contoCorrente.getIdUtente());

		ps.executeUpdate();
	}

	@Override
	public void update(ContoCorrente contoCorrente) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "update conto_corrente set saldo = ?, data_apertura = ?, id_utente = ? where numero = ?";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setDouble(1, contoCorrente.getSaldo());
		ps.setDate(2, contoCorrente.getDataApertura());
		ps.setInt(3, contoCorrente.getIdUtente());
		ps.setInt(4, contoCorrente.getNumero());

		ps.executeUpdate();
	}

	@Override
	public void delete(int numero) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "delete from conto_corrente where numero = ?";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setInt(1, numero);

		ps.executeUpdate();
	}

	@Override
	public ContoCorrente select(int numero) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "select * from conto_corrente where numero = ?";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setInt(1, numero);

		ResultSet resultSet = ps.executeQuery();

		ContoCorrente contoCorrente = null;

		if (resultSet.next()) {
			contoCorrente = new ContoCorrente();

			double saldo = resultSet.getDouble("saldo");
			Date dataApertura = resultSet.getDate("data_apertura");
			int idUtente = resultSet.getInt("id_utente");

			contoCorrente.setNumero(numero);
			contoCorrente.setSaldo(saldo);
			contoCorrente.setDataApertura(dataApertura);
			contoCorrente.setIdUtente(idUtente);
		}

		return contoCorrente;
	}

	@Override
	public void withdraw(int numeroConto, double importo, String canale) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "insert into movimentazione values(null, ?, ?, ?, ?, ?)";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setString(1, "P");
		ps.setDouble(2, importo);

		Date date = new Date(System.currentTimeMillis());
		ps.setDate(3, date);
		ps.setString(4, canale);
		ps.setInt(5, numeroConto);

		ps.executeUpdate();
	}

	@Override
	public void pour(int numeroConto, double importo) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "insert into movimentazione values(null, ?, ?, ?, ?, ?)";

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setString(1, "V");
		ps.setDouble(2, importo);

		Date date = new Date(System.currentTimeMillis());

		ps.setDate(3, date);
		ps.setString(4, "SPORTELLO");
		ps.setInt(5, numeroConto);

		ps.executeUpdate();

	}

	@Override
	public List<Movimentazioni> getMovimentazioni(int numConto) throws SQLException {
		
		Connection connection = SingletonConnection.getConnection();
		
		String sql = "select * from movimentazione where numero_conto = ?";
		
		PreparedStatement ps = connection.prepareStatement(sql);
		
		ps.setInt(1, numConto);
		
		ResultSet resultSet = ps.executeQuery();
		
		List<Movimentazioni> list = new ArrayList<>();
		
		while (resultSet.next()) {
			Movimentazioni movimentazioni = new Movimentazioni();
			int id = resultSet.getInt("ID");
			String tipo = resultSet.getString("TIPO");
			double importo = resultSet.getDouble("IMPORTO");
			Date dataOperazione = resultSet.getDate("DATA_OPERAZIONE");
			String canale = resultSet.getString("CANALE");
			
			movimentazioni.setId(id);
			movimentazioni.setTipo(tipo);
			movimentazioni.setImporto(importo);
			movimentazioni.setDataOperazione(dataOperazione);
			movimentazioni.setCanale(canale);
			movimentazioni.setNumeroConto(numConto);
			
			list.add(movimentazioni);
		}
		
		return list;
	}

}
