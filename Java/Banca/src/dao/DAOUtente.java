package dao;

import bean.Utente;

public interface DAOUtente {

	/**
	 * Insert a new user into db.
	 * 
	 * @param utente
	 *            - the new user to insert
	 * @throws Exception
	 */
	void insert(Utente utente) throws Exception;
	
	/**
	 * Update a user.
	 * @param utente - the updated user
	 * @throws Exception
	 */
	void update(Utente utente) throws Exception;

	/**
	 * Delete a user from the db.
	 * @param idUtente - the user identifier to delete
	 * @throws Exception
	 */
	void delete(int idUtente) throws Exception;

	/**
	 * Select a user from the db.
	 * @param idUtente - the user identifier to select
	 * @return the user identified by idUtente
	 * @throws Exception
	 */
	Utente select(int idUtente) throws Exception;
}
