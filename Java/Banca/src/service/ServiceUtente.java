package service;

import bean.Utente;

public interface ServiceUtente {

	public void registraUtente(Utente u);

	public void modificaDatiUtente(Utente u);

	void cancellaUtente(int idUtente);

	public Utente leggiUtente(int id);

}
