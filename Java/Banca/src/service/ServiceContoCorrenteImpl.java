package service;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import bean.ContoCorrente;
import bean.Movimentazioni;
import dao.DAOContiImpl;
import service.eccezione.ImportoException;

public class ServiceContoCorrenteImpl implements ServiceContoCorrente {

	@Override
	public int apriNuovoConto(int id, double importo) {

		DAOContiImpl dao = new DAOContiImpl();

		ContoCorrente conto1 = new ContoCorrente();
		conto1.setIdUtente(id);
		conto1.setSaldo(importo);

		Date date = new Date(System.currentTimeMillis());
		conto1.setDataApertura(date);

		int randomNum = ThreadLocalRandom.current().nextInt(1, 4000 + 1);
		conto1.setNumero(randomNum);

		try {
			dao.insert(conto1);
			return randomNum;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return -1;
	}

	@Override
	public void preleva(int numConto, double importo, String tipo) {

		DAOContiImpl dao = new DAOContiImpl();
		try {
			ContoCorrente conto1 = dao.select(numConto);

			if (importo > conto1.getSaldo()) {
				throw new ImportoException("l'importo deve essere minore o uguale del saldo");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void versa(int numConto, double importo) {

		DAOContiImpl dao = new DAOContiImpl();
		try {
			dao.pour(numConto, importo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Boolean check(int cumConto) {

		DAOContiImpl dao = new DAOContiImpl();

		try {
			ContoCorrente contoCorrente = dao.select(cumConto);

			double saldo = contoCorrente.getSaldo();
			List<Movimentazioni> list = dao.getMovimentazioni(cumConto);

			double saldoFinale = 0;

			for (Movimentazioni movimentazioni : list) {
				double importo = movimentazioni.getImporto();
				if (movimentazioni.getTipo().equals("P")) {
					saldoFinale -= importo;
				} else {
					saldoFinale += importo;
				}
			}

			if (saldoFinale == saldo) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

}
