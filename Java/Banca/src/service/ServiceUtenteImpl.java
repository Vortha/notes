package service;

import bean.Utente;
import dao.DAOUtenteImp;

public class ServiceUtenteImpl implements ServiceUtente {

	@Override
	public void registraUtente(Utente u) {

		DAOUtenteImp dao = new DAOUtenteImp();

		try {
			dao.insert(u);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void modificaDatiUtente(Utente u) {

		DAOUtenteImp dao = new DAOUtenteImp();

		try {
			dao.update(u);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void cancellaUtente(int idUtente) {

		DAOUtenteImp dao = new DAOUtenteImp();

		try {
			dao.delete(idUtente);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public Utente leggiUtente(int id) {

		DAOUtenteImp dao = new DAOUtenteImp();
		Utente utente = null;

		try {
			utente = dao.select(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return utente;
	}

}
