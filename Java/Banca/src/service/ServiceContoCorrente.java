package service;

public interface ServiceContoCorrente {
	
	public int apriNuovoConto(int id, double importo);
	
	public void preleva(int numConto, double importo, String tipo);
	
	public void versa(int numConto, double importo);
	
	public Boolean check(int cumConto);

}
