package bean;

import java.util.Date;

public class Movimentazioni {
	
	private int id;
	private String tipo;
	private double importo;
	private Date dataOperazione;
	private int numeroConto;
	private String canale;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public double getImporto() {
		return importo;
	}
	public void setImporto(double importo) {
		this.importo = importo;
	}
	public Date getDataOperazione() {
		return dataOperazione;
	}
	public void setDataOperazione(Date dataOperazione) {
		this.dataOperazione = dataOperazione;
	}
	public int getNumeroConto() {
		return numeroConto;
	}
	public void setNumeroConto(int numeroConto) {
		this.numeroConto = numeroConto;
	}
	public String getCanale() {
		return canale;
	}
	public void setCanale(String canale) {
		this.canale = canale;
	}
	@Override
	public String toString() {
		return "Movimentazioni [id=" + id + ", tipo=" + tipo + ", importo=" + importo + ", dataOperazione="
				+ dataOperazione + ", numeroConto=" + numeroConto + ", canale=" + canale + "]";
	}
	
	
	
	
	
}
