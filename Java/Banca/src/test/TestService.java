package test;

import bean.Utente;
import service.ServiceContoCorrenteImpl;
import service.ServiceUtenteImpl;

public class TestService {

	public static void main(String[] args) {
		
		ServiceContoCorrenteImpl service = new ServiceContoCorrenteImpl();
		
//		System.out.println(service.apriNuovoConto(3, 1));
		
//		System.out.println(service.check(1));
		
		ServiceUtenteImpl service2 = new ServiceUtenteImpl();
		
		Utente u = new Utente();
		
		u.setId(21);
		u.setNome("Giovanni");
		u.setCognome("Pompili");
//		
//		service2.registraUtente(u);
		
		service2.modificaDatiUtente(u);
	}

}
