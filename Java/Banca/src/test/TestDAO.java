package test;

import java.sql.Date;

import bean.ContoCorrente;
import dao.DAOContiImpl;

public class TestDAO {

	public static void main(String[] args) {

		try {
			DAOContiImpl dao = new DAOContiImpl();

			ContoCorrente contoCorrente = new ContoCorrente();
			
			contoCorrente.setIdUtente(3);
			contoCorrente.setNumero(1);
			contoCorrente.setSaldo(1002000);
			Date dataApertura = new Date(117, 06, 04);
			contoCorrente.setDataApertura(dataApertura);
			
//			dao.insert(contoCorrente);
//			dao.update(contoCorrente);
//			System.out.println(dao.select(1));
			
//			dao.withdraw(1, 400, "WEB");
			dao.pour(1, 200);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
