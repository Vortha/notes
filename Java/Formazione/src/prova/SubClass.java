package prova;

public class SubClass extends SuperClass {

	@Override
	public void prova0() {
		System.out.println("SubClass.prova0()");
	}
	
	public void prova1() {
		System.out.println("SubClass.prova1()");
	}
	
	public void prova2() {
		System.out.println("SubClass.prova2()");
	}
	
	@Override
	public String toString() {
		return "subClass.toString()";
	}
}
