package it.ats.formazione.interfacciacomparable.control;

import java.util.ArrayList;
import java.util.Collections;

import it.ats.formazione.interfacciacomparable.model.Persona;

public class Principale {

	public static void main(String[] args) {
		Principale principale = new Principale();
		principale.esegui();
	}

	public void esegui() {

		ArrayList<Persona> persone = new ArrayList<>();
		persone.add(new Persona("Valerio", 24));
		persone.add(new Persona("Natalina", 23));
		persone.add(new Persona("Giordano", 26));
		persone.add(new Persona("Valerio", 25));

		Persona persona = new Persona("Valerio", 12);
		try {
			Persona personaClonata = (Persona) persona.clone();
			System.out.println(personaClonata);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(persone.get(0).compareTo(persone.get(2)));

		Collections.sort(persone);

		System.out.println(persone);
	}
}
