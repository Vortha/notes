package it.ats.formazione.interfacciacomparable.model;


public class Persona implements Comparable<Persona>, Cloneable {

	private String nome;
	private int eta;

	public Persona(String nome, int eta) {
		super();
		this.nome = nome;
		this.eta = eta;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		
		Persona persona = (Persona) super.clone();
		
		return persona;
	}

	@Override
	public int compareTo(Persona o) {
		int res = this.nome.compareTo(o.nome);

		if (res == 0) {
			res = this.eta - o.eta;
		}

		return res;
	}

	@Override
	public String toString() {
		return "Persona [nome=" + nome + ", eta=" + eta + "]";
	}

}
