package it.ats.formazione.interfacciacomparable.utilita;

import java.lang.reflect.Field;
import java.util.Comparator;

public class ComparatoreEta<T> implements Comparator<T> {

	@Override
	public int compare(T arg0, T arg1) {
		Field[] fieldsArg0 = arg0.getClass().getDeclaredFields();
		Field[] fieldsArg1 = arg1.getClass().getDeclaredFields();

		boolean ret = false;

		for (int i = 0; i < fieldsArg1.length; i++) {
			ret = fieldsArg1[i].equals(fieldsArg0[i]);

			try {
				Class classe = Class.forName(fieldsArg1[i].getType().getSimpleName());

				Class class2 = Class.forName(fieldsArg0[i].getType().getSimpleName());
				
				classe.getMethod("compareTo", class2);
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return 0;
	}

}
