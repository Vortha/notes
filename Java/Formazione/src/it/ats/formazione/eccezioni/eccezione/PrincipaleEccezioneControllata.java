package it.ats.formazione.eccezioni.eccezione;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class PrincipaleEccezioneControllata {

	public void eccezioneGestita() {

		FileReader reader;
		BufferedReader in;

		try {
			reader = new FileReader("inputs.txt");
			in = new BufferedReader(reader);
			String sCurrentLine;

			while ((sCurrentLine = in.readLine()) != null) {
				System.out.println(sCurrentLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
