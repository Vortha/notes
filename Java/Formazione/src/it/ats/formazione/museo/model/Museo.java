package it.ats.formazione.museo.model;

public class Museo {
	private String nome;
	private String citta;
	private String tipo;
	private Quadro[] quadri;

	private Opera[] opere;

	// Predispongo lo spazio per i quadri senza doverli assegnare sul momento
	// per rispettare la relazione di Aggregazione
	public Museo(String nome, String citta, String tipo, int dimMuseo) {
		super();
		this.nome = nome;
		this.citta = citta;
		this.tipo = tipo;
		this.quadri = new Quadro[dimMuseo]; // Aggregazione
		this.opere = new Opera[dimMuseo];

		Autore autore = new Autore("Giordano", "Colozzi");
		Autore autore2 = new Autore("Valerio", "Pompili");
		
		Quadro quadro = new Quadro("Gioconda", 50, 40, autore, "olio");
		Statua statua = new Statua("Prova", 20, 30, autore2, "Stucco");
		
		this.opere[0] = quadro;
		this.opere[2] = statua;
	}

	public void caricaQuadro(Quadro quadro) {
		for (int i = 0; i < quadri.length; i++) {
			if (quadri[i] == null) {
				quadri[i] = quadro;
				break;
			}
		}
	}

	public void caricaOpera(Opera... opere) {
		int i = 0;
		int arrayLength = this.opere.length;

		for (Opera opera : opere) {
			for (; i < arrayLength; i++) {
				if (this.opere[i] == null) {
					this.opere[i] = opera;
					i++;
					break;
				}
			}
		}
	}

	public void stampaQuadri() {
		boolean nulli = true;
		for (Quadro quadro : quadri) {
			if (quadro != null) {
				nulli = false;
				System.out.println(quadro);
			}
		}

		if (nulli) {
			System.out.println("Il museo non contiene quadri");
		}
	}

	public void stampaOpere() {
		boolean nulli = true;
		for (Opera opera : opere) {
			if (opera != null) {
				nulli = false;
				System.out.println(opera);
			}
		}

		if (nulli) {
			System.out.println("Il museo non contiene opere");
		}
	}

	@Override
	public String toString() {
		return "Museo [nome=" + nome + ", citta=" + citta + ", tipo=" + tipo + "]";
	}
}
