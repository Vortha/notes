package it.ats.formazione.museo.model;

public class Statua extends Opera {

	private String materiale;

	public Statua(String titolo, int altezza, int larghezza, Autore autore, String materiale) {
		super(titolo, altezza, larghezza, autore);
		this.materiale = materiale;
	}

	public String getMateriale() {
		return materiale;
	}

	public void setMateriale(String materiale) {
		this.materiale = materiale;
	}

	@Override
	public String toString() {
		return "Statua [materiale=" + materiale + ", toString()=" + super.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((materiale == null) ? 0 : materiale.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statua other = (Statua) obj;
		if (materiale == null) {
			if (other.materiale != null)
				return false;
		} else if (!materiale.equals(other.materiale))
			return false;
		return true;
	}

}
