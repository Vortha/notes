package it.ats.formazione.museo.model;

public abstract class Opera {

	private String titolo;
	private int altezza;
	private int larghezza;
	private Autore autore;

	public Opera(String titolo, int altezza, int larghezza, Autore autore) {
		super();
		this.titolo = titolo;
		this.altezza = altezza;
		this.larghezza = larghezza;
		this.autore = autore;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getAltezza() {
		return altezza;
	}

	public void setAltezza(int altezza) {
		this.altezza = altezza;
	}

	public int getLarghezza() {
		return larghezza;
	}

	public void setLarghezza(int larghezza) {
		this.larghezza = larghezza;
	}

	public Autore getAutore() {
		return autore;
	}

	public void setAutore(Autore autore) {
		this.autore = autore;
	}

	@Override
	public String toString() {
		return "Opera [titolo=" + titolo + ", altezza=" + altezza + ", larghezza=" + larghezza + ", autore=" + autore
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + altezza;
		result = prime * result + ((autore == null) ? 0 : autore.hashCode());
		result = prime * result + larghezza;
		result = prime * result + ((titolo == null) ? 0 : titolo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Opera other = (Opera) obj;
		if (altezza != other.altezza)
			return false;
		if (autore == null) {
			if (other.autore != null)
				return false;
		} else if (!autore.equals(other.autore))
			return false;
		if (larghezza != other.larghezza)
			return false;
		if (titolo == null) {
			if (other.titolo != null)
				return false;
		} else if (!titolo.equals(other.titolo))
			return false;
		return true;
	}

}
