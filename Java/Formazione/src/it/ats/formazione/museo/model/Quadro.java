package it.ats.formazione.museo.model;

public class Quadro extends Opera {

	private String tipologia;

	public Quadro(String titolo, int altezza, int larghezza, Autore autore, String tipoColore) {
		super(titolo, altezza, larghezza, autore);
		this.tipologia = tipoColore;
	}

	@Override
	public String toString() {
		return "Quadro [tipologia=" + tipologia + ", toString()=" + super.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tipologia == null) ? 0 : tipologia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quadro other = (Quadro) obj;
		if (tipologia == null) {
			if (other.tipologia != null)
				return false;
		} else if (!tipologia.equals(other.tipologia))
			return false;
		return true;
	}

	// private String titolo;
	// private int altezza;
	// private int larghezza;
	// private Autore autore = new Autore(); // Associazione (utile per quando
	// verr� creato il
	// database (ORM - Object
	// Relationship Mapping))
	//
	// Ho bisogno per forza di un Autore per la relazione di Composizione
	// public Quadro(String titolo, int altezza, int larghezza, Autore autore) {
	// super();
	// this.titolo = titolo;
	// this.altezza = altezza;
	// this.larghezza = larghezza;
	// this.autore = autore; // Composizione
	// }
	//
	// public String getTitolo() {
	// return titolo;
	// }
	//
	// public void setTitolo(String titolo) {
	// this.titolo = titolo;
	// }
	//
	// public int getAltezza() {
	// return altezza;
	// }
	//
	// public void setAltezza(int altezza) {
	// this.altezza = altezza;
	// }
	//
	// public int getLarghezza() {
	// return larghezza;
	// }
	//
	// public void setLarghezza(int larghezza) {
	// this.larghezza = larghezza;
	// }
	//
	// public Autore getAutore() {
	// return autore;
	// }
	//
	// public void setAutore(Autore autore) {
	// this.autore = autore;
	// }
	//
	// @Override
	// public String toString() {
	// return "Quadro [titolo=" + titolo + ", altezza=" + altezza + ",
	// larghezza=" + larghezza + ", autore=" + autore
	// + "]";
	// }

}
