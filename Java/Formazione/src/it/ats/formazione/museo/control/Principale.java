package it.ats.formazione.museo.control;

import java.util.HashSet;

import it.ats.formazione.museo.model.Autore;
import it.ats.formazione.museo.model.Museo;
import it.ats.formazione.museo.model.Opera;
import it.ats.formazione.museo.model.Quadro;
import it.ats.formazione.museo.model.Statua;

public class Principale {

	public static void main(String[] args) {
		Principale principale = new Principale();
		principale.creaMuseo();
	}

	public void creaMuseo() {
		Museo museo = new Museo("Louvre", "Parigi", "Museo misto", 5);
		System.out.println(museo);

		HashSet<Opera> opere = new HashSet<>();

		Autore autore = new Autore("Giordano", "Colozzi");
		Autore autore2 = new Autore("Valerio", "Pompili");

		Quadro quadro = new Quadro("Gioconda", 50, 40, autore, "olio");
		Quadro quadro2 = new Quadro("Gioconda", 50, 40, autore, "olio");
		Statua statua = new Statua("Prova", 20, 30, autore2, "Stucco");

		opere.add(quadro);
		opere.add(statua);
		opere.add(quadro2);
		
		System.out.println(opere);

		// museo.caricaOpera(statua, quadro, quadro, quadro);

		// Quadro quadro = new Quadro("Gioconda", 50, 40, autore);
		// Quadro quadro2 = new Quadro("Prova", 20, 30, autore2);
		// museo.caricaQuadro(quadro);
		// museo.caricaQuadro(quadro2);

		// museo.stampaOpere();
	}
}
