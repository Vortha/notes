package it.ats.formazione.array.model;

public class Prova {
	private int numero;
	
	public Prova(int numero) {
		this.numero = numero;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
}
