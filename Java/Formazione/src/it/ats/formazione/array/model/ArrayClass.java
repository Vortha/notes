package it.ats.formazione.array.model;

import java.util.Arrays;

public class ArrayClass extends Prova {

	public ArrayClass(int numero) {
		super(numero);
	}

	public static void main(String[] args) {
		int[] array = {1, 2, 3, 4};
		
		System.out.println(Arrays.toString(array));
		
		ArrayClass arrayClass = new ArrayClass(10);
		System.out.println(arrayClass.getNumero());
	}
}