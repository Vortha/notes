package it.ats.formazione.ereditariet�epolimorfismo.azienda.model;

public class Impiegato {

	private int matricola;
	private String nome;
	private String cognome;

	public Impiegato(int matricola, String nome, String cognome) {
		super();
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
	}
	
	public String cosaSoFare() {
		return "So timbrare il cartellino";
	}

	public int getMatricola() {
		return matricola;
	}

	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	@Override
	public String toString() {
		return "Impiegato [matricola=" + matricola + ", nome=" + nome + ", cognome=" + cognome + "]";
	}

}
