package it.ats.formazione.ereditariet�epolimorfismo.azienda.model;

public class Manager extends Impiegato {

	private String nomeSegreteria;

	public Manager(int matricola, String nome, String cognome, String nomeSegreteria) {
		super(matricola, nome, cognome);
		this.nomeSegreteria = nomeSegreteria;
	}
	
	@Override
	public String cosaSoFare() {
		return "So timbrare il cartellino e dirigere un'azienda";
	}
	
	public String licenziaTutti() {
		return "Sto licenziando tutti";
	}

	@Override
	public String toString() {
		return "Manager [nomeSegreteria=" + nomeSegreteria + ", matricola=" + super.getMatricola() + ", nome="
				+ super.getNome() + ", cognome=" + super.getCognome() + "]";
	}

}
