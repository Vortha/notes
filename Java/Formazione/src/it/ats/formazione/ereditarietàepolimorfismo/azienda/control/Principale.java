package it.ats.formazione.ereditariet�epolimorfismo.azienda.control;

import java.util.ArrayList;

import it.ats.formazione.ereditariet�epolimorfismo.azienda.model.Impiegato;
import it.ats.formazione.ereditariet�epolimorfismo.azienda.model.Manager;

public class Principale {

	public static void main(String[] args) {
		Principale principale = new Principale();
		principale.esegui();

	}
	
	public void esegui() {
		
		ArrayList<Impiegato> impiegati = new ArrayList<Impiegato>();
		Impiegato impiegato = new Impiegato(12821, "Nicola", "Verdi");
		Manager manager = new Manager(4231, "Carlo", "Bianchi", "Rossella");
	
		impiegati.add(impiegato);
		impiegati.add(manager);
		
		manager.licenziaTutti();
		impiegato = manager;
		
		for (Impiegato imp : impiegati) {
			System.out.println(imp);
			System.out.println("\t" + imp.cosaSoFare());
			if (imp instanceof Manager) {
				System.out.println("\t\t" + ((Manager) imp).licenziaTutti());
			}
		}
	}

}
