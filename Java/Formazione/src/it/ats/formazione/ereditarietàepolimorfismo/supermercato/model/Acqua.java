package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model;

public class Acqua extends Bibita {

	private String produttore;

	public Acqua(String nome, double prezzo, double litri, String produttore) {
		super(nome, prezzo, litri);
		this.produttore = produttore;
	}

	public String getProduttore() {
		return produttore;
	}

	public void setProduttore(String produttore) {
		this.produttore = produttore;
	}

	@Override
	public String toString() {
		return "Acqua [produttore=" + produttore + ", toString()=" + super.toString() + "]";
	}

}
