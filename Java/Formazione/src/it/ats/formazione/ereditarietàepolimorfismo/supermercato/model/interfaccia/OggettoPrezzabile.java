package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.interfaccia;

public interface OggettoPrezzabile {

	/** Restituisce il prezzo dell'oggetto. */
	public double getPrezzo();

	/**
	 * Modifica il prezzo dell'oggetto.
	 * 
	 * @param x
	 */
	public void setPrezzo(double x);
}
