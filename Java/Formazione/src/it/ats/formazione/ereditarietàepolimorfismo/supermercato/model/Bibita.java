package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model;

import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.interfaccia.OggettoPrezzabile;

public class Bibita implements OggettoPrezzabile {

	private String nome;
	private double prezzo;
	private double litri;

	public Bibita(String nome, double prezzo, double litri) {
		super();
		this.nome = nome;
		this.prezzo = prezzo;
		this.litri = litri;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	public double getLitri() {
		return litri;
	}

	public void setLitri(double litri) {
		this.litri = litri;
	}

	@Override
	public String toString() {
		return "Bibita [nome=" + nome + ", prezzo=" + prezzo + ", litri=" + litri + "]";
	}

}
