package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model;

import java.util.ArrayList;

import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.interfaccia.OggettoPrezzabile;

public class Supermercato {

	private String nome;
	private ArrayList<OggettoPrezzabile> oggettiPrezzabili;

	public Supermercato(String nome) {
		super();
		this.nome = nome;
		this.oggettiPrezzabili = new ArrayList<>(); // Composizione
	}

	public double totalePrezzi() {
		double somma = 0;

		for (OggettoPrezzabile oggettoPrezzabile : oggettiPrezzabili) {
			somma += oggettoPrezzabile.getPrezzo();
		}

		return somma;
	}

	public void caricaOggettoPrezzabile(OggettoPrezzabile... op) {
		for (OggettoPrezzabile oggettoPrezzabile : op) {
			this.oggettiPrezzabili.add(oggettoPrezzabile);
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<OggettoPrezzabile> getOggettiPrezzabili() {
		return oggettiPrezzabili;
	}

	public void setOggettiPrezzabili(ArrayList<OggettoPrezzabile> oggettiPrezzabili) {
		this.oggettiPrezzabili = oggettiPrezzabili;
	}

	@Override
	public String toString() {
		return "Supermercato [nome=" + nome + "]";
	}

}
