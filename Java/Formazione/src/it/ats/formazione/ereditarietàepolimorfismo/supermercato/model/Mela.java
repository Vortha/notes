package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model;

public class Mela extends Frutto {

	private String produttore;
	private boolean peluria;

	public Mela(String nome, double prezzo, String produttore, boolean peluria) {
		super(nome, prezzo);
		this.produttore = produttore;
		this.peluria = peluria;
	}

	public String getProduttore() {
		return produttore;
	}

	public void setProduttore(String produttore) {
		this.produttore = produttore;
	}

	public boolean isPeluria() {
		return peluria;
	}

	public void setPeluria(boolean peluria) {
		this.peluria = peluria;
	}

	@Override
	public String toString() {
		return "Mela [produttore=" + produttore + ", peluria=" + peluria + ", toString()=" + super.toString() + "]";
	}

}
