package it.ats.formazione.ereditariet�epolimorfismo.supermercato.control;

import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.Acqua;
import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.Mela;
import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.Supermercato;

public class Principale {

	public static void main(String[] args) {

		Supermercato supermercato = new Supermercato("Conad");

		Mela mela = new Mela("Melinda", 4, "Melinda", false);
		Acqua acqua = new Acqua("Levissima", 3, 2, "Levissima inc.");
		
		supermercato.caricaOggettoPrezzabile(mela, acqua);
		
		System.out.println(supermercato.getOggettiPrezzabili());
		System.out.println(supermercato.totalePrezzi());
	}
}
