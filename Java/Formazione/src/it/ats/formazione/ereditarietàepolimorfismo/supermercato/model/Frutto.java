package it.ats.formazione.ereditariet�epolimorfismo.supermercato.model;

import it.ats.formazione.ereditariet�epolimorfismo.supermercato.model.interfaccia.OggettoPrezzabile;

public class Frutto implements OggettoPrezzabile {

	private String nome;
	private double prezzo;

	public Frutto(String nome, double prezzo) {
		super();
		this.nome = nome;
		this.prezzo = prezzo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return "Frutto [nome=" + nome + ", prezzo=" + prezzo + "]";
	}

}
