package it.ats.formazione.ereditariet�epolimorfismo.animali.model;

public class Orango extends Animale {

	private double grandezzaMani;

	public Orango(String nome, int eta, char sesso, double grandezzaMani) {
		super(nome, eta, sesso);
		this.grandezzaMani = grandezzaMani;
	}

	@Override
	public String cosaFaccio() {
		return "Mi arrampico sugli alberi";
	}

	@Override
	public String toString() {
		return "Orango [grandezzaMani=" + grandezzaMani + ", nome=" + nome + ", eta=" + eta + ", sesso=" + sesso + "]";
	}

}
