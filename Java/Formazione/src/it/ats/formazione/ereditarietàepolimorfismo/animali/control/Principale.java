package it.ats.formazione.ereditariet�epolimorfismo.animali.control;

import java.util.ArrayList;

import it.ats.formazione.ereditariet�epolimorfismo.animali.model.Animale;
import it.ats.formazione.ereditariet�epolimorfismo.animali.model.Orango;
import it.ats.formazione.ereditariet�epolimorfismo.animali.model.Volpe;

public class Principale {

	public static void main(String[] args) {

		Principale principale = new Principale();
		principale.esegui();

	}

	public void esegui() {

		ArrayList<Animale> animali = new ArrayList<>();
		Volpe volpe = new Volpe("Giulia", 7, 'f', "Volpe alpina");
		Orango orango = new Orango("Giulio", 13, 'm', 50);
		animali.add(volpe);
		animali.add(orango);

		for (Animale animale : animali) {
			System.out.println(animale);
			System.out.println("\t" + animale.cosaFaccio());
		}
	}
}
