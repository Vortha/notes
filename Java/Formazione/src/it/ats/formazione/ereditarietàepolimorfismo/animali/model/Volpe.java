package it.ats.formazione.ereditariet�epolimorfismo.animali.model;

public class Volpe extends Animale {

	private String tipoDiVolpe;

	public Volpe(String nome, int eta, char sesso, String coloreDelManto) {
		super(nome, eta, sesso);
		this.tipoDiVolpe = coloreDelManto;
	}

	@Override
	public String cosaFaccio() {
		return "Non arrivo all'uva e dico che � acerba";
	}

	@Override
	public String toString() {
		return "Volpe [tipoDiVolpe=" + tipoDiVolpe + ", nome=" + nome + ", eta=" + eta + ", sesso=" + sesso + "]";
	}

}
