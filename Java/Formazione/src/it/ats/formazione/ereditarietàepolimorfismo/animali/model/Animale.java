package it.ats.formazione.ereditariet�epolimorfismo.animali.model;

public abstract class Animale {

	protected String nome;
	protected int eta;
	protected char sesso;

	public Animale(String nome, int eta, char sesso) {
		super();
		this.nome = nome;
		this.eta = eta;
		this.sesso = sesso;
	}
	
	public abstract String cosaFaccio();

	@Override
	public String toString() {
		return "Animale [nome=" + nome + ", eta=" + eta + ", sesso=" + sesso + "]";
	}

}
