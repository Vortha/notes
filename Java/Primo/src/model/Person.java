package model;

public class Person implements Comparable<Person> {

	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Person o) {
		int affinity = 0;

		affinity = Math.abs(this.name.compareTo(o.name));
		affinity += Math.abs(this.age - o.age);
		if (affinity > 100) {
			affinity = 100;
		}

		return affinity;
	}

	public static void main(String[] args) {
		Person p1 = new Person();
		Person p2 = new Person();
		p1.setName("Valerio");
		p1.setAge(29);
		p2.setName("Alessia");
		p2.setAge(24);

		System.out.println(p1.compareTo(p2));
	}

}
