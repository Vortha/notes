package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/DataOra")
public class DataOraCorrenteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 1 - Recupero di eventuali parametri provenienti dal client

		// 2 - Elaborazione
		Date date = new Date();

		// 3 - Creazione della pagina HTML con i contenuti calcolati
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/response.jsp").forward(request, response);

	}

}
