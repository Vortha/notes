package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Somma")
public class SommaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String n1 = request.getParameter("n1");
		String n2 = request.getParameter("n2");
		double d1 = Double.parseDouble(n1);
		double d2 = Double.parseDouble(n2);

		double somma = d1 + d2;

		request.setAttribute("somma", somma);
		
		getServletContext().getRequestDispatcher("/WEB-INF/jsp/risSomma.jsp").forward(request, response);
	}

}
