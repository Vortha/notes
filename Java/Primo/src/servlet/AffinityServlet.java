package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Person;

@WebServlet("/Affinity")
public class AffinityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name1 = request.getParameter("person1Name");
		String name2 = request.getParameter("person2Name");
		String age1 = request.getParameter("person1Age");
		String age2 = request.getParameter("person2Age");

		int age1Int = Integer.parseInt(age1);
		int age2Int = Integer.parseInt(age2);

		Person p1 = new Person();
		p1.setName(name1);
		p1.setAge(age1Int);

		Person p2 = new Person();
		p2.setName(name2);
		p2.setAge(age2Int);

		int affinity = p1.compareTo(p2);
		String message = "";

		if (affinity >= 0 && affinity <= 25) {
			message = "Coppia molto affine!";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
		} else if (affinity >= 26 && affinity <= 50) {
			message = "Coppia affine!";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
		} else if (affinity >= 51 && affinity <= 75) {
			message = "Coppia non molto affine!";
			message += "<img src='img/pieno.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
		} else if (affinity >= 76 && affinity <= 100) {
			message = "Coppia per nulla affine!";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
			message += "<img src='img/vuoto.png' style='width: 20px; height: auto;'>";
		}

		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();

		pw.println("<html>");
		pw.println("<head>");
		pw.println("<title>Somma</title>");
		pw.println("<body>");
		pw.println(message);
		pw.println("</body>");
		pw.println("</html>");

		pw.close();
	}

}
