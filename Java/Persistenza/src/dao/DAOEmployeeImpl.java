package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.Employee;
import util.DateConversion;

public class DAOEmployeeImpl implements DAOEmployee {

	@Override
	public void insert(Employee e) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "insert into emp values(?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		preparedStatement.setInt(1, e.getEmpno());
		preparedStatement.setString(2, e.getEname());
		preparedStatement.setString(3, e.getJob());
		preparedStatement.setInt(4, e.getMgr());

		preparedStatement.setDate(5, DateConversion.localDateToDate(e.getHiredate()));
		preparedStatement.setDouble(6, e.getSal());
		preparedStatement.setDouble(7, e.getComm());
		preparedStatement.setInt(8, e.getDeptno());

		preparedStatement.executeUpdate();

	}

	@Override
	public void update(Employee e) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "update emp set ename = ?, job = ?, mgr = ?, hiredate = ?, sal = ?, comm = ?, deptno = ? where empno = ?";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		preparedStatement.setString(1, e.getEname());
		preparedStatement.setString(2, e.getJob());
		preparedStatement.setInt(3, e.getMgr());
		preparedStatement.setDate(4, DateConversion.localDateToDate(e.getHiredate()));
		preparedStatement.setDouble(5, e.getSal());
		preparedStatement.setDouble(6, e.getComm());
		preparedStatement.setInt(7, e.getDeptno());

		preparedStatement.setInt(8, e.getEmpno());

		preparedStatement.executeUpdate();

	}

	@Override
	public void delete(int empno) throws Exception {

		Connection connection = SingletonConnection.getConnection();

		String sql = "delete from emp where empno = ?";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		preparedStatement.setInt(1, empno);

		preparedStatement.executeUpdate();

	}

	@Override
	public List<Employee> select() throws Exception {
		List<Employee> employees = new ArrayList<>();

		Connection connection = SingletonConnection.getConnection();

		String sql = "select * from emp";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		ResultSet resultSet = preparedStatement.executeQuery();

		while (resultSet.next()) {
			Employee e = new Employee();
			e.setComm(resultSet.getInt("COMM"));
			e.setDeptno(resultSet.getInt("DEPTNO"));
			e.setEmpno(resultSet.getInt("EMPNO"));
			e.setEname(resultSet.getString("ENAME"));
			e.setJob(resultSet.getString("JOB"));
			e.setMgr(resultSet.getInt("MGR"));
			e.setSal(resultSet.getDouble("SAL"));
			e.setHiredate(DateConversion.dateToLocalDate(resultSet.getDate("HIREDATE")));
			employees.add(e);
		}

		return employees;
	}

	@Override
	public Employee select(int empno) throws Exception {
		Employee e = new Employee();

		Connection connection = SingletonConnection.getConnection();

		String sql = "select * from emp where empno = ?";

		PreparedStatement preparedStatement = connection.prepareStatement(sql);

		preparedStatement.setInt(1, empno);

		ResultSet resultSet = preparedStatement.executeQuery();

		if (resultSet.next()) {
			e.setComm(resultSet.getInt("COMM"));
			e.setDeptno(resultSet.getInt("DEPTNO"));
			e.setEmpno(resultSet.getInt("EMPNO"));
			e.setEname(resultSet.getString("ENAME"));
			e.setJob(resultSet.getString("JOB"));
			e.setMgr(resultSet.getInt("MGR"));
			e.setSal(resultSet.getDouble("SAL"));
			e.setHiredate(DateConversion.dateToLocalDate(resultSet.getDate("HIREDATE")));

			return e;
		}

		return null;
	}

}
