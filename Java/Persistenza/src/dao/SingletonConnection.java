package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class SingletonConnection {

	private static Connection connection;

	private SingletonConnection() throws SQLException {
		ResourceBundle bundle = ResourceBundle.getBundle("dao.datasource");
		String urlConnection = bundle.getString("urlConnection");
		String username = bundle.getString("username");
		String password = bundle.getString("password");
		connection = DriverManager.getConnection(urlConnection, username, password);
	}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			new SingletonConnection();
		}

		return connection;
	}
}
