package dao;

import java.util.List;

import bean.Employee;

public interface DAOEmployee {

	/**
	 * Insert a new Employee.
	 * 
	 * @param e
	 *            - Employee to insert
	 * @throws Exception
	 */
	public void insert(Employee e) throws Exception;

	/**
	 * Update an Employee.
	 * 
	 * @param e
	 *            - updated Employee
	 * @throws Exception
	 */
	public void update(Employee e) throws Exception;

	/**
	 * Delete an Employee.
	 * 
	 * @param empno
	 *            - Employee (identifier) to delete
	 * @throws Exception
	 */
	public void delete(int empno) throws Exception;

	/**
	 * Select all Employee(s).
	 * 
	 * @return the list of Employee(s)
	 * @throws Exception
	 */
	public List<Employee> select() throws Exception;

	/**
	 * Select an Employee.
	 * 
	 * @param empno
	 *            - Employee identifier
	 * @return the Employee identified by empno
	 * @throws Exception
	 */
	public Employee select(int empno) throws Exception;
}
