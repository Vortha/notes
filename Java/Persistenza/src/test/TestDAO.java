package test;

import java.time.LocalDate;

import bean.Employee;
import dao.DAOEmployeeImpl;

public class TestDAO {

	public static void main(String[] args) {

		try {
			DAOEmployeeImpl dao = new DAOEmployeeImpl();

			Employee employee = new Employee();
			employee.setComm(1);
			employee.setDeptno(10);
			employee.setEmpno(1000);
			employee.setEname("GIACOMO");
			employee.setJob("MANAGER");
			employee.setMgr(7902);
			employee.setSal(2000);
			LocalDate hiredate = LocalDate.now();
			employee.setHiredate(hiredate);

			// dao.insert(employee);

			// dao.delete(1200);

			// dao.update(employee);

//			List<Employee> employees = dao.select();
//			for (Employee employee2 : employees) {
//				System.out.println(employee2);
//			}

			Employee e = dao.select(1000);
			System.out.println(e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Errore nella richiesta");
			e.printStackTrace();
		}
	}
}
