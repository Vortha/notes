import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestPersistenza {

	/**
	 * Insert department with deptno = 50, dname = HUMAN RESOURCE, loc = ROME.
	 * 
	 * @param connection
	 *            - the connection to the db
	 * @throws SQLException
	 */
	public static void insert(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement("insert into dept values(?, ?, ?)");
		ps.setInt(1, 50);
		ps.setString(2, "HUMAN RESOURCE");
		ps.setString(3, "ROME");

		ps.executeUpdate();

		System.out.println("inserito");
	}

	/**
	 * Delete department with deptno = 50.
	 * 
	 * @param connection
	 *            - the connection to the db
	 * @throws SQLException
	 */
	public static void delete(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement("delete from dept where deptno = ?");
		ps.setInt(1, 50);

		ps.executeUpdate();

		System.out.println("cancellato");
	}

	/**
	 * Increase the salary of 100 to 10 department's employee.
	 * 
	 * @param connection
	 *            - the connection to the db
	 * @throws SQLException
	 */
	public static void update(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement("update emp set sal = sal + ? where deptno = ?");
		ps.setInt(1, 100);
		ps.setInt(2, 10);

		ps.executeUpdate();

		System.out.println("aggiornato");
	}

	/**
	 * Select all values from department table.
	 * 
	 * @param connection
	 *            - the connection to the db
	 * @throws SQLException
	 */
	public static void select(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement("select * from dept");

		ResultSet resultSet = ps.executeQuery();

		while (resultSet.next()) {
			int deptno = resultSet.getInt("DEPTNO");
			String dname = resultSet.getString("DNAME");
			String loc = resultSet.getString("LOC");

			System.out.println("DEPTNO : " + deptno + " - DNAME : " + dname + " - LOC : " + loc);
		}
	}

	/**
	 * Select all values from emp where the salary > 1000 and the location is
	 * NEW YORK.
	 * 
	 * @param connection
	 *            - the connection to the db
	 * @throws SQLException
	 */
	public static void selectEmp(Connection connection) throws SQLException {
		String query = "select emp.ename, emp.sal, dept.deptno, dept.loc from emp, dept where sal > ? and dept.loc = ? and emp.deptno = dept.deptno";

		PreparedStatement ps = connection.prepareStatement(query);

		ps.setInt(1, 1000);
		ps.setString(2, "NEW YORK");

		ResultSet resultSet = ps.executeQuery();

		while (resultSet.next()) {
			String ename = resultSet.getString("ENAME");
			int sal = resultSet.getInt("SAL");
			int deptno = resultSet.getInt("DEPTNO");
			String loc = resultSet.getString("LOC");

			System.out.println("ENAME : " + ename + " - SAL : " + sal + " - DEPTNO : " + deptno + " - LOC : " + loc);
		}
	}

	public static void main(String[] args) {

		// 1 - Assicurarsi di avere il connettore nel class path applicativo

		// 1.1 - Caricare il driver (non pi� necessario da JDBC 1.4)
		// Class.forname("nome driver");

		// 2 - Aprire una connessione verso il DB

		try (Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:oracle", "scott",
				"tiger")) {

			System.out.println("connessione aperta");

			// 3 - Operazione con le tabelle del db
			// TestPersistenza.insert(connection);
			// TestPersistenza.delete(connection);
			// TestPersistenza.update(connection);
			// TestPersistenza.select(connection);
			TestPersistenza.selectEmp(connection);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
