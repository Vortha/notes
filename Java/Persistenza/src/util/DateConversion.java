package util;

import java.sql.Date;
import java.time.LocalDate;

public class DateConversion {

	/**
	 * Convert the java.time.LocalDate to java.sql.Date
	 * 
	 * @param date
	 *            - the date instance to be converted
	 * @return the converted date
	 */
	public static Date localDateToDate(LocalDate date) {
		return java.sql.Date.valueOf(date);
	}

	/**
	 * Convert the java.sql.Date to java.time.LocalDate
	 * 
	 * @param date
	 *            - the date instance to be converted
	 * @return the converted date
	 */
	public static LocalDate dateToLocalDate(Date date) {
		return date.toLocalDate();
	}
}
