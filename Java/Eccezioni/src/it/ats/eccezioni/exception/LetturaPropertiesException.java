package it.ats.eccezioni.exception;

import java.io.IOException;

//Viene sollevata quando non riesce a leggere i valori
//da un file di properties

public class LetturaPropertiesException extends IOException {

	public LetturaPropertiesException() {
		super();
	}

	public LetturaPropertiesException(String message) {
		super(message);
	}

}