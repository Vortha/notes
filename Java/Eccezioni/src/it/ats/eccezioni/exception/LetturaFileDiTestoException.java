package it.ats.eccezioni.exception;

import java.io.FileNotFoundException;

// Viene sollevata quando non trova il file
public class LetturaFileDiTestoException extends FileNotFoundException {

	public LetturaFileDiTestoException() {
		super();
	}

	public LetturaFileDiTestoException(String message) {
		super(message);
	}

}