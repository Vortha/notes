package it.ats.eccezioni.controllo;

import it.ats.eccezioni.modello.Calcolatrice;

//Qui sto lavorando con un'eccezione non controllata, per cui ho inserito io i try/catch e throws,
//il compilatore non me li segnala perch� sono eccezioni che vengono sollevate a runtime
public class PrincipaleEccezioneNonControllata {

	public static void main(String[] args) {

		PrincipaleEccezioneNonControllata principale = new PrincipaleEccezioneNonControllata();

		// 1) Ho gestito l'eccezione nel metodo chiamato: eccezioneGestita()
		principale.eccezioneGestita();

		// 2) Ho gestito nel main(metodo chiamante) l'eccezione che era stata
		// rilanciata dal
		// metodo chiamato: eccezioneRilanciataEGestitaNelMain()

		try {

			principale.eccezioneRilanciataEGestitaNelMain();

		} catch (ArithmeticException ae) {

			System.out.println("Non puoi effettuare una divisione per zero");

		}

		// 3) Stessa cosa del punto 2, solo che l'eccezione � stata gestita nel
		// metodo: eccezioneRilanciataEGestitaDalChiamante()
		principale.eccezioneRilanciataEGestitaDalChiamante();

	}

	// Gestisce l'eccezione ma non interrompe il flusso di esecuzione
	public void eccezioneGestita() {

		int risultatoDivisione;

		try {

			risultatoDivisione = Calcolatrice.divisione(5, 0);

			// non arriva a fare questa stampa perch� va in errore
			System.out.println("Il risultato della divisione �:" + risultatoDivisione);

		}

		catch (ArithmeticException aex) {

			// questo metodo mi dice dove si � verificata l'eccezione
			// aex.printStackTrace();

			System.out.println(aex);

			System.out.println("Non puoi effettuare una divisione per zero");

		}

		// Non arriver� mai in questo blocco perch� verr� catturata dal
		// precedente
		catch (Exception exc) {

			System.out.println(exc);

			System.out.println("Si � verificata una generica eccezione");

		}

		System.out.println("Ho terminato l'esecuzione del metodo");

	}

	// Rilancia l'eccezione e qualora si verifichi, interrompe il flusso di
	// esecuzione ed esce,
	// Per cui non effettua alcuna stampa System.out.println(...);
	public void eccezioneRilanciataEGestitaNelMain() throws ArithmeticException {

		int risultatoDivisione = Calcolatrice.divisione(5, 0);

		// Non vedr� mai queste stampe perch� viene rilanciata eccezione prima
		// e quindi non arriver� mai qui l'esecuzione
		System.out.println("Il risultato della divisione �:" + risultatoDivisione);

	}

	public void eccezioneRilanciataEGestitaDalChiamante() {

		int risultato = 0;

		try {

			risultato = rilancio();

		} catch (ArithmeticException ex) {

			System.out.println("Non puoi effettuare una divisione per zero");

		}

		// Questo messaggio verr� stampato, in quanto il flusso di esecuzione
		// non viene interrotto
		System.out.println("Il risultato della divisione �: " + risultato);

	}

	public int rilancio() throws ArithmeticException {

		int risultatoDivisione;

		risultatoDivisione = Calcolatrice.divisione(5, 0);

		// Non arriver� mai a stamparlo perch� viene rilanciata l'eccezione
		// prima
		System.out.println("Ho terminato l'esecuzione del metodo");

		return risultatoDivisione;

	}

}
