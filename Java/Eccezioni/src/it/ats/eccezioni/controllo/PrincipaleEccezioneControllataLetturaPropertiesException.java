package it.ats.eccezioni.controllo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import it.ats.eccezioni.exception.LetturaPropertiesException;

public class PrincipaleEccezioneControllataLetturaPropertiesException {

	public static void main(String[] args) {

		PrincipaleEccezioneControllataLetturaPropertiesException principaleTrasformazioneEccezione = new PrincipaleEccezioneControllataLetturaPropertiesException();

		try {

			principaleTrasformazioneEccezione.trasformaEccezione();

		} catch (LetturaPropertiesException e) {

			e.printStackTrace();

		}

	}

	// Leggo 2 propriet� da un file di properties e in caso di errore in lettura
	// trasformo l'eccezione di IOException e rilancio quella
	// personalizzata a cui associo un messaggio
	public void trasformaEccezione() throws LetturaPropertiesException {

		Properties dati = new Properties();

		InputStream input = null;

		try {

			input = new FileInputStream("dati.properties");

			dati.load(new FileInputStream("dati.properties"));

			// cambiare in nome per eseguire il tutto correttamente:
			String nome = dati.getProperty("nomes");

			String cognome = dati.getProperty("cognome");

			if (nome == null || cognome == null) {

				// supponiamo che si verifichi questa eccezione... ora sto
				// forzando a farlo andare in errore
				throw new IOException();

			}

			System.out.println("Lettura dei valori effettuata correttamente: " + nome + " " + cognome);

		} catch (IOException e) {

			// trasformo l'eccezione

			throw new LetturaPropertiesException(
					"Ops si � verificato un errore nella lettura di una o pi� propriet�");

		}

		if (input != null) {

			try {

				input.close();

			} catch (IOException e) {

				e.printStackTrace();

			}
		}

	}

}
