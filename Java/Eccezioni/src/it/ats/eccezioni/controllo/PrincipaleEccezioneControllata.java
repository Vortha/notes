package it.ats.eccezioni.controllo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import it.ats.eccezioni.exception.LetturaFileDiTestoException;

//Qui sto lavorando con un'eccezione non controllata, per cui ho inserito io i try/catch e throws,
//il compilatore non me li segnala perch� sono eccezioni che vengono sollevate a runtime

public class PrincipaleEccezioneControllata {

	public static void main(String[] args) {

		PrincipaleEccezioneControllata principale = new PrincipaleEccezioneControllata();

		// principale.eccezioneGestita();

		FileReader reader = null;
		BufferedReader in = null;

		try {

			reader = new FileReader("inputs.txt");
			in = new BufferedReader(reader);

			principale.eccezioneRilanciataEGestitaNelMain(reader, in);

		} catch (FileNotFoundException fnfe) {

			fnfe.printStackTrace();

			System.out.println("File non trovato");

		} catch (IOException e) {

			e.printStackTrace();

			System.out.println("Non riesco ad effettuare la lettura da file");

		} finally {

			try {

				in.close();

				reader.close();

			} catch (IOException ioe) {

				ioe.printStackTrace();

			}

		}

		// principale.eccezioneTrasformataRilanciataEGestitaDalChiamante();

	}

	// Gestisce l'eccezione ma non interrompe il flusso di esecuzione
	public void eccezioneGestita() {

		FileReader reader;
		BufferedReader in;

		try {

			reader = new FileReader("inputs.txt");
			in = new BufferedReader(reader);
			String sCurrentLine;

			while ((sCurrentLine = in.readLine()) != null) {
				System.out.println(sCurrentLine);
			}

			// non arriva a fare questa stampa, se il file non viene trovato,
			// perch� va in errore e salta nel catch
			System.out.println("Non si � verificata alcuna eccezione");

		} catch (FileNotFoundException fnfe) {

			// il metodo printStackTrace() mi dice dove si � verificata
			// l'eccezione
			fnfe.printStackTrace();

			System.out.println("File non trovato");

		} catch (IOException e) {

			e.printStackTrace();

			System.out.println("Non riesco ad effettuare la lettura da file");

		}

		System.out.println("Ho terminato l'esecuzione del metodo");

	}

	// Rilancia l'eccezione e qualora si verifichi, interrompe il flusso di
	// esecuzione ed esce,
	// Per cui non effettua alcuna stampa System.out.println(...);
	public void eccezioneRilanciataEGestitaNelMain(FileReader reader, BufferedReader in)
			throws FileNotFoundException, IOException {

		String sCurrentLine;

		while ((sCurrentLine = in.readLine()) != null) {
			System.out.println(sCurrentLine);
		}

		// Non vedr� mai queste stampe perch� viene rilanciata un'eccezione
		// prima di arrivare a questo punto e quindi il controllo passa al
		// metodo chiamante
		System.out.println("Ho terminato l'esecuzione del metodo");

	}

	public void eccezioneTrasformataRilanciataEGestitaDalChiamante() {

		try {

			trasformoERilancioEccezionePersonalizzata();

		} catch (LetturaFileDiTestoException lfte) {

			lfte.getMessage();

			lfte.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void trasformoERilancioEccezionePersonalizzata() throws LetturaFileDiTestoException, IOException {

		FileReader reader = null;
		try {

			reader = new FileReader("inputs.txt");

			BufferedReader in = new BufferedReader(reader);
			String sCurrentLine;

			while ((sCurrentLine = in.readLine()) != null) {
				System.out.println(sCurrentLine);
			}

		} catch (FileNotFoundException fnfe) {

			throw new LetturaFileDiTestoException("Non riesco a trovare il file di testo specificato");

		} catch (IOException e) {

			throw new IOException("Generico errore di I/O");

		}

		// non arriva a fare questa stampa, se il file non viene trovato,
		// perch� va in errore e salta nel catch
		System.out.println("Ho terminato l'esecuzione del metodo");

	}

}
