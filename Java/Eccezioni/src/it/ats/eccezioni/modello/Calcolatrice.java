package it.ats.eccezioni.modello;

public class Calcolatrice {
	
	public static double addizione(double op1, double op2){
		
		double risultato = op1 + op2;
		
		return risultato;
		
	}
	
	public static double sottrazione(double op1, double op2){
		
		double risultato = op1 - op2;
		
		return risultato;
		
	}
	
	public static double moltiplicazione(double op1, double op2){
		
		double risultato = op1 * op2;
		
		return risultato;
		
	}
	
	//con l'int restituisce eccezione, con double e float no
	public static int divisione(int op1, int op2){
		
		int risultato = op1 / op2;
		
		return risultato;
		
	}

}
