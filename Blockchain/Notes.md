# 1. What is Blockchain?

<p>
A blockchain is a growing list of records, called blocks, which are linked used cryptography. Each block
contains a cryptographic hash of the previous block, a timestamp and a transaction data (generally represented
as a merkle tree, or hash tree, root hash).
</p>

# 2. How does Blockchain work?

<p>
Information held on a blockchain exists as a shared database. The blockchain database isn't stored in any single
location, meaning the records it keeps are truly public and easily verifiable. No centralized version of this
information exists for a hacker to corrupt. Hosted by millions of computers simultaneously, its data is accessible
to anyone on the internet.
</p>