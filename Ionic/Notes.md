<h1>Environment</h1>
<table>
    <thead>
        <tr>
            <th>Component</th>
            <th>Description</th>
            <th>Additional</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Ionic</td>
            <td>Mobile applications development framework</td>
            <td></td>
        </tr>
        <tr>
            <td>Atom</td>
            <td>Text editor</td>
            <td><a href="https://atom.io/packages/atom-typescript">Atom TypeScript</a> plugin</td>
        </tr>
        <tr>
            <td>GIT</td>
            <td>VCS</td>
            <td></td>
        </tr>
    </tbody>
</table>

<h1>Atom shortcuts</h1>
<dl>
    <dt>ctrl + shift + \</dt>
    <dd>Shows the current viewed file inside the project structure</dd>
</dl>

<h1>Ionic commands</h1>
<dl>
    <dt>ionic start <projectName> blank</dt>
    <dd>Creates a new Ionic application with a blank page</dd>

    <dt>ionic g page <page-name></dt>
    <dd>Creates a new page</dd>
</dl>